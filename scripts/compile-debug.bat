@git pull origin
@set MYCURFOLDER=%~dp0
@call C:\Qt\5.15.2\mingw81_32\bin\qtenv2.bat
cd %MYCURFOLDER%\build-libengine
@qmake.exe ..\src\libengine\engine.pro -r -spec win32-g++ "CONFIG+=debug" && mingw32-make.exe qmake_all
@mingw32-make.exe -j4
cd %MYCURFOLDER%\build
@qmake.exe ..\src\billfold\Billfold.pro -r -spec win32-g++ "CONFIG+=debug" && mingw32-make.exe qmake_all
@mingw32-make.exe -j4
cd debug
@windeployqt --debug Billfold.exe
@rm engine.dll
@mklink engine.dll ..\..\build-libengine\debug\engine.dll
cd %MYCURFOLDER%
