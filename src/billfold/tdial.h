#ifndef TDIAL_H
#define TDIAL_H

#include <cmath>
#include <QWidget>

class TLancet;
class TDial : public QWidget
{
  Q_OBJECT
  TLancet *_lancet;
protected:
  static QPointF pointInCircumference(QPointF C, double r, double a);
protected slots:
  void paintEvent(QPaintEvent *event) override;
public:
//    static double                    m(QPointF A, QPointF C);
//    static std::pair<double, double> Px(double m, QPointF A, double d);
//    static double                    Py(double m, QPointF A, double Px);
//    static QPointF                   P(QPointF A, QPointF C, double d);

  explicit TDial(QWidget *parent = nullptr);
};

#endif // TDIAL_H
