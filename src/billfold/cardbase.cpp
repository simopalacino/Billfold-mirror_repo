#include "cardbase.h"

#include <QPainter>
#include <QResizeEvent>
#include <QStyleOption>

int CardBase::heightTitle()
{
  return _titleLbl->size().height() + _titleLbl->pos().y();
}

void CardBase::paintEvent(QPaintEvent *event)
{
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
  QWidget::paintEvent(event);
}

void CardBase::resizeEvent(QResizeEvent *event)
{
  if (_wchild)
    _wchild->resize(event->size().width() - LEFT_SPACE*2, event->size().height() - (heightTitle() + UPPER_SPACE*2));
}

void CardBase::addWidget(QWidget* child)
{
  if (_wchild)
    delete _wchild;
  _wchild = child;
  _wchild->setParent(this);
  _wchild->move({ LEFT_SPACE, heightTitle() + 4 });
  QSize s = { size().width() - LEFT_SPACE*2, size().height() - (heightTitle() + UPPER_SPACE*2) };
  _wchild->resize(s);
  _wchild->show();
}

void CardBase::setTitle(QString title)
{
  _titleLbl->setText(title);
}

CardBase::CardBase(QWidget *parent)
  : QWidget{parent}
{
  _titleLbl = new QLabel(this);
  _titleLbl->setObjectName("titleLbl");
  _titleLbl->setStyleSheet("font: Bold 14pt;");

  _titleLbl->move({ 16, 16 });

}
