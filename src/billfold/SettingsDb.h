#ifndef SETTINGSDB_H
#define SETTINGSDB_H

#include <QDialog>

namespace Ui {
class SettingsDb;
}

class SettingsDb : public QDialog
{
  Q_OBJECT

  void errorInput();

public:
  explicit SettingsDb(QString addrAndPort, QString dbName, QWidget *parent = nullptr);
  ~SettingsDb();

private slots:
  void on_buttonBox_accepted();

private:
  Ui::SettingsDb *ui;
};

#endif // SETTINGSDB_H
