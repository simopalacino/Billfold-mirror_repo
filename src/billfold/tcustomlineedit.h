#ifndef TCUSTOMLINEEDIT_H
#define TCUSTOMLINEEDIT_H

#include <QLineEdit>

class TCustomLineEdit : public QLineEdit
{
  Q_OBJECT
  bool _nullable = false;
  static constexpr const char * NULL_STRING = "< null >";
protected slots:
  void on_editFinished();

protected:
  QString _warningMsg;

  virtual QString defaultText()               = 0;
  virtual bool    isValid()                   = 0;
  virtual void    setText(const QString &str) = 0;
  virtual QString toString()                  = 0;
public:
  bool isNullable() const { return _nullable; }
  void setText();
  void setNullable(bool nullable = true, bool setNull = false);
  TCustomLineEdit(QWidget *parent = nullptr);
};

#endif // TCUSTOMLINEEDIT_H
