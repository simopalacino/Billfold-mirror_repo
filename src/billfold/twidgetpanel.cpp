#include "twidgetpanel.h"
#include "utils.h"

uint16_t TWidgetPanel::_lMargin{ 16 };
uint16_t TWidgetPanel::_rMargin{ 16 };
uint16_t TWidgetPanel::_tMargin{ 16 };
uint16_t TWidgetPanel::_bMargin{ 8 };

void TWidgetPanel::paintEvent(QPaintEvent *event)
{
  (void)event;
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void TWidgetPanel::panelResized(int w, int h)
{
  uint16_t tw = _lMargin + _rMargin + w;
  uint16_t th = _tMargin + h + _bMargin + 16 * 2 + _bMargin;
  resize(tw, th);
  appLog("after Resize: %d, %d", size().width(), size().height());
  _panel->move(_lMargin, _rMargin);
  _title.resize(w, 16);
  _value.resize(w, 16);
  _title.move(_lMargin, _tMargin + h + _bMargin);
  _value.move(_lMargin, _tMargin + h + _bMargin + 16);
}

void TWidgetPanel::resizeEvent(QResizeEvent *event)
{
  (void)event;
  if (_panel)
    panelResized(_panel->width(), _panel->height());
}

void TWidgetPanel::addPanel(QWidget *panel)
{
  if (!panel)
    panel = new QWidget(this);

  _panel = panel;
  _panel->setParent(this);
  panelResized(_panel->size().width(), _panel->size().height());
}

void TWidgetPanel::setTitle(const QString &str) { _title.setText(str); }

void TWidgetPanel::setValue(const QString &str) { _value.setText(str); }

TWidgetPanel::TWidgetPanel(QWidget *parent) : QWidget(parent), _panel(nullptr), _title(this), _value(this)
{
  QWidget::setStyleSheet(QString::fromUtf8("background-color: #bbbcbd;"));
  _title.setStyleSheet("font: Bold 14pt;");
  _title.setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
  _value.setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
}
