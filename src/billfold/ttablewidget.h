#ifndef TTABLEWIDGET_H
#define TTABLEWIDGET_H

#include <QTableWidget>

class TTableWidget : public QTableWidget
{
  Q_OBJECT
public:
  enum class EType
  {
    Expense  = 0,
    Income   = 1
  };

private:
  int _row;
  QMenu *_menu;

private slots:
  void mousePressEvent(QMouseEvent *event) override;
  void on_recordEditAction();
  void on_recordDeleteAction();
  void on_recordDuplicateAction();
signals:
  void askToEditSignal(EType type, uint id);
  void askToDeleteSignal(EType type, uint id);
  void askToDuplicateSignal(EType type, uint id);
public:
  explicit TTableWidget(QWidget *parent = nullptr);
  ~TTableWidget();
};

#endif // TTABLEWIDGET_H
