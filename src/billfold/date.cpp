#include "date.h"

#include <QDate>
#include <QMessageBox>

#include "utils.h"


void TDateEdit::on_editFinished()
{

}

void TDateEdit::setDate(const QString &str)
{
  _isNull = str == "";
  QDate date(QDate::fromString(str, Qt::DefaultLocaleShortDate));
  static const char *formats[] = { "yyyy-MM-dd", "dd/MM/yyyy", "dd-MM-yyyy", "d/M/yy", "yyyyMMdd", "ddMMyy", "yyMMdd" };
  unsigned i;
  for (i = 0; i < sizeof formats / sizeof(const char*) && !date.isValid(); ++i)
    date = QDate::fromString(str, formats[i]);
  setDate(date);
}

void TDateEdit::setDate(const QDate &date)
{
  _value = date;
  TCustomLineEdit::setText();
}

void TDateEdit::setText(const QString &str)
{
  setDate(str);
}

QString TDateEdit::ansi()
{
  return _value.toString(ANSI_DATE_FORMAT);
}

QDate TDateEdit::getDate()
{
  return _value;
}

QString TDateEdit::defaultText()
{
  return _today.toString(Qt::DefaultLocaleShortDate);
}

bool TDateEdit::isValid()
{
  return _value.isValid() || (isNullable() && _isNull);
}

QString TDateEdit::toString()
{
  return !isNullable() || _value.isValid() ? _value.toString(Qt::DefaultLocaleShortDate) : "";
}

bool TDateEdit::operator==(const TDateEdit& rhv)
{
  if (!_value.isValid() || !rhv._value.isValid())
    return false;
  return _value == rhv._value;
}

bool TDateEdit::operator<(const TDateEdit& rhv)
{
  if (!_value.isValid() || !rhv._value.isValid())
    return false;
  return _value < rhv._value;
}

bool TDateEdit::operator>(const TDateEdit& rhv)
{
  if (!_value.isValid() || !rhv._value.isValid())
    return false;
  return _value > rhv._value;
}

bool TDateEdit::operator>=(const TDateEdit& rhv)
{
  if (!_value.isValid() || !rhv._value.isValid())
    return false;
  return _value >= rhv._value;
}

bool TDateEdit::operator<=(const TDateEdit& rhv)
{
  if (!_value.isValid() || !rhv._value.isValid())
    return false;
  return _value <= rhv._value;
}

TDateEdit::TDateEdit(QWidget *parent) : TCustomLineEdit(parent), _today(QDate::currentDate()), _isNull(false)
{
  connect(this, &TDateEdit::editingFinished, this, &TDateEdit::on_editFinished);
  _warningMsg = tr("Invalid date format.");
  setDate(_today);
}
