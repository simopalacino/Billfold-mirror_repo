#include "billfold0200.h"

#include <QApplication>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QDate>
#include <QMessageBox>
#include <QSettings>


bool TTranslator::insertAccounts()
{
  QSqlQuery sql(_mssqlDb);
  QSqlQuery sql2(_sqliteDb);
  QString   query = QString::fromUtf8("SELECT conto, name, owner, banca, contanti, risparmio, "
                                      "init_value, date_init, value, color, sorting, visible FROM conti");
  QString   query2;
  for (bool ok = sql.exec(query) && sql.first(); ok; ok = sql.next())
  {
    query2 = QString::fromUtf8("INSERT INTO accounts "
"(id, name, owner, banca, contanti, risparmio, init_value, date_init, value, color, sorting, visible)\n"
"VALUES (");

    /* id         */ query2 += sql.value(0).toString() + ", ";
    /* name       */ query2 += "'" + sql.value(1).toString() + "', ";
    /* owner      */ query2 += "'" + sql.value(2).toString() + "', ";
    /* banca      */ query2 += "'" + sql.value(3).toString() + "', ";
    /* contanti   */ query2 += sql.value(4).toBool() ? "1, " : "0, ";
    /* risparmio  */ query2 += sql.value(5).toBool() ? "1, " : "0, ";
    /* init_value */ query2 += "'" + sql.value(6).toString() + "', ";
    /* date_init  */ query2 += "'" + sql.value(7).toDate().toString("yyyy-MM-dd") + "', ";
    /* value      */ query2 += "'" + sql.value(8).toString() + "', ";
    /* color      */ query2 += sql.value(9).toString() + ", ";
    /* sorting    */ query2 += sql.value(10).toString() + ", ";
    /* visible    */ query2 += sql.value(11).toBool() ? "1" : "0";

    query2 += ");";
    if (!sql2.exec(query2))
    {
      qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
      return false;
    }
  }
  return true;
}

bool TTranslator::insertTags()
{
  QSqlQuery sql(_mssqlDb);
  QSqlQuery sql2(_sqliteDb);
  QString   query = QString::fromUtf8("SELECT name FROM tag_out"); // UPDATE TO CATEGORIES
  QString   query2;
  for (bool ok = sql.exec(query) && sql.first(); ok; ok = sql.next())
  {
    query2 = QString::fromUtf8("INSERT INTO tags (name, nature, visible)\n"
                               "VALUES (");

    /* name       */ query2 += "'" + sql.value(0).toString() + "', ";
    /* nature     */ query2 += "0, ";
    /* color      */ ;
    /* icon       */ ;
    /* visible    */ query2 += "1";

    query2 += ");";
    if (!sql2.exec(query2))
    {
      qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
      return false;
    }
  }

  query = QString::fromUtf8("SELECT name, second\n"
                            "FROM tag_out\n"      // todo: update to categories!!
                            "JOIN t_out_is_a toia on tag_out.name = toia.first");

  for (bool ok = sql.exec(query) && sql.first(); ok; ok = sql.next())
  {
    query2 = QString::fromUtf8("INSERT INTO subtags (tagP, tagC)\n"
                               "VALUES (");

    /* tagP       */ query2 += "'" + sql.value(1).toString() + "', ";
    /* tagC       */ query2 += "'" + sql.value(0).toString() + "'";

    query2 += ");";
    if (!sql2.exec(query2))
    {
      qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
      return false;
    }
  }

  // Incomes
  query = QString::fromUtf8("SELECT name\n"
                            "FROM tag_in\n");
  query2 = QString::fromUtf8("INSERT INTO tags (name, nature, visible)\nVALUES ('Incomes', 1, 1);");

  if (!sql2.exec(query2))
  {
    qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
    return false;
  }

  for (bool ok = sql.exec(query) && sql.first(); ok; ok = sql.next())
  {
    query2 = QString::fromUtf8("INSERT INTO tags (name, nature, visible)\n"
                               "VALUES (");

    /* name       */ query2 += "'" + sql.value(0).toString() + "', ";
    /* nature     */ query2 += "1, ";
    /* color      */ ;
    /* icon       */ ;
    /* visible    */ query2 += "1";

    query2 += ");";

    if (!sql2.exec(query2))
    {
      qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
    }

    query2 = QString("INSERT INTO subtags (tagP, tagC)\n"
                               "VALUES (");

    /* tagP */ query2 += "'Incomes', ";
    /* tagC */ query2 += "'" + sql.value(0).toString() + "'";

    query2 += ");";

    if (!sql2.exec(query2))
    {
      qDebug("Error insertion!\n%s", query2.toLocal8Bit().data());
    }
  }
  return true;
}

bool TTranslator::insertTransactions()
{
  QSqlQuery sql2(_sqliteDb);
  QString   query2 = "CREATE TABLE \"temp\" ("
          "    \"old_id\"	INTEGER NOT NULL,"
          "    \"new_id\"	INTEGER NOT NULL UNIQUE,"
          "    \"uscita\"	INTEGER NOT NULL DEFAULT 1,"
          "    \"nota_cred\"	INTEGER,"
          "    FOREIGN KEY(\"new_id\") REFERENCES \"transactions\"(\"id\"),"
          "    PRIMARY KEY(\"old_id\",\"uscita\")"
          ");";
  if (!sql2.exec(query2))
  {
    qDebug("%s", query2.toLocal8Bit().data());
    qDebug("Error CREATE TABLE!");
    return false;
  }



  QSqlQuery sql(_mssqlDb);
  QString   query = QString("SELECT id, date, -cifra AS cifra, tag, description, conto, acc_date, nota_cred, time, 1 as is_uscita FROM uscite\n"
                            "UNION ALL\n"
                            "SELECT id, date, cifra AS cifra, tag, description, conto, acc_date, nota_cred, time, 0 as is_uscita FROM entrate\n"
                            "ORDER BY date ASC;");

  int reg_id = 0, new_id = 0;
  for (bool ok = sql.exec(query) && sql.first(); ok; ok = sql.next(), ++reg_id, ++new_id)
  {
    query2 = QString::fromUtf8("INSERT INTO registrations (id, deleted) VALUES (");
    query2 += QString::fromUtf8("%1, 0);").arg(reg_id);

    if (!sql2.exec(query2))
    {
      qDebug("%s", query2.toLocal8Bit().data());
      qDebug("Error insertion REGISTRATION!");
      return false;
    }

    query2 = QString("INSERT INTO transactions "
                     "(id, date, time, acc_date, tag, value, description, account, registration, deleted)\n"
                     "VALUES (");

    bool    timenn   = !sql.value(8).isNull();
    QString time     = "'" + sql.value(8).toString() + "', ";
    bool    adatenn  = !sql.value(6).isNull();
    QString acc_date = "'" + sql.value(6).toString() + "', ";
    /* id           */ query2 += QString::fromUtf8("%1, ").arg(new_id);
    /* date         */ query2 += "'" + sql.value(1).toString() + "', ";
    /* time         */ query2 += timenn ? time : "NULL, ";
    /* acc_date     */ query2 += adatenn ? acc_date : "NULL, ";
    /* tag          */ query2 += "'" + sql.value(3).toString() + "', ";
    /* value        */ query2 += QString::fromUtf8("'%1', ").arg(sql.value(2).toString());
    /* causal       */ ;
    /* description  */ query2 += "'" + sql.value(4).toString().replace("'", "''") + "', ";
    /* account      */ query2 += "'" + sql.value("conto").toString() + "', ";
    /* registration */ query2 += QString::fromUtf8("%1, ").arg(reg_id);
    /* g1           */ ;
    /* deleted      */ query2 += "0);";

    if (!sql2.exec(query2))
    {
      qDebug("%s", query2.toLocal8Bit().data());
      qDebug("Error insertion!\n");
      return false;
    }

    bool null_notaCred = sql.value("nota_cred").isNull();
    query2 = QString::fromUtf8("INSERT INTO temp VALUES (%1, %2, %3, %4)")
                              .arg(sql.value(0).toString())
                              .arg(new_id)
                              .arg(sql.value("is_uscita").toInt())
                              .arg(!null_notaCred ? sql.value("nota_cred").toString() : "NULL");
    if (!sql2.exec(query2))
    {
      qDebug("%s", query2.toLocal8Bit().data());
      qDebug("Error insertion!\n");
      return false;
    }

    if ((new_id % 100) == 0)
      qDebug("%s", query2.toLocal8Bit().data());
  }
  return true;
}

void TTranslator::sistemaNoteCredInRegistrations()
{
  QSqlQuery sqlLite(_sqliteDb);
  QString   queryLite = QString::fromUtf8("SELECT old_id, new_id, uscita, nota_cred FROM temp;");
//    typedef struct
//    {
//        int  old_id;
//        bool uscita;
//    } key;
  typedef struct
  {
    int  old_id;
    int  new_id;
    bool uscita;
    int  nota_cred;
  } ttemp;
  std::map<std::pair<int, bool>, ttemp> map;
  std::vector<ttemp>   vect;
  for (bool ok = sqlLite.exec(queryLite) && sqlLite.first(); ok; ok = sqlLite.next())
  {
    bool is_null = sqlLite.value(3).isNull();
    ttemp t = { sqlLite.value(0).toInt(), sqlLite.value(1).toInt(), sqlLite.value(2).toBool(), is_null ? -1 : sqlLite.value(3).toInt() };

    if (!is_null)
      vect.emplace_back(t);
    map.insert({ {t.old_id, t.uscita}, t});
  }

  for (auto it = vect.begin(); it != vect.end(); ++it)
  {
    ttemp t = *it;
    qDebug("%s", QString::fromUtf8("old_id:%1, new_id:%2, uscita:%3, nota_cred:%4").arg(t.old_id).arg(t.new_id).arg(t.uscita).arg(t.nota_cred).toLocal8Bit().data());
    if (!t.uscita)
    {
      ttemp &entrata = t;
      std::pair<int, bool> keyUscitaDaAssociare = { entrata.nota_cred, 1 };
      ttemp &uscitaDaAssociare = map[keyUscitaDaAssociare];
      qDebug("uscitaDaAssociare: { %s };", QString::fromUtf8("old_id:%1, new_id:%2, uscita:%3, nota_cred:%4")
             .arg(uscitaDaAssociare.old_id)
             .arg(uscitaDaAssociare.new_id)
             .arg(uscitaDaAssociare.uscita)
             .arg(uscitaDaAssociare.nota_cred)
             .toLocal8Bit().data()
          );
      const int newIdUscitaDaAssociare = uscitaDaAssociare.new_id;
      queryLite = QString::fromUtf8("SELECT registration FROM transactions WHERE id = %1").arg(newIdUscitaDaAssociare);
      if (!sqlLite.exec(queryLite) || !sqlLite.first())
      {
        qDebug("%s", queryLite.toLocal8Bit().data());
        qDebug("Error select!\n");
        return;
      }

      int registrationOfUscitaDaAssociare = sqlLite.value(0).toInt();
      queryLite = QString::fromUtf8("UPDATE transactions SET registration = %1 WHERE id = %2").arg(registrationOfUscitaDaAssociare).arg(entrata.new_id);
      if (!sqlLite.exec(queryLite))
      {
        qDebug("%s", queryLite.toLocal8Bit().data());
        qDebug("Error update!\n");
        return;
      }
    }
  }

  // Pulisco le registrazioni che ormai son vuote.
  queryLite = QString::fromUtf8("DELETE FROM registrations WHERE id IN (SELECT id FROM\n"
                                "( SELECT * FROM registrations r\n"
                                "LEFT JOIN transactions t ON r.id = t.registration\n"
                                "WHERE t.id IS NULL) a);");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("%s", queryLite.toLocal8Bit().data());
    qDebug("Error delete!\n");
    return;
  }

  queryLite = QString::fromUtf8("DROP TABLE temp;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("%s", queryLite.toLocal8Bit().data());
    qDebug("Error DROP TABLE!\n");
    return;
  }
}

void TTranslator::sistemaTrasferimentiInRegistrations()
{
  QSqlQuery sqlLite(_sqliteDb);
  QString   queryLite = QString::fromUtf8("SELECT t1.id as id1, t2.id as id2, t1.registration as reg1, t2.registration as reg2\n"
                                          "FROM transactions t1\n"
                                          "JOIN transactions t2 ON t1.date = t2.date AND t1.value = -t2.value\n"
                                          "WHERE t1.tag = 'Trasferimento' AND t2.tag = 'Trasferimento' AND t1.value >= 0;");
  typedef struct
  {
    int id1;
    int id2;
    int reg1;
    int reg2;
  } ttrasf;

  std::vector<ttrasf> vect;
  for (bool ok = sqlLite.exec(queryLite) && sqlLite.first(); ok; ok = sqlLite.next())
  {
    ttrasf t = { sqlLite.value(0).toInt(), sqlLite.value(1).toInt(), sqlLite.value(2).toInt(), sqlLite.value(3).toInt() };
    vect.emplace_back(t);
  }

  for (auto it = vect.begin(); it != vect.end(); ++it)
  {
    ttrasf t = *it;
    qDebug("%s", QString::fromUtf8("id1:%1, id2:%2, reg1:%3, reg2:%4").arg(t.id1).arg(t.id2).arg(t.reg1).arg(t.reg2).toLocal8Bit().data());
    ttrasf &rec = *it;
    queryLite = QString::fromUtf8("UPDATE transactions SET registration = %1 WHERE id = %2").arg(rec.reg1).arg(rec.id2);
    if (!sqlLite.exec(queryLite))
    {
      qDebug("%s", queryLite.toLocal8Bit().data());
      qDebug("Error update!\n");
      return;
    }
  }
}

void TTranslator::truncateTable()
{
  QSqlQuery sqlLite(_sqliteDb);
  QString   queryLite = QString("DELETE FROM transactions;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
  queryLite = QString("DELETE FROM registrations;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
  queryLite = QString("DELETE FROM subtags;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
  queryLite = QString("DELETE FROM tags;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
  queryLite = QString("DELETE FROM accounts;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
  queryLite = QString("DELETE FROM users;");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("Error delete: %s", queryLite.toLocal8Bit().data());
    return;
  }
}

bool TTranslator::cleanEmptyRegistrations()
{
  QSqlQuery sqlLite(_sqliteDb);
  QString queryLite = QString::fromUtf8("DELETE FROM registrations WHERE id IN (SELECT id FROM\n"
                                "( SELECT * FROM registrations r\n"
                                "LEFT JOIN transactions t ON r.id = t.registration\n"
                                "WHERE t.id IS NULL) a);");
  if (!sqlLite.exec(queryLite))
  {
    qDebug("%s", queryLite.toLocal8Bit().data());
    qDebug("Error delete!\n");
    return false;
  }
  return true;
}

bool TTranslator::createUserTable()
{
  QSqlQuery sql(_sqliteDb);
  QString query = "INSERT INTO users(id, username, password, email) VALUES (0, 'admin', 'ad.min', 'invalid@mail.invalid');";
  if (!sql.exec(query))
  {
    qDebug("%s", query.toLocal8Bit().data());
    qDebug("Error insert admin user!\n");
    return false;
  }
  return true;
}

void TTranslator::start()
{
  // Connect to DB //////////////////////////////////////////////////////////

  QSettings settings("conf.ini", QSettings::IniFormat);
  settings.beginGroup("SQLSERVER");
  QString address  = settings.value("server_address", QVariant("127.0.0.1")).toString();
  QString port     = settings.value("server_port",    QVariant("1433")).toString();
  QString database = settings.value("database_name",  QVariant("sp_soldi")).toString();
  QString uid      = settings.value("username",       QVariant("sp")).toString();
  uint32_t p[3] = { 0x365a6b60, 0x202f, 0x7d786b7c };

  QString effP;
  for (int i = 4; i > 0; --i)
    effP += ((p[2] >> (i - 1) * 8) & 0xff) ^ 0x19;
  for (int i = 2; i > 0; --i)
    effP += ((p[1] >> (i - 1) * 8) & 0xff) ^ 0x19;
  for (int i = 4; i > 0; --i)
    effP += ((p[0] >> (i - 1) * 8) & 0xff) ^ 0x19;

  qDebug("%s", effP.toLocal8Bit().data());
  QString pwd = settings.value("password", QVariant(effP)).toString();

  QString connectString = QStringLiteral(
              "Driver={SQL Server};"
              "Server=127.0.0.1,1433;"
              "Database=sp_soldi;"
              "Uid=sp;"
              "Pwd=dare96/Cry;");
  //        "SCROLLABLERESULT=true");
  _mssqlDb.setDatabaseName(connectString);
  _sqliteDb.setDatabaseName("C:/Users/simone/newsqlite.db");

  if(!_mssqlDb.open())
  {
    qDebug("MSSQL NOT OPEN: %s", (const char*)_mssqlDb.lastError().driverText().toUtf8());
  }
  else if (!_sqliteDb.open())
  {
    qDebug("SQLITE NOT OPEN: %s", (const char*)_mssqlDb.lastError().driverText().toUtf8());
  }
  else
  {
    truncateTable();
    if (insertAccounts() &&
        insertTags() &&
        insertTransactions())
    {
        sistemaNoteCredInRegistrations();
        sistemaTrasferimentiInRegistrations();
        cleanEmptyRegistrations();
    }
    createUserTable();
  }
}

TTranslator::TTranslator() :
  _mssqlDb(QSqlDatabase::addDatabase("QODBC3", "mssql_conn")),
  _sqliteDb(QSqlDatabase::addDatabase("QSQLITE", "sqlite_conn"))
{
//    emit ready();
}

///////////////////////////////////////////////////////////////////////////////
// db_translation
///////////////////////////////////////////////////////////////////////////////

int billfold0200(int &argc, char *argv[])
{
  QCoreApplication *app;

  bool noGui = false;
  if ((noGui = argc > 2 && strcmp(argv[2], "-no-gui") == 0))
    app = new QCoreApplication(argc, argv);
  else
    app = new QApplication(argc, argv);

  TTranslator w;
  if (noGui)
  {
    w.start();
    qDebug("DB Translator: Translation finish.");
  }
  else if (QMessageBox::question(nullptr, "DB Translator", "Start with translation?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
  {
    w.start();
    QMessageBox::information(nullptr, "DB Translator", "Translation finish");
    return app->exec();
  }
  return EXIT_SUCCESS;
}
