#ifndef BUTTONCONFIRMDEFAULT_H
#define BUTTONCONFIRMDEFAULT_H

#include <QPushButton>

class ButtonConfirmDefault : public QPushButton
{
  Q_OBJECT
public:
  ButtonConfirmDefault(QWidget* parent = nullptr) : QPushButton(parent)
  {
    QString ButtonSS = (R"(/* ButtonConfirmDefault */
ButtonConfirmDefault {
    border-radius: 8px;
    background-color: rgb(0, 102, 204);
    color: #ffffff;
    border: none;
}
ButtonConfirmDefault:hover, ButtonConfirmDefault:focus {
    background-color: #0073e6;
}
ButtonConfirmDefault:pressed {
    background-color: #0053a6;
}
ButtonConfirmDefault:disabled {
    background-color: rgb(218, 222, 229);
    color: rgba(255, 255, 255, 179);
})");
    setStyleSheet(ButtonSS);
  }
};

#endif // BUTTONCONFIRMDEFAULT_H
