#ifndef CHANGEPWDDIALOG_H
#define CHANGEPWDDIALOG_H

#include <QDialog>

namespace Ui {
class ChangePwdDialog;
}

class ChangePwdDialog : public QDialog
{
  Q_OBJECT

private slots:
  void closeEvent(QCloseEvent *) override;

public:
  QString get_newPwdLine()   const;
  QString get_reNewPwdLine() const;

  explicit ChangePwdDialog(QWidget *parent = nullptr);
  ~ChangePwdDialog();

private:
  Ui::ChangePwdDialog *ui;
};

#endif // CHANGEPWDDIALOG_H
