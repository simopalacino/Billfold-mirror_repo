#include "tcustomlineedit.h"
#include <QMessageBox>

void TCustomLineEdit::on_editFinished()
{
  if (_nullable && text().isEmpty())
  {
    QLineEdit::setText("");
    QLineEdit::setPlaceholderText(NULL_STRING);
  }
  else
      setText(text());
}

void TCustomLineEdit::setText()
{
  if (isValid())
    QLineEdit::setText(toString());
  else
  {
    QMessageBox::warning(this, tr("Warning"), _warningMsg, QMessageBox::Ok, QMessageBox::Ok);
    setText(defaultText());
  }
}

void TCustomLineEdit::setNullable(bool nullable, bool setNull)
{
  _nullable = nullable;
  if (setNull)
    QLineEdit::setPlaceholderText(NULL_STRING);
}

TCustomLineEdit::TCustomLineEdit(QWidget *parent) : QLineEdit(parent)
{
  connect(this, &TCustomLineEdit::editingFinished, this, &TCustomLineEdit::on_editFinished);
  _warningMsg = tr("Invalid format");
}
