#include "taccountbuttonlist.h"
#include <QEvent>
#include <QResizeEvent>
#include <QPainter>
#include <QModelIndex>
#include <QMenu>
#include <QDebug>
#include "utils.h"

uint8_t TAccountButtonList::sLeftMargin   = 8;
uint8_t TAccountButtonList::sRightMargin  = 8;
uint8_t TAccountButtonList::sTopMargin    = 8;
uint8_t TAccountButtonList::sBottomMargin = 8;
uint8_t TAccountButtonList::sHMargin      = 4;
uint8_t TAccountButtonList::sVMargin      = 4;

void TAccountButtonList::initSubtotalLabel()
{
  connect(_subtotalLabel, &LabelP::labelPResized, this, [this](QResizeEvent *)
    {
      resize(width(), spaceMin().height());
      posButtons();
    });
  connect(this, &TAccountButtonList::buttonSelectedSignal, _subtotalLabel, [this](int){
      _subtotalLabel->setText("Subtotal: € " + getSubtotal().toString());
      _subtotalLabel->adjustSize();
    });
  connect(this, &TAccountButtonList::selectedAllButtons, this, &TAccountButtonList::setTotalText);
  _subtotalLabel->setStyleSheet("background-color: rgba(0,0,0,0);\n"
"font: bold 20pt #000000;");
  _subtotalLabel->setText("Subtotal: € 0.00");
  _subtotalLabel->setWordWrap(false);
  _subtotalLabel->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
  int x = _leftDiscard + sLeftMargin;
  _subtotalLabel->move(x, 0);
}

void TAccountButtonList::initSelectionButton()
{
  _selectionButton->resize(151, 21);
  _selectionButton->setStyleSheet(QString::fromUtf8("QPushButton {\n"
                                      "    background-color: rgb(0, 102, 204);\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "    border: none;\n"
                                      "    border-radius: 8px;\n"
                                      "}\n"
                                      "QPushButton:disabled {\n"
                                      "    background-color: rgb(218, 222, 229);\n"
                                      "    color: rgba(255, 255, 255, 179);\n"
                                      "}"));
  moveSelectionButton();
  _selectionButton->setObjectName("selectionButton");
  _selectionButton->setText("Select All");
  _selectionButton->setEnabled(false);
  _selectionButton->setIcon(QIcon(":/icons/xwhite.ico"));
  _selectionButton->setIconSize({0, 0});
  connect(_selectionButton, &QAbstractButton::clicked, this, &TAccountButtonList::selectionButtonPressed);
}

void TAccountButtonList::initAdjustButton()
{
  _adjustButton->resize(151, 21);
  _adjustButton->setStyleSheet(QString::fromUtf8("QPushButton {\n"
                                      "    background-color: rgb(0, 102, 204);\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "    border: none;\n"
                                      "    border-radius: 8px;\n"
                                      "}\n"
                                      "QPushButton:disabled {\n"
                                      "    background-color: rgb(218, 222, 229);\n"
                                      "    color: rgba(255, 255, 255, 179);\n"
                                      "}"));
  moveAdjustButton();
  _adjustButton->setObjectName("adjustButton");
  _adjustButton->setText("Adjust Balance");
  _adjustButton->setEnabled(false);
//    _adjustButton->setIcon(QIcon(":/icons/xwhite.ico"));
  _adjustButton->setIconSize({0, 0});
  connect(_adjustButton, &QAbstractButton::clicked, this, &TAccountButtonList::adjustButtonPressed);
}

void TAccountButtonList::moveSubtotalLabel()
{
  int x = _leftDiscard + sLeftMargin;
  _subtotalLabel->move(x, 0);
}

void TAccountButtonList::moveSelectionButton()
{
  QSize sMin = spaceMin();
  int w = width();
//    int h = height();
  if (w < sMin.width())
    w = sMin.width();
  int nRows = _listButtons.size() / _maxPerRow + 1;
  int h = sMin.height();
  h = (h*(2*nRows - 1)) / (2*nRows);  //h - (h / (2*nRows));    // Row center.
  h -= _selectionButton->height() + 4;
//    h += _subtotalLabel->height();
  _selectionButton->move(w - 202, h);   //sMin.height() - 52); // (790, 89)
}

void TAccountButtonList::moveAdjustButton()
{
  QSize sMin = spaceMin();
  int w = width();
//    int h = height();
  if (w < sMin.width())
    w = sMin.width();
  int nRows = _listButtons.size() / _maxPerRow + 1;
  int h = sMin.height();
  h = (h*(2*nRows - 1)) / (2*nRows);  //h - (h / (2*nRows));    // Row center.
  h += 4;
//    h += _subtotalLabel->height();
  _adjustButton->move(w - 202, h);
}

void TAccountButtonList::paintEvent(QPaintEvent *event)
{
  UNUSED(event)

  QPainter p(this);
  p.setPen(Qt::transparent);
  p.setBrush(QColor(0xffffff));
  QSize s = size();
  p.drawRect(QRect(0, 0, s.width(), s.height()));
}

void TAccountButtonList::posButtons()
{
  for (size_t i = 0; i < _listButtons.size(); ++i)
  {
    TAccountButton *button = _listButtons[i];
    int x = _leftDiscard + sLeftMargin + (i % _maxPerRow)  * (button->buttWidth()  + sHMargin);
    int y = _topDiscard  + sTopMargin  + ((i / _maxPerRow) * (button->buttHeight() + sVMargin));
    y += _subtotalLabel->height();
    button->move(QPoint(x, y));
  }
}

QSize TAccountButtonList::spaceMin()
{
  int buttW = 192;
  int buttH = 56;
  if (_listButtons.size() > 0)
  {
    auto * b = _listButtons[0];
    buttW = b->buttWidth();
    buttH = b->buttHeight();
  }

  int width  = sLeftMargin + (_maxPerRow * (buttW + sHMargin) - sHMargin) + sRightMargin;
  int height = sTopMargin + ((buttH + sVMargin) * (_listButtons.size() / _maxPerRow + 1) - sVMargin) + sBottomMargin;
  height += _subtotalLabel->height();
  return { width, height };
}

void TAccountButtonList::buttonRightClick(QPoint &pos)
{
  int idx = getIndexButton(pos);
  if (idx != -1)
  {
    _idxRightClick = idx;
    static QMenu *menu = nullptr;
    if (!menu)
    {
      menu = new QMenu(nullptr);
      static QAction *s = new QAction(tr("Select"), this);
      static QAction *e = new QAction(tr("Edit"), this);
      static QAction *d = new QAction(tr("Delete"), this);
      static QAction *i = new QAction(tr("Info"), this);
      menu->addAction(s);
      menu->addAction(e);
      menu->addAction(d);
      menu->addAction(i);

      connect(s, &QAction::triggered, this, &TAccountButtonList::on_selectAction_triggered);
      connect(e, &QAction::triggered, this, &TAccountButtonList::on_editAction_triggered);
      connect(d, &QAction::triggered, this, &TAccountButtonList::on_deleteAction_triggered);
      connect(i, &QAction::triggered, this, &TAccountButtonList::on_infoAction_triggered);
    }
    menu->popup(mapToGlobal(this->pos()) + pos);
  }
}

void TAccountButtonList::onClickedTitledBtn(QPoint &pos)
{
  int idx = getIndexButton(pos);
  if (idx == -1)
    return;

  buttonSelected(idx);
}

void TAccountButtonList::buttonLongPressed(QPoint &posButton)
{
  int idx = getIndexButton(posButton);
  if (idx == -1)
    return;

  if (!_multiSelection && _nSelected != 1)
    setAllSelected(false);

  _multiSelection  = true;
  _wasLongPressure = true;

  // Anticipation of selection during pression.
  if (!isButtSelected(idx))
  {
    setSelected(idx, true);
    ++_nSelected;
    _selectionButton->setText(QString("%1 selected account%2").arg(_nSelected).arg(_nSelected == 1 ? "" : "s"));
    _selectionButton->setIconSize({ 12, 12 });
    _adjustButton->setEnabled(false);
  }
  _selectionButton->setEnabled(true);
}

void TAccountButtonList::on_deleteAction_triggered()
{
  emit accountRequest(Delete, _idxRightClick);
}

void TAccountButtonList::on_editAction_triggered()
{
  emit accountRequest(Edit, _idxRightClick);
}

void TAccountButtonList::on_infoAction_triggered()
{
  emit accountRequest(Info, _idxRightClick);
}

void TAccountButtonList::on_selectAction_triggered()
{
  _multiSelection = false;
  _selectionButton->setText("Select All");
  _selectionButton->setIconSize({ 0, 0 });
  setAllSelected(false);
  setSelected(_idxRightClick, true);
  _nSelected = 1;
  _selectionButton->setEnabled(true);
  _adjustButton->setEnabled(true);
  emit buttonSelectedSignal(_idxRightClick);
}

void TAccountButtonList::selectionButtonPressed(bool checked)
{
  UNUSED(checked);
  setAllSelected(true);
  _multiSelection = false;
  _nSelected      = 0;
  _selectionButton->setText("Select All");
  _selectionButton->setIconSize({ 0, 0 });
  _selectionButton->setEnabled(false);
  _adjustButton->setEnabled(false);
  emit selectedAllButtons();
}

void TAccountButtonList::adjustButtonPressed(bool checked)
{
  UNUSED(checked);
  QWidget *w = new QWidget(this, Qt::Popup);
  w->resize(100, 100);
  w->move(1000, 200);
  w->show();
}

void TAccountButtonList::resizeEvent(QResizeEvent *event)
{
  QSize s       = spaceMin();
  QSize newSize = event->size();
  newSize.setHeight(rect().height());
  _leftDiscard = (newSize.width() - s.width()) / 2;
  if (_leftDiscard < 0)
    _leftDiscard = 0;
  moveSubtotalLabel();
  moveSelectionButton();
  moveAdjustButton();
  posButtons();
  setMinimumHeight(newSize.height());
  emit accountButtonListResized(event);
}

TAccountButton &TAccountButtonList::addButton(TAccountButton *button)
{
  button->setToggleButton();

  int x = _leftDiscard + sLeftMargin + (_listButtons.size() % _maxPerRow) * (button->buttWidth()  + sHMargin);
  int y = _topDiscard  + sTopMargin  + (_listButtons.size() / _maxPerRow) * (button->buttHeight() + sVMargin);
  y += _subtotalLabel->height();
  button->move(x, y);
  _listButtons.emplace_back(button);
  connect(button, &TAccountButton::rightClickSignal, this, &TAccountButtonList::buttonRightClick);
  connect(button, &TAccountButton::clickedTitledBtn,   this, &TAccountButtonList::onClickedTitledBtn);
  connect(button, &TAccountButton::longPression,     this, &TAccountButtonList::buttonLongPressed);
  resize(sLeftMargin + _maxPerRow * (button->buttWidth() + sHMargin) - sHMargin + sRightMargin + _leftDiscard * 2,
         sTopMargin + (button->buttHeight() + sVMargin) * (_listButtons.size() / _maxPerRow + 1) - sVMargin + sBottomMargin + _subtotalLabel->height());

  moveSubtotalLabel();
  moveSelectionButton();
  moveAdjustButton();

  button->show();
  update();
  setTotalText();
  return (TAccountButton &)*_listButtons.back();
}

TAccountButtonList &TAccountButtonList::clearAll()
{
  for (auto it = _listButtons.begin(); it != _listButtons.end(); ++it)
  {
    delete *it;
    *it = nullptr;
  }
  _listButtons.clear();

  _idxRightClick   = -1;
  _maxPerRow       = 5;
  _multiSelection  = false;
  _nSelected       = 0;
  _wasLongPressure = false;
  initSubtotalLabel();
  initSelectionButton();
  initAdjustButton();

  return *this;
}

int TAccountButtonList::getIndexButton(QString &title)
{
  for (size_t i = 0; i < _listButtons.size(); ++i)
  {
    if (_listButtons[i]->getTitle() == title)
      return i;
  }
  return -1;
}

int TAccountButtonList::getIndexButton(QPoint &pos)
{
  if (_listButtons.size() == 0)
    return -1;

  int width  = _listButtons[0]->buttWidth();
  int height = _listButtons[0]->buttHeight();
  for (size_t i = 0; i < _listButtons.size(); ++i)
  {
    int x = _leftDiscard + sLeftMargin + (i % _maxPerRow) * (width + sHMargin);
    int y = _topDiscard  + sTopMargin  + ((i / _maxPerRow) * (height + sVMargin)) + _subtotalLabel->height();
    QRect rect(x, y, width, height);
    if (rect.contains(pos))
      return i;
  }
  return -1;
}

TAccountButton *TAccountButtonList::getButtonAt(int pos)
{
  if (pos < (int)_listButtons.size())
  {
    return _listButtons.at(pos);
  }
  return nullptr;
}

Database::TNumeric TAccountButtonList::getSubtotal()
{
  Database::TNumeric num(CIFRA_PRECI, CIFRA_SCALE);
  for (auto &it : _listButtons)
  {
    if (it->isSelected() && !it->isVirtual())
    {
      num = num + Database::TNumeric(it->getBalance(), CIFRA_PRECI, CIFRA_SCALE);
    }
  }
  return num;
}

std::vector<int> TAccountButtonList::getIndexSelectedButtons()
{
  std::vector<int> list;
  int pos = 0;
  for (auto it = _listButtons.begin(); it != _listButtons.end(); ++it, ++pos)
  {
    if ((*it)->isSelected())
      list.push_back(pos);
  }
  return list;
}

bool TAccountButtonList::isButtSelected(size_t idx)
{
  return idx <= _listButtons.size() && _listButtons[idx]->isSelected();
}

bool TAccountButtonList::isAllSelected()
{
  bool flag = true;
  for (auto it = _listButtons.begin(); it != _listButtons.end(); ++it)
    flag &= (*it)->isSelected();
  return flag;
}

void TAccountButtonList::setAllSelected(bool state)
{
  for (auto it = _listButtons.begin(); it != _listButtons.end(); ++it)
    (*it)->setSelected(state);
}

void TAccountButtonList::setTotalText()
{
  _subtotalLabel->setText("Total: € " + getSubtotal().toString());
  _subtotalLabel->adjustSize();
}

void TAccountButtonList::buttonSelected(unsigned int idx)
{
  if (!_multiSelection)
  {
    setAllSelected(false);
    setSelected(idx, true);
    _nSelected = 1;
    _selectionButton->setEnabled(true);
    _adjustButton->setEnabled(true);
  }
  else        // multiSelection
  {
    if (!_wasLongPressure)  // Normal path.
    {
      if (_nSelected == 1 && !isButtSelected(idx))
        setSelected(idx, true);     // Ignoro l'azione della release mouse
      else
        isButtSelected(idx) ? ++_nSelected : --_nSelected;
    }
    else    // wasLongPressure - Gia' settato in anticipo.
    {
      _wasLongPressure = false;
      setSelected(idx, !isButtSelected(idx));     // Ignoro l'azione della release mouse
    }
    _selectionButton->setText(QString::fromUtf8("%1 selected account%2").arg(_nSelected).arg(_nSelected == 1 ? "" : "s"));
    _selectionButton->setIconSize({ 12, 12 });
    _adjustButton->setEnabled(false);
  }
  emit buttonSelectedSignal(idx);
}

TAccountButtonList::TAccountButtonList(QWidget *parent)
  : QWidget(parent),
    _idxRightClick(-1),
    _maxPerRow(5),
    _multiSelection(false),
    _nSelected(0),
    _wasLongPressure(false),
    _subtotalLabel(new LabelP(this)),
    _selectionButton(new QPushButton(this)),
    _adjustButton(new QPushButton(this)),
    _leftDiscard(0),
    _topDiscard(0)
{
  initSubtotalLabel();
  initSelectionButton();
  initAdjustButton();

  setMinimumWidth(992);
  parent->setMinimumWidth(992);
}
