#ifndef TSCROLLAREA_H
#define TSCROLLAREA_H

#include <QScrollArea>

class TScrollArea : public QScrollArea
{
  Q_OBJECT

  virtual void resizeEvent(QResizeEvent *event) override;

signals:
  void tabResized(QResizeEvent *event);

public:
  TScrollArea(QWidget *parent = nullptr);
};

#endif // TSCROLLAREA_H
