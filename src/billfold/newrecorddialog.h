#ifndef NEWRECORDDIALOG_H
#define NEWRECORDDIALOG_H

#include <memory>
#include <QDialog>
#include <QDate>
#include <QJsonObject>
#include <engine/irecorditerator.h>
#include "Database/records.h"
#include "tagsdialog.h"

namespace Ui {
class NewRecordDialog;
}

class NewRecordDialog : public QDialog
{
  Q_OBJECT

  enum CheckValidatorRes { ok = 0, emptyCategory, transferSameAccount, amountIsZero };
  Ui::NewRecordDialog *ui;

  std::vector<engine::SIRecord>& _accountsList;
  std::vector<int>               _accountIdList;
  bool                           _accountingDateEdit_dirty;
  bool                           _editMode;
  QString                        _editId;
  Database::RecordType           _type;
  bool                           _validToAddRecord;
  std::unique_ptr<TagsDialog>    _tagsDialog;
  std::map<QString, QStringList> _tagsTree;

  void changeType(Database::RecordType type);
  int  getIdAccountByIndex(int index);
  int  getIndexAccountById(int id);
  void setCategoryButtonDefault();
  void setFieldWarningsAndErrors();
  bool validatorFields();
  void fillAccountsCBoxes(const std::vector<engine::SIRecord>& accountsList);
  void fillCategories(const std::map<QString, QStringList> &tagsTree);

  static bool timeValidatorAndReformatToLocale(QString& str);

private slots:
  void closeEvent(QCloseEvent *event);
  void onOutgoingBtnToggled(bool checked);
  void onIncomeBtnToggled(bool checked);
  void onTransferBtnToggled(bool checked);

  void on_okBtn_clicked(bool);
  void on_cancelBtn_clicked();
  void on_dateEdit_editingFinished();
  void on_timeEdit_editingFinished();
  void on_accountingDateEdit_editingFinished();
  void on_categoryButton_clicked();

  void tagSelectedEvent(QString text, bool isIncoming);

public slots:
  void notifyAccountsListChanged(const std::vector<engine::SIRecord>& accountsList);
  void notifyCategoriesListChanged(const std::map<QString, QStringList>& tagsTree);

signals:
  void insertRecord(engine::SIRecord fields);
  void updateRecord(QString oldId, engine::SIRecord fields);

public:
  Database::RecordType getType()   { return _type; }
  bool                 isValidNewRecord() { return _validToAddRecord; }

//    void setAccounts(Database::account_record_t accounts[], size_t size)
//    { _accounts.assign(accounts, accounts + size); }
  /** Set edit mode to update a record. */
  bool setEditMode(bool editMode, QString idUpdate = "", engine::SIRecord fields = engine::SIRecord(nullptr));
  void preselectAccounts(int index);
  void precompileFields(const engine::SIRecord& fields);

  explicit NewRecordDialog(std::vector<engine::SIRecord>& accountsList, const std::map<QString, QStringList>& tagsTree, QWidget *parent = nullptr);
  ~NewRecordDialog();
};

#endif // NEWRECORDDIALOG_H
