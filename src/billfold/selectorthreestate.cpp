#include "selectorthreestate.h"
#include <QPainter>
#include <QPainterPath>
#include <QColor>

void SelectorThreeState::paintEvent(QPaintEvent *event)
{
  ThreeStateButton::paintEvent(event);

  int h = height();
  {
    // Income Arrow.
    QPainter     painter(this);
    QPainterPath path;
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setBrush(QColor(0x00cc44));
    painter.setPen(QColor(0, 0, 0, 0));
    QPoint p{ 12, 18 + (h - 18)/2 };
    path.moveTo(p);
    p += { 0, -(12 - 3) };
    path.lineTo(p);
    p += { -3, 0 };
    path.lineTo(p);
    p += { 6, -(12 - 3) };
    path.lineTo(p);
    p += { 6, (12 - 3) };
    path.lineTo(p);
    p += { -3, 0 };
    path.lineTo(p);
    p += { 0, (12 - 3) };
    path.lineTo(p);
    path.closeSubpath();
    painter.drawPath(path);
  }
  {
    // Expense Arrow.
    QPainter     painter(this);
    QPainterPath path;
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(QColor(0xdd4400));
    painter.setPen(QColor(0, 0, 0, 0));
    QPoint p{ width() - 12, (h - 18)/2 };
    path.moveTo(p);
    p += { 0, (12 - 3) };
    path.lineTo(p);
    p += { 3, 0 };
    path.lineTo(p);
    p += { -6, (12 - 3) };
    path.lineTo(p);
    p += { -6, -(12 - 3) };
    path.lineTo(p);
    p += { 3, 0 };
    path.lineTo(p);
    p += { 0, -(12 - 3) };
    path.lineTo(p);
    path.closeSubpath();
    painter.drawPath(path);
  }
  {
    // Center Arrow.
    QPainter     painter(this);
    QPainterPath path;
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(QColor(0x0088ff));
    painter.setPen(QColor(0, 0, 0, 0));
    QPoint p{ width()/2 - 8 + 2, h - 3 };
    path.moveTo(p);
    p += { 0, -(12 - 3) };
    path.lineTo(p);
    p += { -2, 0 };
    path.lineTo(p);
    p += { 4, -(12 - 3) };
    path.lineTo(p);
    p += { 4, (12 - 3) };
    path.lineTo(p);
    p += { -2, 0 };
    path.lineTo(p);
    p += { 0, (12 - 3) };
    path.lineTo(p);
    path.closeSubpath();
    painter.drawPath(path);

    p = { width()/2 + 8 - 2, 3 };
    path.moveTo(p);
    p += { 0, (12 - 3) };
    path.lineTo(p);
    p += { 2, 0 };
    path.lineTo(p);
    p += { -4, (12 - 3) };
    path.lineTo(p);
    p += { -4, -(12 - 3) };
    path.lineTo(p);
    p += { 2, 0 };
    path.lineTo(p);
    p += { 0, -(12 - 3) };
    path.lineTo(p);
    path.closeSubpath();
    painter.drawPath(path);
  }
}

void SelectorThreeState::changeColor(ThreeStateButton::State state)
{
  ThreeStateSelectorButton *widget = getSelectorButton();
  QColor   col;
  switch (state)
  {
  case LeftV:   col = { QColor(0, 0xee, 0x44, 255/4) }; break;
  case CenterV: col = { QColor(0, 0x88, 0xdd, 255/2.3) }; break;
  case RightV:  col = { QColor(0xee, 0x44, 0, 255/2.3) }; break;
  }
  widget->changeColor(col);
  widget->update();
}

SelectorThreeState::SelectorThreeState(QWidget *parent) : ThreeStateButton(parent)
{
  connect(this, &ThreeStateButton::stateChanged, this, &SelectorThreeState::changeColor);
  changeColor(_state);
}
