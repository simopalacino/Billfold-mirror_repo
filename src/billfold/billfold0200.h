#ifndef BILLFOLD0200_H
#define BILLFOLD0200_H

#include <QSqlDatabase>

int billfold0200(int &argc, char *argv[]);

class TTranslator
{
  QSqlDatabase _mssqlDb;
  QSqlDatabase _sqliteDb;

  bool insertAccounts();
  bool insertTags();
  bool insertTransactions();
  void sistemaNoteCredInRegistrations();
  void sistemaTrasferimentiInRegistrations();
  void truncateTable();

  bool cleanEmptyRegistrations();
  bool createUserTable();
public:
  void start();

  TTranslator();
};

#endif // BILLFOLD0200_H
