#include "ttablewidget.h"

#include <QMouseEvent>

#include <QDebug>
#include <QMenu>

void TTableWidget::on_recordEditAction()
{
  QTableWidgetItem *itemType = QTableWidget::item(_row, 0);
  QTableWidgetItem *itemId   = QTableWidget::item(_row, 1);
  emit askToEditSignal(EType(itemType->text().toUInt()), itemId->text().toUInt());
}

void TTableWidget::on_recordDeleteAction()
{
  QTableWidgetItem *itemType = QTableWidget::item(_row, 0);
  QTableWidgetItem *itemId   = QTableWidget::item(_row, 1);
  emit askToDeleteSignal(EType(itemType->text().toUInt()), itemId->text().toUInt());
}

void TTableWidget::on_recordDuplicateAction()
{
  QTableWidgetItem *itemType = QTableWidget::item(_row, 0);
  QTableWidgetItem *itemId   = QTableWidget::item(_row, 1);
  emit askToDuplicateSignal(EType(itemType->text().toUInt()), itemId->text().toUInt());
}

void TTableWidget::mousePressEvent(QMouseEvent *event)
{
  if (event && event->button() == Qt::RightButton)
  {
    QPoint pos = mapToParent(event->pos());
    QTableWidgetItem *item = itemAt(pos);
    if (item)
    {
      _row = item->row();
      selectRow(item->row());

      if (!_menu)
      {
        _menu = new QMenu();
        static QAction *editAc = new QAction(tr("Edit"),      _menu);
        static QAction *delAc  = new QAction(tr("Delete"),    _menu);
        static QAction *dupAc  = new QAction(tr("Duplicate"), _menu);
        _menu->addAction(editAc);
        _menu->addAction(delAc);
        _menu->addAction(dupAc);

        connect(editAc, &QAction::triggered, this, &TTableWidget::on_recordEditAction);
        connect(delAc,  &QAction::triggered, this, &TTableWidget::on_recordDeleteAction);
        connect(dupAc,  &QAction::triggered, this, &TTableWidget::on_recordDuplicateAction);
      }
      _menu->popup(viewport()->mapToGlobal(QPoint(0, 0)) + pos);
    }
    event->accept();
  }
  else
    QTableWidget::mousePressEvent(event);
}

TTableWidget::TTableWidget(QWidget *parent)
  : QTableWidget(parent), _menu(nullptr)
{

}

TTableWidget::~TTableWidget()
{
  delete _menu;
  _menu = nullptr;
}
