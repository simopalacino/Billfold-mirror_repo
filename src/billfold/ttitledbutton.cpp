#include "ttitledbutton.h"
#include <QPainter>
#include <QColor>
#include <QMouseEvent>
#include <QLocale>
#include <QApplication>
#include "utils.h"

void TTitledButton::paintOnScreen()
{
  _painter.begin(this);
  _painter.setRenderHint(QPainter::Antialiasing, true);
  QColor c((QRgb)0x0);
  c.setAlpha(0);
  _painter.setPen(c);

  if ((_toggleButton && _selected) || !_toggleButton)
    _painter.setBrush(_colorFill);
  else if ((_toggleButton && !_selected) || !_toggleButton)
    _painter.setBrush(QColor(0xdadee5));     // Grey [RGB(218, 222, 229)]
  _painter.drawRoundedRect(_rect, 4, 4);
  _painter.end();

  drawTitle();
  drawValue();
}

void TTitledButton::drawTitle()
{
  _painter.begin(this);
  _painter.setRenderHint(QPainter::Antialiasing, true);
  _colorText.setAlpha(179);
  _painter.setPen(_colorText);
  _painter.drawText(36 + 2, 8, 120, 20, Qt::AlignLeft | Qt::AlignVCenter, _title);
  _painter.end();
}

void TTitledButton::drawValue()
{
  _painter.begin(this);
  _painter.setRenderHint(QPainter::Antialiasing, true);
  _colorText.setAlpha(255);
  _painter.setPen(_colorText);
  _painter.drawText(36 + 2, 8 + 8 + 8, 120, 20, Qt::AlignLeft | Qt::AlignVCenter, _value);
  _painter.end();
}

void TTitledButton::timeoutPressing()
{
//    _longPressed = true;
  QRect  r = rect();
  QPoint p = mapToParent({r.x(), r.y()});
  emit longPression(p);
}

// protected:

void TTitledButton::paintEvent(QPaintEvent *event)
{
  UNUSED(event)

  paintOnScreen();
}

void TTitledButton::mousePressEvent(QMouseEvent *event)
{
  const bool ctrlPressed = QApplication::keyboardModifiers() & Qt::ControlModifier;

  if (!ctrlPressed)
  {
    if (event->button() == Qt::LeftButton && _toggleButton)
        _timer.start();
    if (event->button() == Qt::RightButton)
    {
      int x = event->x();
      int y = event->y();
      QPoint pparent = mapToParent(event->pos());
      if (x >= 0 && x <= buttWidth() && y >= 0 && y <= buttHeight())
        emit rightClickSignal(pparent);
    }
  }
}

void TTitledButton::mouseReleaseEvent(QMouseEvent *event)
{
  const bool ctrlPressed = QApplication::keyboardModifiers() & Qt::ControlModifier;

  if (event->button() == Qt::LeftButton)
  {
    if (ctrlPressed)
    {
      QRect  r = rect();
      QPoint p = mapToParent({ r.x(), r.y() });
      emit longPression(p);
    }

    _timer.stop();
    int x = event->x();
    int y = event->y();
    QPoint pparent = mapToParent(event->pos());

    if (x >= 0 && x <= buttWidth() && y >= 0 && y <= buttHeight())
    {
      _selected = !_selected;
      emit clickedTitledBtn(pparent);
      update();
    }
  }
}

void TTitledButton::keyPressEvent(QKeyEvent *e)
{ UNUSED(e) }

void TTitledButton::keyReleaseEvent(QKeyEvent *e)
{ UNUSED(e) }

void TTitledButton::focusInEvent(QFocusEvent *e)
{ UNUSED(e) }

// public:

int TTitledButton::buttHeight() const
{
  return _buttH;
}

void TTitledButton::setButtHeight(int newHeight)
{
  _buttH = newHeight;
  resize(_buttW, _buttH);
  update();
}

void TTitledButton::setButtWidth(int newWidth)
{
  _buttW = newWidth;
  resize(_buttW, _buttH);
  update();
}

void TTitledButton::setSelected(bool selected)
{
  _selected = selected;
  update();
}

void TTitledButton::setTitle(QString &title)
{
  _title = title;
  update();
}

void TTitledButton::setTitle(const char *title)
{
  _title = title;
  update();
}

void TTitledButton::setToggleButton(bool set)
{
  _toggleButton = set;
}

void TTitledButton::setValue(QString &value)
{
  _value = value;
  update();
}

TTitledButton::TTitledButton(bool toogleButton, QWidget *parent)
  : QWidget(parent),
    _colorFill((QRgb)0),
    _colorText((QRgb)0),
    _selected(false),
    _toggleButton(toogleButton)
{
  _buttH = 56;
  _buttW = 192;
  _rect.setRect(0, 0, _buttW, _buttH);
  _timer.setInterval(250);
  _timer.setSingleShot(true);
  connect(&_timer, &QTimer::timeout, this, &TTitledButton::timeoutPressing);
//    setStyleSheet("font: 11pt Bold \"Calibri\";");
}

TTitledButton::TTitledButton(QWidget *parent)
  : QWidget(parent),
    _colorFill((QRgb)0),
    _colorText((QRgb)0),
    _selected(false),
    _toggleButton(false)
{
  _buttH = 56;
  _buttW = 192;
  _rect.setRect(0, 0, _buttW, _buttH);
  _timer.setInterval(250);
  _timer.setSingleShot(true);
  connect(&_timer, &QTimer::timeout, this, &TTitledButton::timeoutPressing);
//    setStyleSheet("font: 11pt Bold \"Calibri\";");
}
