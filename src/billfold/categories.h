#ifndef CATEGORIES_H
#define CATEGORIES_H

#include <engine/engine.h>


inline std::map<QString, QStringList> getMappedTags(engine::Libengine* libengine)
{
  engine::SIRecordIterator       tags = libengine->selectCategories();
  std::map<QString, QStringList> list;

  for (tags.next(); tags.isValid(); tags.next())
    list[tags["second"].toString()] << tags["first"].toString();
  return list;
}

#endif // CATEGORIES_H
