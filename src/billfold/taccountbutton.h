#ifndef TACCOUNTBUTTON_H
#define TACCOUNTBUTTON_H

#include <QObject>
#include <QLocale>
#include "ttitledbutton.h"


class TAccountButton : public TTitledButton
{
  Q_OBJECT

  qreal   _balance;
  QString _currencySymbol;

public:
  void setVirtualAccount(bool isVirtual = true) { addMetadata("virtual", QString("%1").arg(isVirtual ? "1" : "0")); }
  void setBalance(qreal balance)
  {
    _balance = balance;
    QString val = QString::fromUtf8("%1%2").arg(_currencySymbol).arg(QLocale().toString(_balance, 'f', 2));
    TTitledButton::setValue(val);
    update();
  }
  void setCurrencySymbol(QString currencySymbol)
  {
    _currencySymbol = currencySymbol;
    update();
  }
  void     setId(uint32_t id) { addMetadata("id", QString("%1").arg(id)); }
  qreal    getBalance()       { return _balance; }
  uint32_t getId()            { return getMetadata("id").toUInt(); }
  bool     isVirtual()        { return getMetadata("virtual").toUInt(); }

  TAccountButton(QWidget *parent = nullptr)
      : TTitledButton(true, parent)
  {
    setButtWidth(192);
    setButtHeight(56);
    setCurrencySymbol("€");
    setBalance(0);
  }
};

#endif // TACCOUNTBUTTON_H
