#ifndef CARDBASE_H
#define CARDBASE_H

#include <QLabel>
#include <QWidget>

class CardBase : public QWidget
{
  Q_OBJECT

  static constexpr int UPPER_SPACE = 4;
  static constexpr int LEFT_SPACE  = 4;

  QLabel *_titleLbl;
  QWidget* _wchild = nullptr;

  int heightTitle();

  // QWidget interface
protected:
  virtual void paintEvent(QPaintEvent *event)   override;
  virtual void resizeEvent(QResizeEvent *event) override;
public:
  void addWidget(QWidget* child);
  void setTitle(QString title);
  explicit CardBase(QWidget *parent = nullptr);
};

#endif // CARDBASE_H
