#include "periodbar.h"
#include "Database/tnumeric.h"
#include "ui_periodbar.h"

#include <FilterPopup.h>
#include <categories.h>

void PeriodBar::resizeEvent(QResizeEvent *event)
{
  QSize areaSize = size();
  // Center periodButton.
  movePeriodButton();
  // Move newRecordBtn -16 from right.
  ui->newRecordBtn->move((areaSize.width() - 16) - ui->newRecordBtn->width(),
                         (areaSize.height() - ui->newRecordBtn->height()) / 2);
  ui->selector->move(16, (areaSize.height() - ui->selector->height()) / 2);
  QWidget::resizeEvent(event);
}

void PeriodBar::movePeriodButton()
{
  QSize areaSize = size();
  QPoint xy = { (areaSize.width() - ui->periodBtn->width()) / 2, (areaSize.height() - ui->periodBtn->height()) / 2 };
  ui->periodBtn->move(xy);
  ui->filtersBtn->move(xy.x() + ui->periodBtn->width() + 4, xy.y());
}

void PeriodBar::onNewRecordBtnClicked()
{
  emit newRecordBtnClicked();
}

void PeriodBar::onSelectorStateChanged()
{
  emit selectorStateChanged();
}

void PeriodBar::onFiltersBtnClicked()
{
  if (_filterPopup == nullptr)
  {
    _filterPopup = std::make_shared<FilterPopup>(this);
    auto mapTags = std::make_shared<std::map<QString, QStringList>>(getMappedTags(_libengine));
    _filterPopup->setCategoriesMap(mapTags);
  }

  if (!_filterPopup->exec())
    return;

  emit filtersChanged(_filterPopup->getFilters());
}

ThreeStateButton::State PeriodBar::getStateSelector()
{
  return ui->selector->getState();
}

void PeriodBar::setStateSelector(ThreeStateButton::State state)
{
  ui->selector->setState(state);
}

void PeriodBar::setSubtot(qreal subtot)
{
  ui->subtotalPeriod->setText("Subtotal Period: €" + Database::TNumeric(subtot, CIFRA_PRECI, CIFRA_SCALE).toString());
}

void PeriodBar::setText(QString str)
{
  QFontMetrics fm(ui->periodBtn->font());
  ui->periodBtn->resize(std::max(fm.size(Qt::TextSingleLine, str).width() + 16, ui->periodBtn->width()), ui->periodBtn->height());
  movePeriodButton();
  ui->periodBtn->setText(str);
}

void PeriodBar::categoriesChanged()
{
  if (_filterPopup == nullptr)
    return;

  auto mapTags = std::make_shared<std::map<QString, QStringList>>(getMappedTags(_libengine));
  _filterPopup->setCategoriesMap(mapTags);
}

PeriodBar::PeriodBar(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::PeriodBar)
{
  ui->setupUi(this);

  connect(ui->newRecordBtn, &QPushButton::clicked,             this, &PeriodBar::onNewRecordBtnClicked);
  connect(ui->selector,     &SelectorThreeState::stateChanged, this, &PeriodBar::onSelectorStateChanged);
  connect(ui->filtersBtn,   &QPushButton::clicked,             this, &PeriodBar::onFiltersBtnClicked);

  setTabOrder(ui->selector,     ui->periodBtn);
  setTabOrder(ui->periodBtn,    ui->filtersBtn);
  setTabOrder(ui->filtersBtn,   ui->newRecordBtn);
}

PeriodBar::~PeriodBar()
{
  delete ui;
}
