#ifndef TAGSSUMMARY_H
#define TAGSSUMMARY_H

#include <QTreeWidgetItem>
#include <QWidget>

namespace Ui {
class TagsSummary;
}

class TagsSummary : public QWidget
{
  Q_OBJECT

  QTreeWidgetItem* _lastParent;

  static QTreeWidgetItemIterator getFirstItemLessThan(QTreeWidget* tree, const qreal amount);
  static void                    putItemSorterByAmountAsc(QTreeWidget* tree, const qreal amount, QTreeWidgetItem* it);

public:
  void addParent(const char* name, qreal amount);
  void addChildToLastParent(const char* name, qreal amount);
  void addToLastParent(qreal amount);

  QTreeWidget* getTreeWidget();
  explicit TagsSummary(QWidget *parent = nullptr);
  ~TagsSummary();

private:
  Ui::TagsSummary *ui;
};

#endif // TAGSSUMMARY_H
