#ifndef ADDCATEGORYDIALOG_H
#define ADDCATEGORYDIALOG_H

#include <QDialog>

#include <engine/irecorditerator.h>

namespace Ui {
class AddCategoryDialog;
}

class AddCategoryDialog : public QDialog
{
  Q_OBJECT

public:
  QString getPrimaryName();
  QString getGroupName();
  void addCategories(engine::SIRecordIterator categories);
  explicit AddCategoryDialog(QWidget *parent = nullptr);
  ~AddCategoryDialog();

private:
  Ui::AddCategoryDialog *ui;
};

#endif // ADDCATEGORYDIALOG_H
