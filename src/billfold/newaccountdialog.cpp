#include "newaccountdialog.h"
#include "ui_newaccountdialog.h"
#include <Database/TNumeric>
#include <QMessageBox>

#include "engine/db/accounts.h"

bool NewAccountDialog::validatorFields()
{
  if (ui->nameEdit->text().isEmpty())
  {
    QMessageBox::warning(this, tr("Warning"), tr("Insert name"));
    return false;
  }
  return true;
}

#define MAP_INSERT_NOT_EMPTY(obj, var_name, string_method) \
{ \
  QString var_name = ui->var_name##Edit->string_method(); \
  if (!var_name.isEmpty()) \
    (obj).insert({ #var_name, var_name }); \
}

void NewAccountDialog::accept()
{
  if (validatorFields())
  {
    std::map<QString, QVariant> obj;
    obj.insert({ ACCOUNTS_NAME,      ui->nameEdit->text()          });
    obj.insert({ ACCOUNTS_CONTANTI,  ui->checkCash->isChecked()    });
    obj.insert({ ACCOUNTS_RISPARMIO, ui->checkSavings->isChecked() });
    obj.insert({ ACCOUNTS_VISIBLE,   ui->checkVisible->isChecked() });
    obj.insert({ ACCOUNTS_VIRTUAL,   ui->checkVirtual->isChecked() });
    obj.insert({ ACCOUNTS_SORTING,   (uint)ui->orderNoEdit->getNumericValue().toDouble() });
    obj.insert({ ACCOUNTS_INIT_VALUE, ui->init_valueEdit->text().toDouble() });
    MAP_INSERT_NOT_EMPTY(obj, owner,      text);
    MAP_INSERT_NOT_EMPTY(obj, banca,      text);

    const QString date_init = ui->date_initEdit->ansi();
    if (!date_init.isEmpty())
      obj.insert({ ACCOUNTS_DATE_INIT, date_init });

    const int color = ( _selectedColor != nullptr ? _selectedColor->name().mid(1) : QString("3399ff") ).toInt(nullptr, 16);
    obj.insert({ ACCOUNTS_COLOR, color });

    if (_editMode)
    {
      obj.insert({ ACCOUNTS_ID, _editId });
      MAP_INSERT_NOT_EMPTY(obj, value, text);
    }

    emit acceptedAccount(obj);
    close();
  }
}

void NewAccountDialog::colorSelectedEvent(const QColor &color)
{
  _selectedColor = std::unique_ptr<QColor>(new QColor(color));
  ui->colorBtn->setText(color.name());
}

void NewAccountDialog::on_colorBtn_clicked()
{
  if (_selectedColor != nullptr)
    _cDialog = std::unique_ptr<QColorDialog>(new QColorDialog(*_selectedColor, this));
  else
    _cDialog = std::unique_ptr<QColorDialog>(new QColorDialog(this));
  connect(_cDialog.get(), &QColorDialog::colorSelected, this, &NewAccountDialog::colorSelectedEvent);
  _cDialog->show();
}

void NewAccountDialog::setEditMode(const engine::SIRecord& record)
{
  _editMode = true;
  setWindowTitle("Edit Account");
  ui->valueEdit->setEnabled(true);
  ui->orderNoEdit->setEnabled(true);

  _editId = record.value(ACCOUNTS_ID).toUInt();
  ui->nameEdit->setText(          record.value(ACCOUNTS_NAME       ).toString());
  ui->ownerEdit->setText(         record.value(ACCOUNTS_OWNER      ).toString());
  ui->bancaEdit->setText(         record.value(ACCOUNTS_BANCA      ).toString());
  ui->date_initEdit->setText(     record.value(ACCOUNTS_DATE_INIT  ).toString());
  ui->valueEdit->setNumeric(      record.value(ACCOUNTS_VALUE      ).toString());
  ui->init_valueEdit->setNumeric( record.value(ACCOUNTS_INIT_VALUE ).toString());
  ui->checkCash->setChecked(      record.value(ACCOUNTS_CONTANTI   ).toBool());
  ui->checkSavings->setChecked(   record.value(ACCOUNTS_RISPARMIO  ).toBool());
  colorSelectedEvent(QColor(QRgb( record.value(ACCOUNTS_COLOR      ).toUInt())));
  ui->checkVirtual->setChecked(   record.value(ACCOUNTS_VIRTUAL    ).toBool());
  ui->checkVisible->setChecked(   record.value(ACCOUNTS_VISIBLE    ).toBool());
  ui->orderNoEdit->setNumeric(QString::number(record.value(ACCOUNTS_SORTING).toUInt()));
}

NewAccountDialog::NewAccountDialog(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::NewAccountDialog)
{
  ui->setupUi(this);
  setWindowTitle("New Account");

  ui->init_valueEdit->setNumeric(Database::TNumeric("0", CIFRA_PRECI, CIFRA_SCALE));
  ui->valueEdit->setNumeric(Database::TNumeric("0", CIFRA_PRECI, CIFRA_SCALE));
  ui->orderNoEdit->setNumeric(Database::TNumeric("0", 4, 0));

  ui->valueEdit->setEnabled(false);
  ui->orderNoEdit->setEnabled(false);
  ui->nameEdit->setFocus();

  connect(ui->okBtn,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
  ui->okBtn->setDefault(true);
}

NewAccountDialog::~NewAccountDialog()
{
  delete ui;
}
