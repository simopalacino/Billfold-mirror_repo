#ifndef NEWACCOUNTDIALOG_H
#define NEWACCOUNTDIALOG_H

#include <memory>

#include <QDialog>
#include <QColorDialog>

#include "engine/irecord.h"


namespace Ui {
class NewAccountDialog;
}

class NewAccountDialog : public QDialog
{
  Q_OBJECT
  Ui::NewAccountDialog *ui;

  std::unique_ptr<QColorDialog> _cDialog;
  std::unique_ptr<QColor>       _selectedColor;
  bool _editMode{ false };
  uint _editId;

  bool validatorFields();

private slots:
  void accept();
  void colorSelectedEvent(const QColor &color);
  void on_colorBtn_clicked();

signals:
  void acceptedAccount(const std::map<QString, QVariant>& record);

public:
  void setEditMode(const engine::SIRecord& record);
  explicit NewAccountDialog(QWidget *parent = nullptr);
  ~NewAccountDialog();
};

#endif // NEWACCOUNTDIALOG_H
