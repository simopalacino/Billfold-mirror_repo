#ifndef TIMEVALIDATOR_H
#define TIMEVALIDATOR_H

#include <QString>
#include <QStringList>


class TimeValidator
{
  QString     _value;
  bool        _valid;
  QStringList _fmts;

public:
  bool        isValid() { return _valid; }
  QString     get()     { return _value; }
  QStringList getFmts() { return _fmts;  }

  /** a 'fmt' is a a valid string format for QTime.  */
  void operator()(QString time, QStringList fmts = {});

  /** a 'fmt' is a a valid string format for QTime.  */
  TimeValidator(QStringList fmts = {});
};

#endif // TIMEVALIDATOR_H
