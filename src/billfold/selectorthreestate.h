#ifndef SELECTORTHREESTATE_H
#define SELECTORTHREESTATE_H

#include "threestatebutton.h"

class SelectorThreeState : public ThreeStateButton
{
  Q_OBJECT
  void paintEvent(QPaintEvent *event) override;
  void changeColor(State state);
public:
  SelectorThreeState(QWidget *parent = nullptr);
};

#endif // SELECTORTHREESTATE_H
