#ifndef THREESTATEBUTTON_H
#define THREESTATEBUTTON_H

#include <QWidget>

namespace Ui {
class ThreeStateButton;
}

class ThreeStateSelectorButton : public QWidget
{
  Q_OBJECT
  friend class ThreeStateButton;
  QColor _color;
  bool   _hasFocus{true};

  void paintEvent(QPaintEvent *event)      override;
  void getFocus()  { _hasFocus = true;  update(); }
  void loseFocus() { _hasFocus = false; update(); }
public:
  void changeColor(QColor color);
  ThreeStateSelectorButton(QWidget *parent = nullptr) : QWidget(parent), _color(0xdddedf) { }
};

class ThreeStateButton : public QWidget
{
  Q_OBJECT
  static constexpr int perc = 12;
  Ui::ThreeStateButton *ui;
public:
  enum State{
    LeftV   = 0,
    CenterV = 1,
    RightV  = 2
  };

private:
  bool  _mouseMoving;
protected:
  State _state;

private:
  void focusInEvent(QFocusEvent *event)      override;
  void focusOutEvent(QFocusEvent *event)     override;
  void keyPressEvent(QKeyEvent *event)       override;
  void mouseMoveEvent(QMouseEvent *event)    override;
  void mousePressEvent(QMouseEvent *event)   override;
  void mouseReleaseEvent(QMouseEvent *event) override;

  void moveSx();
  void moveCx();
  void moveDx();

protected:
  void paintEvent(QPaintEvent *event)        override;
  ThreeStateSelectorButton *getSelectorButton();

signals:
  void stateChanged(State value);

public:
  State getState() { return _state; }
  void  setState(State state);
  explicit ThreeStateButton(QWidget *parent = nullptr);
  ~ThreeStateButton();
};

#endif // THREESTATEBUTTON_H
