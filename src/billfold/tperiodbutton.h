#ifndef TPERIODBUTTON_H
#define TPERIODBUTTON_H

#include <memory>

#include <QPushButton>
#include <QDate>
#include "periodpopup.h"

struct TPeriodData
{
  QDate from;
  QDate to;
  bool  fullMonths;
  bool  singleMonth;
};

class TPeriodButton : public QPushButton
{
  Q_OBJECT
  PeriodPopup *_pop;
  QStringList _values;
private slots:
  virtual void mousePressEvent(QMouseEvent *e) override;
signals:
  void periodChanged(std::shared_ptr<TPeriodData> event);
public:
  TPeriodButton(QWidget *parent = nullptr);
};

#endif // TPERIODBUTTON_H
