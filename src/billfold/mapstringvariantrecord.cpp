#include "mapstringvariantrecord.h"
#include <algorithm>

int MapStringVariantRecord::count() const
{
  return _map.size();
}

QString MapStringVariantRecord::fieldName(int i) const
{
  auto it = _map.begin();
  std::advance(it, i);
  return it->first;
}

QVariant MapStringVariantRecord::value(int i) const
{
  auto it = _map.begin();
  std::advance(it, i);
  return it->second;
}

QVariant MapStringVariantRecord::value(const QString &name) const
{
  auto it = _map.find(name);
  if (it != _map.end())
    return it->second;
  return {};
}

void MapStringVariantRecord::setValue(int i, const QVariant &val)
{
  auto it = _map.begin();
  std::advance(it, i);
  it->second = val;
}

void MapStringVariantRecord::setValue(const QString &name, const QVariant &val)
{
  _map[name] = val;
}

engine::SIRecord MapStringVariantRecord::getCopy() const
{
  IRecord* p = new MapStringVariantRecord(*this);
  return p->getInstance();
}
