<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="43"/>
        <source>version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="62"/>
        <source>Made by Simone Palacino</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="79"/>
        <source>BILLFOLD</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddCategoryDialog</name>
    <message>
        <location filename="addcategorydialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="addcategorydialog.ui" line="47"/>
        <source>Primary Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="addcategorydialog.ui" line="64"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="addcategorydialog.ui" line="123"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="addcategorydialog.ui" line="155"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BillfoldWindow</name>
    <message>
        <location filename="billfoldwindow.ui" line="20"/>
        <source>Billfold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="124"/>
        <source>Boards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="218"/>
        <source>Transactions table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="302"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="311"/>
        <source>Edit</source>
        <translation type="unfinished">Modifica</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="325"/>
        <source>Help</source>
        <translation type="unfinished">Aiuto</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="338"/>
        <source>Exit</source>
        <translation type="unfinished">Esci</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="343"/>
        <source>About</source>
        <translation type="unfinished">Informazioni</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="348"/>
        <source>New Record</source>
        <translation type="unfinished">Nuovo Record</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="351"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="356"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="359"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="364"/>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="369"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="374"/>
        <source>Edit Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="379"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="384"/>
        <source>Add Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="392"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="400"/>
        <source>Show invisible accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.ui" line="408"/>
        <source>Save last tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="374"/>
        <source>Account updated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="379"/>
        <source>Error: No accounts updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="380"/>
        <location filename="billfoldwindow.cpp" line="445"/>
        <location filename="billfoldwindow.cpp" line="458"/>
        <location filename="billfoldwindow.cpp" line="487"/>
        <location filename="billfoldwindow.cpp" line="493"/>
        <location filename="billfoldwindow.cpp" line="506"/>
        <location filename="billfoldwindow.cpp" line="540"/>
        <source>Warning</source>
        <translation type="unfinished">Attenzione</translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="380"/>
        <source>Error during account updating.
No accounts updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="400"/>
        <location filename="billfoldwindow.cpp" line="405"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="400"/>
        <source>You can show your invisible accounts checking the flag on Edit &gt; Show Invisible Accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="405"/>
        <source>You can save the last tab opened checking the flag on Edit &gt; Save Last Tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="416"/>
        <source>Record inserted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="446"/>
        <source>Are you sure to delete this account?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="452"/>
        <source>Account deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="457"/>
        <source>Error: No accounts deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="458"/>
        <source>Error during account elimination.
No accounts deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="483"/>
        <source>Account Added!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="487"/>
        <source>Error: add account failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="494"/>
        <source>Are you sure to delete this record?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="500"/>
        <source>Record deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="505"/>
        <source>Error: No records deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="506"/>
        <source>Error during record elimination.
No records deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="534"/>
        <source>Record updated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="539"/>
        <source>Error: No records update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="540"/>
        <source>Error during record updating.
No records updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="573"/>
        <source>Average: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="573"/>
        <source>Count: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="573"/>
        <source>Sum: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="634"/>
        <source>Refreshing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="640"/>
        <source>Refreshing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="699"/>
        <source>New Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="billfoldwindow.cpp" line="705"/>
        <source>Category inserted!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="changepwddialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="changepwddialog.ui" line="51"/>
        <source>New Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="changepwddialog.ui" line="71"/>
        <source>Retype New Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="changepwddialog.ui" line="131"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="changepwddialog.ui" line="163"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="logindialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="79"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="88"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="95"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="166"/>
        <source>Change Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="192"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="224"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Accounts</source>
        <translation type="vanished">Conti</translation>
    </message>
    <message>
        <source>New Record</source>
        <translation type="vanished">Nuovo Record</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="vanished">Controllo</translation>
    </message>
    <message>
        <source>DASHBOARD</source>
        <translation type="vanished">Cruscotto</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="vanished">Pagina</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Modifica</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Aiuto</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Apri</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Esci</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Opzioni</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Informazioni</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation type="vanished">Ctrl+N</translation>
    </message>
</context>
<context>
    <name>NewAccountDialog</name>
    <message>
        <location filename="newaccountdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="66"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="83"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="100"/>
        <source>Bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="123"/>
        <source>Cash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="136"/>
        <source>Savings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="143"/>
        <source>Initial value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="160"/>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="177"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="194"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="207"/>
        <source>Pick a color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="214"/>
        <source>Order #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="237"/>
        <source>Visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="253"/>
        <source>Virtual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="297"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.ui" line="329"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newaccountdialog.cpp" line="12"/>
        <source>Warning</source>
        <translation type="unfinished">Attenzione</translation>
    </message>
    <message>
        <location filename="newaccountdialog.cpp" line="12"/>
        <source>Insert name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewRecordDialog</name>
    <message>
        <location filename="newrecorddialog.ui" line="20"/>
        <source>New Record</source>
        <translation>Nuovo Record</translation>
    </message>
    <message>
        <source>Expense</source>
        <translation type="vanished">Uscita</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="206"/>
        <source>Income</source>
        <translation>Entrata</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="231"/>
        <location filename="newrecorddialog.cpp" line="32"/>
        <source>Transfer</source>
        <translation>Trasferimento</translation>
    </message>
    <message>
        <source>Amount</source>
        <translation type="vanished">Cifra</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="134"/>
        <source>From account</source>
        <translation>Dal conto</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="181"/>
        <source>Outgoing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="285"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="410"/>
        <source>Causal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="480"/>
        <source>Place</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="493"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="591"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="86"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="613"/>
        <source>ADD TRANSACTION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="647"/>
        <source>Add record</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <source>ADD RECORD</source>
        <translation type="vanished">INSERIMENTO</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Categoria</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="351"/>
        <source>Accounting date</source>
        <translation>Data contabile</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="251"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="newrecorddialog.ui" line="377"/>
        <source>to account</source>
        <translation>al conto</translation>
    </message>
    <message>
        <source>00:00</source>
        <translation type="vanished">00:00</translation>
    </message>
    <message>
        <location filename="newrecorddialog.cpp" line="60"/>
        <source>Select a category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.cpp" line="170"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.cpp" line="170"/>
        <source>The account is virtual
Do you really want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newrecorddialog.cpp" line="349"/>
        <source>Error loading fields.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeriodBar</name>
    <message>
        <location filename="periodbar.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodbar.ui" line="88"/>
        <source>Dic 2020</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodbar.ui" line="104"/>
        <source>New Record</source>
        <translation type="unfinished">Nuovo Record</translation>
    </message>
    <message>
        <location filename="periodbar.ui" line="127"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeriodPopup</name>
    <message>
        <location filename="periodpopup.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodpopup.ui" line="45"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodpopup.ui" line="52"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodpopup.ui" line="85"/>
        <source>Full months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodpopup.ui" line="98"/>
        <source>Single month period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="periodpopup.ui" line="108"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDb</name>
    <message>
        <location filename="SettingsDb.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SettingsDb.ui" line="28"/>
        <source>Database Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SettingsDb.ui" line="35"/>
        <source>Server Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SettingsDb.cpp" line="9"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SettingsDb.cpp" line="9"/>
        <source>Format of input incorrect.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TAccountButtonList</name>
    <message>
        <location filename="taccountbuttonlist.cpp" line="168"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="taccountbuttonlist.cpp" line="169"/>
        <source>Edit</source>
        <translation type="unfinished">Modifica</translation>
    </message>
    <message>
        <location filename="taccountbuttonlist.cpp" line="170"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="taccountbuttonlist.cpp" line="171"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TCustomLineEdit</name>
    <message>
        <location filename="tcustomlineedit.cpp" line="21"/>
        <source>Warning</source>
        <translation type="unfinished">Attenzione</translation>
    </message>
    <message>
        <location filename="tcustomlineedit.cpp" line="36"/>
        <source>Invalid format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TDateEdit</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">Attenzione</translation>
    </message>
    <message>
        <location filename="date.cpp" line="99"/>
        <source>Invalid date format.</source>
        <translation>Formato data invalido.</translation>
    </message>
</context>
<context>
    <name>TTableWidget</name>
    <message>
        <location filename="ttablewidget.cpp" line="36"/>
        <source>Edit</source>
        <translation type="unfinished">Modifica</translation>
    </message>
    <message>
        <location filename="ttablewidget.cpp" line="37"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsDialog</name>
    <message>
        <location filename="tagsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tagsdialog.ui" line="20"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tagsdialog.cpp" line="38"/>
        <source>Incomings (Earnings)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsSummary</name>
    <message>
        <location filename="tagssummary.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tagssummary.ui" line="36"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tagssummary.ui" line="41"/>
        <source>Amount</source>
        <translation type="unfinished">Cifra</translation>
    </message>
</context>
<context>
    <name>ThreeStateButton</name>
    <message>
        <location filename="threestatebutton.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
