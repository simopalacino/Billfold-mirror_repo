#include "tagssummary.h"
#include "ui_tagssummary.h"

#include <Database/tnumeric.h>



QTreeWidgetItemIterator TagsSummary::getFirstItemLessThan(QTreeWidget* tree, const qreal amount)
{
  QTreeWidgetItemIterator it(tree);
  for (; *it && !(!(*it)->parent() && (*it)->data(1, Qt::UserRole).toReal() > amount); ++it)
  { }
  return it;
}

void TagsSummary::putItemSorterByAmountAsc(QTreeWidget* tree, const qreal amount, QTreeWidgetItem* it)
{
  QTreeWidgetItemIterator itL = getFirstItemLessThan(tree, amount);
  if ((*itL) == nullptr)
    tree->addTopLevelItem(it);
  else
  {
    const int idx = tree->indexOfTopLevelItem(*itL);
    if (idx == -1)
      tree->addTopLevelItem(it);
    else
      tree->insertTopLevelItem(idx, it);
  }
}

void TagsSummary::addParent(const char *name, qreal amount)
{
  QTreeWidgetItem* it = new QTreeWidgetItem;
  it->setData(0, Qt::DisplayRole, name);
  it->setData(1, Qt::UserRole,    amount);
  it->setData(1, Qt::DisplayRole, Database::TNumeric(amount, 18, 2).toString());
  it->setTextAlignment(1, Qt::AlignRight);
  putItemSorterByAmountAsc(ui->treeWidget, amount, it);
  _lastParent = it;
}

void TagsSummary::addChildToLastParent(const char *name, qreal amount)
{
  if (_lastParent)
  {
    QTreeWidgetItem* it = new QTreeWidgetItem;
    it->setData(0, Qt::DisplayRole, name);
    it->setData(1, Qt::UserRole,    amount);
    it->setData(1, Qt::DisplayRole, Database::TNumeric(amount, 18, 2).toString());
    it->setTextAlignment(1, Qt::AlignRight);
    _lastParent->addChild(it);
    addToLastParent(amount);
  }
}

void TagsSummary::addToLastParent(qreal amount)
{
  if (_lastParent)
  {
    qreal newA = _lastParent->data(1, Qt::DisplayRole).toReal() + amount;
    _lastParent->setData(1, Qt::UserRole,    newA);
    _lastParent->setData(1, Qt::DisplayRole, Database::TNumeric(newA, 18, 2).toString());
    _lastParent->setTextAlignment(1, Qt::AlignRight);
    QTreeWidgetItem* p = ui->treeWidget->takeTopLevelItem(ui->treeWidget->indexOfTopLevelItem(_lastParent));
    putItemSorterByAmountAsc(ui->treeWidget, newA, p);
  }
}

QTreeWidget* TagsSummary::getTreeWidget()
{
  return ui->treeWidget;
}

TagsSummary::TagsSummary(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TagsSummary)
{
  ui->setupUi(this);
}

TagsSummary::~TagsSummary()
{
  delete ui;
}
