// Header
#include "addcategorydialog.h"
#include "billfoldwindow.h"
#include "categories.h"
#include "mapstringvariantrecord.h"
#include "ui_billfoldwindow.h"
// QtLib
#include <QStatusBar>
#include <QResizeEvent>
#include <QTableView>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QChart>
#include <QChartView>
#include <set>
#include <QtCharts/QPieSeries>

// Other includes
#include "tscrollarea.h"
#include "aboutdialog.h"
#include "utils.h"
#include "json_record.h"
#include "tagssummary.h"

#include <engine/insertnewaccountaction.h>
#include <engine/insertnewcategoryaction.h>
#include <engine/insertnewrecordaction.h>
#include <engine/updaterecordaction.h>
#include <engine/db/accounts.h>

typedef Database::TNumeric numeric;

#define DEFAULT_USER_ID         "1"


namespace BillfoldIni
{
constexpr const char* selector_button = "selector_button";
constexpr const char* state           = "state";
constexpr const char* geometry        = "geometry";
constexpr const char* page_tabs       = "page_tabs";
constexpr const char* period_data     = "period_data";
constexpr const char* table_header    = "tableWidget_HorizzontalHeader_State";
constexpr const char* save_last_tab   = "save_last_tab";
constexpr const char* invisible_accs  = "invisible_accs";

constexpr const char* InfoMsg_invisibleAccounts = "InfoMsg_invisibleAccounts";
constexpr const char* InfoMsg_saveLastPage      = "InfoMsg_saveLastPage";

QByteArray get_default_header_state() { return { "\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\x1\0\0\0\xf\0\0\0\0\0\0\0\0\0\0\0\0\xf\x3\0\0\0\0\x2\0\0\0\0\0\0\0\"\0\0\0\x1\0\0\0.\0\0\x6`\0\0\0\xf\0\x1\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x84\0\0\0\0\0\0\0\xf\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0I\0\0\0\x1\0\0\0\0\0\0\0\x35\0\0\0\x1\0\0\0\0\0\0\0\x80\0\0\0\x1\0\0\0\0\0\0\x1\xf0\0\0\0\x1\0\0\0\0\0\0\0\x96\0\0\0\x1\0\0\0\0\0\0\0I\0\0\0\x1\0\0\0\0\0\0\0\x39\0\0\0\x1\0\0\0\0\0\0\0\x85\0\0\0\x1\0\0\0\0\0\0\0P\0\0\0\x1\0\0\0\0\0\0\0N\0\0\0\x1\0\0\0\0\0\0\0o\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\0\0\0\0" }; }
};




////////////////////////////////////////////////////////////////

void BillfoldWindow::generalRefresh()
{
  refreshAccounts();
  refreshTable();
  refreshTagsSummary();
  setSubtotalPeriod();
}

void BillfoldWindow::refreshAccounts()
{
  const bool maintainSelection = ui->accountsBarList->getSizeList() > 0 && !ui->accountsBarList->isAllSelected();
  std::vector<int> list = ui->accountsBarList->getIndexSelectedButtons();
  std::set<int> setIDs;
  std::map<int /*ID*/, int /*index*/> newMapIDs;  // Salvo l'id con index per dopo.
  for (auto bIndx : list)
  {
    auto *b = ui->accountsBarList->getButtonAt(bIndx);
    setIDs.insert(b->getId());
  }

  _accounts.clear();
  _mapAccounts.clear();
  ui->accountsBarList->clearAll();

  engine::SIRecordIterator it = _libengine->selectAccounts();
  unsigned index = 0;
  for (it.next(); it.isValid(); it.next())
  {
    if (!ui->actionShow_invisible_accounts->isChecked() && !it[ACCOUNTS_VISIBLE].toUInt())
      continue;

    _accounts.push_back(it.get());

    TAccountButton &b = *new TAccountButton(ui->accountsBarList);
    b.setTitle(it[ACCOUNTS_NAME].toString().toLocal8Bit().data());
    b.setObjectName(it[ACCOUNTS_NAME].toString());
    b.setColorText(QColor((QRgb)0xffffff));
    b.setColorFill(QColor((QRgb)it[ACCOUNTS_COLOR].toInt()));
    b.setSelected();
    b.setBalance(it[ACCOUNTS_VALUE].toDouble());
    b.setId(it[ACCOUNTS_ID].toInt());
    b.setVirtualAccount(it[ACCOUNTS_VIRTUAL].toBool());
    ui->accountsBarList->addButton(&b);
    newMapIDs.insert({ it[ACCOUNTS_ID].toInt(), index++ });
  }

  if (maintainSelection)
  {
    // Pesco direttamente l'index con l'id e lo seleziono.
    unsigned count = 0;
    for (auto IDbut : setIDs)
    {
      if (count)
        ui->accountsBarList->setMultiSelection();
      ui->accountsBarList->setSelected(newMapIDs[IDbut]);
      ui->accountsBarList->buttonSelected(newMapIDs[IDbut]);
      ++count;
    }
  }

  emit accountsListChanged(_accounts);
}

void BillfoldWindow::refreshTable()
{
  ui->tableWidget->setRowCount(0);
  auto &barList = *ui->accountsBarList;
  QStringList _buttonSelected;
  for (size_t i = 0; i < barList.getSizeList(); ++i)
  {
    if (barList.isButtSelected(i))
      _buttonSelected.append(QString("%1").arg(barList.getButtonAt(i)->getId()));
  }

  static bool init = true;
  if (init)
  {
    QStringList header;
    header << "type" << "id" << "date" << "cifra" << "category" << "description" << "account" << "acc. date" << "time" << "causal" << "registration" << "regAmount" << "note" << "place" << "currency";
    ui->tableWidget->setColumnCount(header.size());
    for (int i = 0; i < header.size(); ++i)
      ui->tableWidget->setHorizontalHeaderItem(i, new QTableWidgetItem(header.at(i)));
    ui->tableWidget->horizontalHeader()->restoreState(_settings.value(BillfoldIni::table_header, BillfoldIni::get_default_header_state()).toByteArray());
    init = false;
  }

  QString conti;
  for (int i = 0; i < _buttonSelected.size(); ++i)
  {
    conti.append(_buttonSelected.at(i));
    conti.append(',');
  }
  conti = conti.remove(conti.size() - 1, 1);
  engine::SIRecordIterator it = _libengine->selectMovements(_periodFrom.toString(ANSI_DATE_FORMAT), _periodTo.toString(ANSI_DATE_FORMAT), conti);

  ThreeStateButton::State stateBtn = ui->periodBar->getStateSelector();

  int count = 0;
  if (stateBtn == ThreeStateButton::CenterV)  // Insert all.
  {
    for (it.next(); it.isValid(); it.next())
      addRowToTable(it, ui->tableWidget, count);
  }
  else
  {
    bool onlyIncome = stateBtn == ThreeStateButton::LeftV;
    for (it.next(); it.isValid(); it.next())
    {
      if (!(onlyIncome ^ (it.value(REC_TYPE).toUInt() == Database::Income)))
        addRowToTable(it, ui->tableWidget, count);
    }
  }

  ui->tableWidget->hideColumn(0); // Hide 'type' column
  ui->tableWidget->hideColumn(1); // Hide 'id'   column
}

void BillfoldWindow::addRowToTable(engine::SIRecordIterator it, TTableWidget* tableWidget, int& count)
{
  if (!validateRowWFilter(it))
    return;

  int i = 0;
  tableWidget->setRowCount(++count);
  const int c = count - 1;
  uint recType = it.value(REC_TYPE).toUInt();
  /* Hidden columns. */
  tableWidget->setItem(c, i++, new QTableWidgetItem(recType == Database::Outgoing ? "0" : "1"));
  tableWidget->setItem(c, i++, new QTableWidgetItem(it.value(REC_ID  ).toString()));

  tableWidget->setItem(c, i++, new QTableWidgetItem(it.value(REC_DATE).toDate().toString(QLocale().dateFormat(QLocale::ShortFormat))));

  // cifra
  numeric amount(it.value(REC_AMOUNT).toString().toLocal8Bit().data(), CIFRA_PRECI, CIFRA_SCALE);
  if (recType == Database::Outgoing)
    amount.changeSign();
  auto item = new QTableWidgetItem(amount.toString());
  item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableWidget->setItem(c, i++, item);

  tableWidget->setItem(c, i++, new QTableWidgetItem(it.value(REC_TAG        ).toString()));
  tableWidget->setItem(c, i++, new QTableWidgetItem(it.value(REC_DESCRIPTION).toString()));
  // conto
  uint accNum = it.value(REC_CONTO).toInt();
  auto mapIt = _mapAccounts.find(accNum);
  QString con;
  if (mapIt == _mapAccounts.end())
  {
    con = it.value(REC_CONTO).toString();
    auto conto_it = _accounts.begin();
    for (; conto_it != _accounts.end(); ++conto_it)
    {
      if ((*conto_it)[REC_CONTO].toInt() == it.value(REC_CONTO).toInt())
        break;
    }
    if (conto_it != _accounts.end())
    {
      con = QString("[%1] %2").arg(con, (*conto_it)["name"].toString());
      _mapAccounts.insert({ accNum, con });
    }
  }
  else
    con = mapIt->second;
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(con));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_ACC_DATE    ).toDate().toString(QLocale().dateFormat(QLocale::ShortFormat))));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_TIME        ).toDateTime().toString("hh:mm:ss")));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_CAUSAL      ).toString()));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_REGISTRATION).toString()));

  item = new QTableWidgetItem(numeric(it.value(REC_REGAMOUNT).toString().toLocal8Bit().data(), CIFRA_PRECI, CIFRA_SCALE).toString());
  item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableWidget->setItem(count - 1, i++, item);

  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_NOTE  ).toString()));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_PLACE ).toString()));
  tableWidget->setItem(count - 1, i++, new QTableWidgetItem(it.value(REC_CURRENCY).toString()));
}

void BillfoldWindow::refreshTagsSummary()
{
  auto* pie = new QtCharts::QPieSeries;
  ui->catSummary->setTitle("Tags Summary");
  TagsSummary* tsum = new TagsSummary;
  QString query(
R"(SELECT cat.first, cat.second, sum(cifra) as amount FROM (
            select cifra, category from entrate e
            where date >= '%1' and date <= '%2'
            union all
            select -cifra as cifra, category from uscite u
            where date >= '%1' and date <= '%2'
        ) t
join (
    select c.id, c.name as first, c2.name as second
    from categories c
        join categories c2 on c.parent_id = c2.id
    where c.user_id = 2
    union all
    select c.id, c.name, c.name from categories c
    where c.parent_id is null and c.name <> 'Transfer' and c.user_id = %3
) cat on t.category = cat.id
group by cat.first, cat.second
order by cat.second, cat.first)");

  query = query.arg(_periodFrom.toString("yyyy-MM-dd"),
                    _periodTo.toString("yyyy-MM-dd"))
               .arg(2);
  engine::SIRecordIterator cat = _libengine->customQuery(query);
  QString lastParent;
  for (cat.next(); cat.isValid(); cat.next())
  {
    QString first  = cat.value("first").toString();
    QString second = cat.value("second").toString();
    qreal   amount = cat.value("amount").toReal();


    if (lastParent == second)
    {
      if (first != second)
        tsum->addChildToLastParent(first.toLocal8Bit(), amount);
      else
        tsum->addToLastParent(amount);
    }
    else
    {
      if (first == second)
      {
        tsum->addParent(first.toLocal8Bit(), amount);
        lastParent = second;
      }
      else
      {
        tsum->addParent(second.toLocal8Bit(), 0);
        tsum->addChildToLastParent(first.toLocal8Bit(), amount);
        lastParent = second;
      }
    }
  }

  ui->catSummary->addWidget(tsum);
  ui->catSummary->resize(320, 480);


  // PieChart /////////////////////////
  QTreeWidget* tree = tsum->getTreeWidget();
  for (QTreeWidgetItemIterator it(tree); *it; ++it)
  {
    if (!(*it)->parent())
    {
      QString first = (*it)->data(0, Qt::DisplayRole).toString();
      if (first != "Incomings")
        pie->append(first, (*it)->data(1, Qt::UserRole).toReal());
    }
  }

  pie->setLabelsVisible();
  QtCharts::QChart* chart = new QtCharts::QChart;
  chart->addSeries(pie);
  chart->setTitle("Categories Summary");
  chart->legend()->hide();

  QtCharts::QChartView* chartView = new QtCharts::QChartView(chart);
  chartView->setRenderHint(QPainter::Antialiasing);

  ui->piechartCat->addWidget(chartView);
  ui->piechartCat->resize(480, 480);
}

void BillfoldWindow::setPeriod(TPeriodData &data)
{
  QString str;
  if (data.singleMonth || (data.fullMonths && data.from.year() == data.to.year() &&
                                              data.from.month() == data.to.month()))
  {
      _periodFrom  = QDate(data.to.year(), data.to.month(), 1);
      _periodTo    = QDate(data.to.year(), data.to.month(), data.to.daysInMonth());
      str = _periodFrom.toString("MMM yyyy");
  }
  else if (data.fullMonths)
  {
      _periodFrom  = QDate(data.from.year(), data.from.month(), 1);
      _periodTo    = QDate(data.to.year(),   data.to.month(),   data.to.daysInMonth());
      str = _periodFrom.toString("MMM yyyy") + "  -  " + _periodTo.toString("MMM yyyy");
  }
  else
  {
      _periodFrom = data.from;
      _periodTo   = data.to;
      str = _periodFrom.toString(Qt::DefaultLocaleShortDate) + "  -  " + _periodTo.toString(Qt::DefaultLocaleShortDate);
  }
  ui->periodBar->setText(str);
}

bool BillfoldWindow::validateRowWFilter(engine::SIRecordIterator it)
{
  if (_filtersTable == nullptr)
    return true;
  bool ok = true;

  ok &= _filtersTable->descriptionLike.isEmpty() || it.value(REC_DESCRIPTION).toString().contains(_filtersTable->descriptionLike, Qt::CaseInsensitive);
  ok = ok && (!_filtersTable->registrationId.has_value() || it.value(REC_REGISTRATION).toUInt() == _filtersTable->registrationId.value());
  ok = ok && _filtersTable->categories.find(it.value(REC_TAG).toString()) != _filtersTable->categories.end();
  return ok;
}

void BillfoldWindow::setEnabledUndoAction()
{
  ui->actionUndo->setEnabled(true);
}

void BillfoldWindow::updatingAccount(uint i)
{
  _newAccountWin = std::unique_ptr<NewAccountDialog>(new NewAccountDialog(this));
  auto it = _accounts.begin();
  std::advance(it, i);
  _newAccountWin->setEditMode(*it);
  connect(_newAccountWin.get(), &NewAccountDialog::acceptedAccount, this,
          [&](std::map<QString, QVariant> record) {
            engine::SIRecord rec = (new MapStringVariantRecord(record))->getInstance();
            if (_libengine->accountEditRecord(rec, rec))
            {
              ui->statusbar->showMessage(tr("Account updated!"), 7500);
              generalRefresh();
            }
            else
            {
              ui->statusbar->showMessage(tr("Error: No accounts updated"), 7500);
              QMessageBox::warning(this, tr("Warning"), tr("Error during account updating.\nNo accounts updated"));
            }
          });
  _newAccountWin->exec();
}

std::unique_ptr<NewRecordDialog> BillfoldWindow::getNewRecordWin()
{
  auto win = std::unique_ptr<NewRecordDialog>(new NewRecordDialog(_accounts, getMappedTags(_libengine), nullptr));
  connect(this, &BillfoldWindow::accountsListChanged,   win.get(), &NewRecordDialog::notifyAccountsListChanged);
  connect(this, &BillfoldWindow::categoriesListChanged, win.get(), &NewRecordDialog::notifyCategoriesListChanged);
  connect(win.get(), &NewRecordDialog::insertRecord, this, &BillfoldWindow::addNewRegistration);
  connect(win.get(), &NewRecordDialog::updateRecord, this, &BillfoldWindow::updateRegistration);
  return win;
}

void BillfoldWindow::showInformations()
{
  if (!_settings.value(BillfoldIni::InfoMsg_invisibleAccounts, false).toBool())
  {
    QMessageBox::information(this, tr("Info"), tr("You can show your invisible accounts checking the flag on Edit > Show Invisible Accounts."));
    _settings.setValue(BillfoldIni::InfoMsg_invisibleAccounts, true);
  }
  if (!_settings.value(BillfoldIni::InfoMsg_saveLastPage, false).toBool())
  {
    QMessageBox::information(this, tr("Info"), tr("You can save the last tab opened checking the flag on Edit > Save Last Tab."));
    _settings.setValue(BillfoldIni::InfoMsg_saveLastPage, true);
  }
}

void BillfoldWindow::addNewRegistration(engine::SIRecord fields)
{
  int ret = performAction<engine::InsertNewRecordAction>(fields);
  _newRecordWin->close();
  if (ret == 0)
  {
    ui->statusbar->showMessage(tr("Record inserted!"), 7500);
    generalRefresh();
  }
}

void BillfoldWindow::accountSelectedEvent(int i)
{
  (void)i;
  refreshTable();
  refreshTagsSummary();
  setSubtotalPeriod();
}

void BillfoldWindow::closeEvent(QCloseEvent *event)
{
  (void)event;
  if (_newRecordWin.get())
    _newRecordWin->close();
}

void BillfoldWindow::accountRequestEvent(TAccountButtonList::Request type, uint i)
{
  (void)i;
  appLog("[%s:%d] request:%d, i:%d", __FUNCTION__, __LINE__, type, i);

  switch (type)
  {
  case TAccountButtonList::Delete:
    if (QMessageBox::warning(this,
                             tr("Warning"),
                             tr("Are you sure to delete this account?"),
                             QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
            == QMessageBox::Yes)
    {
      if (_libengine->deleteAccount(ui->accountsBarList->getButtonAt(i)->getId()))
      {
        ui->statusbar->showMessage(tr("Account deleted"), 7500);
        generalRefresh();
      }
      else
      {
        ui->statusbar->showMessage(tr("Error: No accounts deleted"), 7500);
        QMessageBox::warning(this, tr("Warning"), tr("Error during account elimination.\nNo accounts deleted"));
      }
    }
    break;
  case TAccountButtonList::Edit:
    updatingAccount(i);
    break;
  case TAccountButtonList::Info:
    break;
  }
}

QString mapToString(const std::map<QString, QVariant>& record)
{
  QString str;
  for (auto it = record.begin(); it != record.end(); ++it)
    str += it->first + " = " + it->second.toString() + "\n";
  return str;
}

void BillfoldWindow::newAccountEvent(const std::map<QString, QVariant>& record)
{
  appLog("[%s:%d] newAccount = %s", __FUNCTION__, __LINE__, mapToString(record).toStdString().c_str());
  if (performAction<engine::InsertNewAccountAction>(record) == 0)
  {
    ui->statusbar->showMessage(tr("Account Added!"), 7500);
    refreshAccounts();
  }
  else
    QMessageBox::warning(this, tr("Warning"), tr("Error: add account failed."));
}

void BillfoldWindow::on_deletingRecord(QString idToDelete)
{
  if (QMessageBox::warning(this,
                         tr("Warning"),
                         tr("Are you sure to delete this record?"),
                         QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        == QMessageBox::Yes)
  {
    if (_libengine->deleteRecord(idToDelete))
    {
      ui->statusbar->showMessage(tr("Record deleted"), 7500);
      generalRefresh();
    }
    else
    {
      ui->statusbar->showMessage(tr("Error: No records deleted"), 7500);
      QMessageBox::warning(this, tr("Warning"), tr("Error during record elimination.\nNo records deleted"));
    }
  }
}

void BillfoldWindow::on_updatingRecord(QString idToUpdate)
{
  _updateRecordWin = getNewRecordWin();
  engine::SIRecord s_rec = _libengine->selectRecord(idToUpdate);
  if (_updateRecordWin->setEditMode(true, idToUpdate, s_rec))
  {
    _updateRecordWin->open();
    _updateRecordWin->raise();
  }
}

void BillfoldWindow::on_periodBtn_periodChanged(std::shared_ptr<TPeriodData> values)
{
  setPeriod(*values.get());
  refreshTable();
  refreshTagsSummary();
  setSubtotalPeriod();
}

void BillfoldWindow::updateRegistration(QString oldId, engine::SIRecord fields)
{
  if (performAction<engine::UpdateRecordAction>(oldId, fields) == 0)
  {
    ui->statusbar->showMessage(tr("Record updated!"), 7500);
    _updateRecordWin->close();
  }
  else
  {
    ui->statusbar->showMessage(tr("Error: No records update"), 7500);
    QMessageBox::warning(this, tr("Warning"), tr("Error during record updating.\nNo records updated"));
  }
  generalRefresh();
}

void BillfoldWindow::resizeWidgetTableTab(QResizeEvent *event)
{
  int newWidth  = event->size().width();
  int newHeight = event->size().height();

  ui->scrollAreaTableTab->resize(newWidth, newHeight);
  ui->tableWidget->resize(newWidth, newHeight);
}

void BillfoldWindow::refreshCifraStatusbarInfos()
{
  int     count   = 0;
  double  average = 0;
  numeric sum(CIFRA_PRECI, CIFRA_SCALE);

  QList<QTableWidgetItem*> listItem = ui->tableWidget->selectedItems();
  if (listItem.size())
  {
    foreach (QTableWidgetItem* item, listItem)
    {
      numeric num(item->text().toLocal8Bit().data(), CIFRA_PRECI, CIFRA_SCALE);
      if (num.isValid())
      {
        average = count ? ((average * count) + num.toDouble()) / (count+1) : num.toDouble();
        sum = sum + num;
        count++;
      }
    }
    _statusbarCifraLabel->setText(tr("Average: ") + QString::number(average) + "     " + tr("Count: ") + QString::number(count) + "     " + tr("Sum: ") + sum.toString());
  }
  else
    _statusbarCifraLabel->setText("");
}

void BillfoldWindow::setSubtotalPeriod()
{
  auto &barList = *ui->accountsBarList;
  QStringList _buttonSelected;
  for (size_t i = 0; i < barList.getSizeList(); ++i)
  {
    if (barList.isButtSelected(i))
      _buttonSelected.append(QString("%1").arg(barList.getButtonAt(i)->getId()));
  }
  QString conti;
  for (int i = 0; i < _buttonSelected.size(); ++i)
  {
    conti.append(_buttonSelected.at(i));
    conti.append(',');
  }
  conti = conti.remove(conti.size() - 1, 1);
  engine::SIRecordIterator it = _libengine->selectMovements(_periodFrom.toString(ANSI_DATE_FORMAT), _periodTo.toString(ANSI_DATE_FORMAT), conti);

  ThreeStateButton::State stateBtn = ui->periodBar->getStateSelector();

  qreal subtot = 0;
  if (stateBtn == ThreeStateButton::CenterV)  // Insert all.
  {
    for (it.next(); it.isValid(); it.next())
    {
      if (it.value(REC_TAG).toString() != "Transfer")
      {
        qreal r = it.value(REC_AMOUNT).toReal();
        subtot += it.value(REC_TYPE).toUInt() == Database::Income ? r : -r;
      }
    }
  }
  else
  {
    bool onlyIncome = stateBtn == ThreeStateButton::LeftV;
    for (it.next(); it.isValid(); it.next())
    {
      if (it.value(REC_TAG).toString() != "Transfer" && !(onlyIncome ^ (it.value(REC_TYPE).toUInt() == Database::Income)))
      {
        qreal r = it.value(REC_AMOUNT).toReal();
        subtot += it.value(REC_TYPE).toUInt() == Database::Income ? r : -r;
      }
    }
  }
  ui->periodBar->setSubtot(subtot);
}

/////////////////////////////////////////////
// Actions

void BillfoldWindow::on_actionRefresh_triggered()
{
  static QLabel *w = nullptr;
  if (!w)
  {
    w = new QLabel(tr("Refreshing"), 0, Qt::Popup);
    w->setFrameStyle(QLabel::WinPanel | QLabel::Raised);
    w->setAlignment(Qt::AlignCenter);
    w->resize(192, 32);
    w->setStyleSheet(QString::fromUtf8("background-color: #ffffff; color: #000000; font: Bold 16pt;"));
  }
  ui->statusbar->showMessage(tr("Refreshing..."));
  w->move(rect().center() + pos() - w->rect().center());
  w->show();
  qApp->processEvents();

  generalRefresh();

  w->close();
  ui->statusbar->showMessage("");
}

void BillfoldWindow::on_actionLogout_triggered()
{
  _logout = true;
  close();
}

void BillfoldWindow::on_actionExit_triggered()
{
  close();
}

void BillfoldWindow::on_actionUndo_triggered()
{
  std::shared_ptr<engine::IAction> action = _actions.pop();
  if (action->undo() == 0)
  {
    generalRefresh();
    emit categoriesListChanged(getMappedTags(_libengine));
  }
  if (_actions.empty())
    ui->actionUndo->setEnabled(false);
}

void BillfoldWindow::on_actionNew_Record_triggered()
{
  _newRecordWin = getNewRecordWin();

  _newRecordWin->setEditMode(false);
  if (ui->accountsBarList->isInMultiSelection() && ui->accountsBarList->getNSelected() == 1)
  {
    const int posAccountSelected = ui->accountsBarList->getIndexSelectedButtons()[0];
    _newRecordWin->preselectAccounts(posAccountSelected);
  }
  _newRecordWin->open();
  _newRecordWin->raise();
  _newRecordWin->setFocus();
}

void BillfoldWindow::on_actionAdd_Account_triggered()
{
  _newAccountWin = std::unique_ptr<NewAccountDialog>(new NewAccountDialog(this));
  connect(_newAccountWin.get(), &NewAccountDialog::acceptedAccount, this, &BillfoldWindow::newAccountEvent);
  _newAccountWin->show();
}

void BillfoldWindow::on_actionAdd_Category_triggered()
{
  AddCategoryDialog dlg(this);
  dlg.setWindowTitle(tr("New Category"));
  engine::SIRecordIterator accs = _libengine->selectCategories();
  dlg.addCategories(accs);
  if (dlg.exec())
  {
    if (performAction<engine::InsertNewCategoryAction>(dlg.getPrimaryName(), dlg.getGroupName()) == 0)
      ui->statusbar->showMessage(tr("Category inserted!"), 7500);
  }
}

void BillfoldWindow::on_actionShow_invisible_accounts_triggered(bool)
{
  generalRefresh();
}

void BillfoldWindow::on_actionAbout_triggered()
{
  AboutDialog aboutWin(this);
  aboutWin.setVersionNum(QString(version));
  aboutWin.exec();
}

void BillfoldWindow::on_recordEditAction(TTableWidget::EType type, uint id)
{
  on_updatingRecord(QString("%1%2").arg(type == TTableWidget::EType::Income ? "e" : "u").arg(id));
}

void BillfoldWindow::on_recordDeleteAction(TTableWidget::EType type, uint id)
{
  on_deletingRecord(QString("%1%2").arg(type == TTableWidget::EType::Income ? "e" : "u").arg(id));
}

void BillfoldWindow::on_recordDuplicateAction(TTableWidget::EType type, uint id)
{
  _newRecordWin = getNewRecordWin();

  _newRecordWin->setEditMode(false);
  if (ui->accountsBarList->isInMultiSelection() && ui->accountsBarList->getNSelected() == 1)
  {
    const int posAccountSelected = ui->accountsBarList->getIndexSelectedButtons()[0];
    _newRecordWin->preselectAccounts(posAccountSelected);
  }
  _newRecordWin->precompileFields(_libengine->selectRecord(QString("%1%2").arg(type == TTableWidget::EType::Income ? "e" : "u").arg(id)));
  _newRecordWin->open();
  _newRecordWin->raise();
  _newRecordWin->setFocus();
}

void BillfoldWindow::resizeWidth(QWidget *widget, int newWidth)
{
  widget->resize(newWidth, widget->size().height());
}

BillfoldWindow::BillfoldWindow(engine::Libengine *libengine, QWidget *parent)
  : QMainWindow(parent),
    ui(new Ui::BillfoldWindow),
    _libengine(libengine),
    _newRecordWin(nullptr),
    _settings("billfold.ini", QSettings::IniFormat)
{
  ui->setupUi(this);

  _settings.beginGroup("Main");
  _statusbarCifraLabel = new QLabel;
  ui->statusbar->addPermanentWidget(_statusbarCifraLabel);

  // Actions Menu Bar
  ui->actionUndo->setShortcut(QKeySequence::Undo);
  ui->actionUndo->setEnabled(false);
  ui->actionShow_invisible_accounts->setCheckable(true);

  // Restore geometry/state.
  restoreGeometry(_settings.value(BillfoldIni::geometry).toByteArray());
  restoreState(_settings.value(BillfoldIni::state).toByteArray());
  int selector = _settings.value(BillfoldIni::selector_button, ThreeStateButton::CenterV).toUInt();
  ui->periodBar->setStateSelector((ThreeStateButton::State)selector);
  ui->actionSave_last_tab->setChecked(_settings.value(BillfoldIni::save_last_tab, false).toBool());
  if (ui->actionSave_last_tab->isChecked())
    ui->tabWidget->setCurrentIndex(_settings.value(BillfoldIni::page_tabs, 0).toUInt());
  ui->actionShow_invisible_accounts->setChecked(_settings.value(BillfoldIni::invisible_accs, false).toBool());


  connect(ui->scrollAreaTableTab,    &TScrollArea::tabResized,            this, &BillfoldWindow::resizeWidgetTableTab);
  connect(ui->tableWidget,           &QTableWidget::itemSelectionChanged, this, &BillfoldWindow::refreshCifraStatusbarInfos);
  connect(ui->periodBar,             &PeriodBar::newRecordBtnClicked,     this, &BillfoldWindow::on_actionNew_Record_triggered);


  /* accountsBarList */
  connect(ui->accountsBarList,  &TAccountButtonList::buttonSelectedSignal,     this, &BillfoldWindow::accountSelectedEvent);
  connect(ui->accountsBarList,  &TAccountButtonList::selectedAllButtons,       this, &BillfoldWindow::refreshTable);
  connect(ui->accountsBarList,  &TAccountButtonList::selectedAllButtons,       this, &BillfoldWindow::refreshTagsSummary);
  connect(ui->accountsBarList,  &TAccountButtonList::selectedAllButtons,       this, &BillfoldWindow::setSubtotalPeriod);
  connect(ui->accountsBarList,  &TAccountButtonList::accountRequest,           this, &BillfoldWindow::accountRequestEvent);


  // SET PERIOD DATA //////////////////////////////////////////////////////////

  QStringList _values;
  _values = _settings.value(BillfoldIni::period_data, QStringList{}).toStringList();
  if (_values.empty())
  {
    QDate today = QDate::currentDate();
    _values.append(QDate(today.year(), today.month(), 1).toString("yyyy-MM-dd"));
    _values.append(today.toString("yyyy-MM-dd"));
    _values.append(QString::number(true));
    _values.append(QString::number(false));
    _settings.setValue(BillfoldIni::period_data, _values);
    _settings.sync();
  }
  TPeriodData pd;
  pd.from        = QDate::fromString(_values[0], "yyyy-MM-dd");
  pd.to          = QString::fromLatin1(_values[1].toLatin1()) == "0" ? QDate::currentDate() :
                                                                       QDate::fromString(_values[1].toLatin1(), "yyyy-MM-dd");
  pd.fullMonths  = _values[2].toUInt();
  pd.singleMonth = _values[3].toUInt();
  setPeriod(pd);

  generalRefresh();
  setSubtotalPeriod();

  /////////////////////////////////////////////////////////////////////////////

  ui->periodBar->setLibengine(_libengine);
  connect(ui->periodBar, &PeriodBar::selectorStateChanged, this, [this]{
    refreshTable();
    setSubtotalPeriod();
  });
  connect(ui->periodBar, &PeriodBar::filtersChanged, this, [this](std::shared_ptr<TFilters> filters) {
    _filtersTable = filters;
    refreshTable();
  });
  connect(this, &BillfoldWindow::categoriesListChanged, [this]{
    ui->periodBar->categoriesChanged();
  });

  setTabOrder(ui->periodBar, ui->tableWidget);
  ui->periodBar->setFocus();

  connect(ui->tableWidget, &TTableWidget::askToEditSignal,      this, &BillfoldWindow::on_recordEditAction);
  connect(ui->tableWidget, &TTableWidget::askToDeleteSignal,    this, &BillfoldWindow::on_recordDeleteAction);
  connect(ui->tableWidget, &TTableWidget::askToDuplicateSignal, this, &BillfoldWindow::on_recordDuplicateAction);

  // Single async operation.
  _showInformationsSingleShot = new QTimer(this);
  _showInformationsSingleShot->setSingleShot(true);
  connect(_showInformationsSingleShot, &QTimer::timeout, this, [this](){
      showInformations();
    });
  _showInformationsSingleShot->start(0);
}

BillfoldWindow::~BillfoldWindow()
{
    _settings.setValue(BillfoldIni::geometry, saveGeometry());
    _settings.setValue(BillfoldIni::state,    saveState());
    _settings.setValue(BillfoldIni::selector_button, (uint)ui->periodBar->getStateSelector());
    _settings.setValue(BillfoldIni::table_header, ui->tableWidget->horizontalHeader()->saveState());
    if (ui->actionSave_last_tab->isChecked())
      _settings.setValue(BillfoldIni::page_tabs, ui->tabWidget->currentIndex());
    _settings.setValue(BillfoldIni::save_last_tab,  ui->actionSave_last_tab->isChecked());
    _settings.setValue(BillfoldIni::invisible_accs, ui->actionShow_invisible_accounts->isChecked());

    _settings.sync();
    delete _showInformationsSingleShot;
    delete ui;
}
