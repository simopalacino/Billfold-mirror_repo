#ifndef TACCOUNTBUTTONLIST_H
#define TACCOUNTBUTTONLIST_H

#include <cstdint>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QMouseEvent>
#include <QVBoxLayout>
#include "taccountbutton.h"
#include "Database/TNumeric"

class LabelP : public QLabel
{
  Q_OBJECT
  virtual void resizeEvent(QResizeEvent *event)
  {
    QLabel::resizeEvent(event);
    emit labelPResized(event);
  }
signals:
  void labelPResized(QResizeEvent *event);
public:
  LabelP(QWidget *parent = nullptr) : QLabel(parent) { }
};

class TAccountButtonList : public QWidget
{
  Q_OBJECT

public:
  enum Request {
    Delete,
    Edit,
    Info
  };

private:
  int                          _idxRightClick;
  std::vector<TAccountButton*> _listButtons;
  uint8_t                      _maxPerRow;
  bool                         _multiSelection;
  int                          _nSelected;    // Selezionati in multiselection
  bool                         _wasLongPressure;

  LabelP      *_subtotalLabel;
  QPushButton *_selectionButton;
  QPushButton *_adjustButton;

  int _leftDiscard;
  int _topDiscard;

  static uint8_t sLeftMargin;
  static uint8_t sRightMargin;
  static uint8_t sTopMargin;
  static uint8_t sBottomMargin;
  static uint8_t sHMargin;
  static uint8_t sVMargin;

  void  initSubtotalLabel();
  void  initSelectionButton();
  void  initAdjustButton();
  void  moveSubtotalLabel();
  void  moveSelectionButton();
  void  moveAdjustButton();
  void  paintEvent(QPaintEvent *event) override;
  void  posButtons();
  QSize spaceMin();

signals:
  // RightClick Action Requests signals
  void accountRequest(Request Edit, uint i);

  void accountButtonListResized(QResizeEvent *event);
  void buttonSelectedSignal(int i);
  void selectedAllButtons();

private slots:
  void buttonRightClick(QPoint &pos);
  void onClickedTitledBtn(QPoint &pos);
  void buttonLongPressed(QPoint &posButton);
  // RightClick Actions slots
  void on_deleteAction_triggered();
  void on_editAction_triggered();
  void on_infoAction_triggered();
  void on_selectAction_triggered();

  void selectionButtonPressed(bool checked = false);
  void adjustButtonPressed(bool checked = false);
  void resizeEvent(QResizeEvent *event) override;

public:
  TAccountButton     &addButton(TAccountButton *button);
  TAccountButtonList &clearAll();

  int                getIndexButton(QString &title);
  int                getIndexButton(QPoint &pos);
  TAccountButton    *getButtonAt(int pos);
  Database::TNumeric getSubtotal();
  int                getNSelected() { return _nSelected; }
  std::vector<int>   getIndexSelectedButtons();
  bool               isButtSelected(size_t idx);
  bool               isAllSelected();
  bool               isInMultiSelection() { return _multiSelection; }

  void setAllSelected(bool state = true);
  void setMaxPerRow(uint8_t max = 0) { _maxPerRow = max; }
  void setMultiSelection(bool flag = true) { _multiSelection = flag; }
  void setSelected(size_t idx, bool state = true) { _listButtons[idx]->setSelected(state); }
  void setTotalText();
  void buttonSelected(unsigned idx);  /** idx must be positive. */

  size_t getSizeList() { return _listButtons.size(); }

  explicit TAccountButtonList(QWidget *parent = nullptr);
};

#endif // TACCOUNTBUTTONLIST_H
