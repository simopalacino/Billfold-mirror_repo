#include "tscrollarea.h"

void TScrollArea::resizeEvent(QResizeEvent *event)
{
  emit tabResized(event);
  QScrollArea::resizeEvent(event);
}

TScrollArea::TScrollArea(QWidget *parent) : QScrollArea(parent)
{  }
