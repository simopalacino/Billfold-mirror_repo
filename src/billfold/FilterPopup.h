#ifndef FILTERPOPUP_H
#define FILTERPOPUP_H

#include <QDialog>
#include <QTreeWidgetItem>
#include <optional>
#include <set>

namespace Ui {
class FilterPopup;
}

struct TFilters
{
  QString descriptionLike;
  std::set<QString> categories;
  std::optional<uint> registrationId;
};

class FilterPopup : public QDialog
{
  Q_OBJECT

  std::shared_ptr<TFilters> _filters;

  std::shared_ptr<std::map<QString, QStringList>> _tagsTree;
  void setTags();
  void checkAllItem(QTreeWidgetItem* item, Qt::CheckState checkState = Qt::Checked);
  std::set<QString> getListCategories(QTreeWidgetItem* item);

private slots:
  void onSelectAllBtnClicked();
  void onDeselectAllBtnClicked();

public:
  std::shared_ptr<TFilters> getFilters();
  void setCategoriesMap(std::shared_ptr<std::map<QString, QStringList>> tagsTree);
  explicit FilterPopup(QWidget *parent = nullptr);
  ~FilterPopup();

private:
  Ui::FilterPopup *ui;
};

#endif // FILTERPOPUP_H
