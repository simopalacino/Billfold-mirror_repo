#include "tnumericedit.h"
#include <QMessageBox>

using namespace Database;


void TNumericEdit::setNumeric(const QString &str)
{
  _num->set(str.toLocal8Bit().data());
  TCustomLineEdit::setText();
}

void TNumericEdit::setNumeric(const Database::TNumeric &numeric)
{
  *_num = numeric;
  TCustomLineEdit::setText();
}

void TNumericEdit::setPrecisionScale(uint8_t precision, uint8_t scale)
{
  if (!TNumeric::validPrecisionScale(precision, scale))
  {
    precision = 18;
    scale     = 0;
  }
  delete _num;
  _num = new TNumeric(precision, scale);
  setNumeric(QString("0"));
}

TNumericEdit::TNumericEdit(QWidget *parent) : TCustomLineEdit(parent), _num(new Database::TNumeric())
{
  _warningMsg = "Invalid numeric format.";
  setPrecisionScale();
}

TNumericEdit::~TNumericEdit()
{
  delete _num;
#ifndef QT_DEBUG
  _num = nullptr;
#endif
}


QString TNumericEdit::defaultText()
{
  return _num->set("0").toString();
}

bool TNumericEdit::isValid()
{
  return _num->isValid();
}

void TNumericEdit::setText(const QString &str)
{
  setNumeric(str);
}

QString TNumericEdit::toString()
{
  return _num->toString();
}
