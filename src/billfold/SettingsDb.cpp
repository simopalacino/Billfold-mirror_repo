#include "SettingsDb.h"
#include "ui_SettingsDb.h"

#include <QMessageBox>
#include <QSettings>

void SettingsDb::errorInput()
{
  QMessageBox::critical(this, tr("Error"), tr("Format of input incorrect."));
}

SettingsDb::SettingsDb(QString addrAndPort, QString dbName, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::SettingsDb)
{
  ui->setupUi(this);
  ui->addrEdit->setText(addrAndPort);
  ui->dbNameEdit->setText(dbName);
}

SettingsDb::~SettingsDb()
{
  delete ui;
}

static bool parseAddrPort(const QString &str, QString &addr, int &port)
{
  int idx = str.indexOf(":");
  if (idx == -1)
  {
    addr = str;
    port = -1;
    return true;
  }

  if (str.length() <= idx + 1)
    return false;

  addr = str.mid(0, idx);
  bool ok;
  port = str.midRef(idx+1, idx).toInt(&ok);
  return ok;
}

void SettingsDb::on_buttonBox_accepted()
{
  const QString str    = ui->addrEdit->text();
  const QString dbName = ui->dbNameEdit->text();
  QString addr;
  int     port;

  if (!parseAddrPort(str, addr, port))
  {
    errorInput();
    return;
  }

  QSettings set("libconfig.ini", QSettings::IniFormat);
  set.beginGroup("Main");
  set.setValue("server_address", addr);
  if (port == -1) // is Default -> delete key.
    set.remove("server_port");
  else
    set.setValue("server_port", port);
  set.setValue("database_name", dbName);
  set.sync();

  accept();
}
