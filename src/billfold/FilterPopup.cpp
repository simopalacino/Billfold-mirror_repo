#include "FilterPopup.h"
#include "ui_FilterPopup.h"

void FilterPopup::setTags()
{
  for (auto it = _tagsTree->begin(); it != _tagsTree->end(); ++it)
  {
    QString name = it->first;
    if (name.startsWith("Incomings"))
      name = tr("Incomings (Earnings)");
    QTreeWidgetItem *item = new QTreeWidgetItem((QTreeWidgetItem*)nullptr);
    item->setCheckState(0, Qt::Checked);
    item->setText(0, name);
    for (int i = 0; i < it->second.size(); ++i)
    {
      if (it->second[i] != name)
      {
        QTreeWidgetItem *child = new QTreeWidgetItem(item);
        child->setCheckState(0, Qt::Checked);
        child->setText(0, it->second[i]);
      }
    }
    ui->treeWidget->addTopLevelItem(item);
  }
  QTreeWidgetItem *item = new QTreeWidgetItem((QTreeWidgetItem*)nullptr);
  item->setCheckState(0, Qt::Checked);
  item->setText(0, tr("Transfer"));
  ui->treeWidget->addTopLevelItem(item);
  ui->treeWidget->expandAll();
}

void FilterPopup::checkAllItem(QTreeWidgetItem *item, Qt::CheckState checkState)
{
  item->setCheckState(0, checkState);

  for (int i = 0; i < item->childCount(); ++i)
    checkAllItem(item->child(i), checkState);
}

std::set<QString> FilterPopup::getListCategories(QTreeWidgetItem *item)
{
  std::set<QString> setStr;
  if (item->checkState(0) == Qt::Checked)
    setStr.insert(item->text(0));
  for (int i = 0; i < item->childCount(); ++i)
  {
    auto ret = getListCategories(item->child(i));
    setStr.insert(ret.begin(), ret.end());
  }
  return setStr;
}

void FilterPopup::onSelectAllBtnClicked()
{
  checkAllItem(ui->treeWidget->invisibleRootItem());
}

void FilterPopup::onDeselectAllBtnClicked()
{
  checkAllItem(ui->treeWidget->invisibleRootItem(), Qt::Unchecked);
}

std::shared_ptr<TFilters> FilterPopup::getFilters()
{
  return _filters;
}

void FilterPopup::setCategoriesMap(std::shared_ptr<std::map<QString, QStringList>> tagsTree)
{
  _tagsTree = tagsTree;
  setTags();
}

FilterPopup::FilterPopup(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::FilterPopup)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Popup);

  connect(ui->selectAllBtn,   &QPushButton::clicked, this, &FilterPopup::onSelectAllBtnClicked);
  connect(ui->deselectAllBtn, &QPushButton::clicked, this, &FilterPopup::onDeselectAllBtnClicked);

  connect(ui->buttonBox, &QDialogButtonBox::accepted, this, [this] {
    _filters = std::make_shared<TFilters>();
    QString str = ui->regIdEdit->text();
    bool ok = false;
    if (!str.isEmpty())
      str.toUInt(&ok);
    if (ok)
      _filters->registrationId  = ui->regIdEdit->text().toUInt();
    _filters->descriptionLike = ui->descrEdit->text();
    _filters->categories      = getListCategories(ui->treeWidget->invisibleRootItem());
  });
  connect(ui->buttonBox, &QDialogButtonBox::rejected, this, [this] {
    _filters = nullptr;
  });
}

FilterPopup::~FilterPopup()
{
  delete ui;
}
