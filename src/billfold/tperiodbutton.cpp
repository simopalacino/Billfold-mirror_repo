#include "tperiodbutton.h"
#include <QMouseEvent>
#include <QSettings>

void TPeriodButton::mousePressEvent(QMouseEvent *e)
{
  if (e->button() != Qt::LeftButton)
    return;

  if (!_pop->exec())
    return;

  QDate fromDate, toDate;
  bool  full, single;
  _pop->getValues(&fromDate, &toDate, &full, &single);

  _values.clear();
  _values.append(fromDate.toString("yyyy-MM-dd"));
  _values.append(toDate != QDate::currentDate() ? toDate.toString("yyyy-MM-dd") : "0");
  _values.append(QString::number(full));
  _values.append(QString::number(single));

  QSettings sets("billfold.ini", QSettings::IniFormat);
  sets.beginGroup("Main");
  sets.setValue("period_data", _values);
  sets.sync();
  auto pd = std::make_shared<TPeriodData>();
  pd->from        = fromDate;
  pd->to          = toDate;
  pd->fullMonths  = full;
  pd->singleMonth = single;
  emit periodChanged(pd);
}

TPeriodButton::TPeriodButton(QWidget *parent) :
  QPushButton(parent), _pop(new PeriodPopup(this))
{
  QSettings sets("billfold.ini", QSettings::IniFormat);
  sets.beginGroup("Main");
  _values = sets.value("period_data", QStringList{}).toStringList();
  if (_values.empty())
  {
    QDate today = QDate::currentDate();
    _values.append(QDate(today.year(), today.month(), 1).toString("yyyy-MM-dd"));
    _values.append(today.toString("yyyy-MM-dd"));
    _values.append(QString::number(true));
    _values.append(QString::number(false));
    sets.setValue("period_data", _values);
    sets.sync();
  }
  _pop->setValues(_values);
}
