#ifndef TNUMERICEDIT_H
#define TNUMERICEDIT_H

#include <QLineEdit>
#include "Database/tnumeric.h"
#include "tcustomlineedit.h"

class TNumericEdit : public TCustomLineEdit
{
  Q_OBJECT

  Database::TNumeric *_num;

public:
  Database::TNumeric getNumericValue() { return _num ? *_num : Database::TNumeric(); };

  void setPrecisionScale(uint8_t precision = 18, uint8_t scale = 0);
  void setNumeric(const QString &str);
  void setNumeric(const Database::TNumeric &str);

  TNumericEdit(QWidget *parent = nullptr);
  ~TNumericEdit();

  // TCustomLineEdit interface
protected:
  virtual QString defaultText()               override;
  virtual bool    isValid()                   override;
  virtual void    setText(const QString &str) override;
  virtual QString toString()                  override;
};

#endif // TNUMERICEDIT_H
