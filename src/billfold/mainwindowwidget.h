#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include "taccountbutton.h"
#include "utils.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

    Ui::Widget *ui;

//    void changeEvent(QEvent *event) override;

private slots:
//    void mousePosEvent(QPoint &point) { UNUSED(point) }
//    void mousePressEvent(QMouseEvent *e) override { UNUSED(e) }
    void resizeWidget(QResizeEvent *event);
    void on_newRecordBtn_clicked();

public:
    static void resizeWidth(QWidget *widget, int newWidth);
    Widget(QWidget *parent = nullptr);
    ~Widget();
};
#endif // WIDGET_H
