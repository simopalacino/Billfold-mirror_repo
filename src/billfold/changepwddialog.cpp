#include "changepwddialog.h"
#include "ui_changepwddialog.h"

#include <QCloseEvent>
#include <QMessageBox>

void ChangePwdDialog::closeEvent(QCloseEvent *e)
{
  if (get_newPwdLine() == ui->reNewPwdLine->text())
    QDialog::closeEvent(e);
  else
  {
    QMessageBox::warning(this, "Change Password", "Passwords are not equal");
    e->ignore();
  }
}

QString ChangePwdDialog::get_newPwdLine() const
{
  return ui->newPwdLine->text();
}

QString ChangePwdDialog::get_reNewPwdLine() const
{
  return ui->reNewPwdLine->text();
}

ChangePwdDialog::ChangePwdDialog(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::ChangePwdDialog)
{
  ui->setupUi(this);
  connect(ui->okBtn,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
  ui->okBtn->setDefault(true);
}

ChangePwdDialog::~ChangePwdDialog()
{
  delete ui;
}

