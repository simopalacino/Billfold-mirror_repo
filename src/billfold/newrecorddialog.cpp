#include "newrecorddialog.h"
#include "engine/db/accounts.h"
#include "ui_newrecorddialog.h"
#include <QListView>
#include <QMessageBox>
#include <QJsonArray>
#include "date.h"
#include "json_record.h"
#include "mapstringvariantrecord.h"
#include "timevalidator.h"
#include "utils.h"

typedef Database::TNumeric numeric;

void NewRecordDialog::changeType(Database::RecordType type)
{
  Database::RecordType prev = _type;
  Database::RecordType last = _type;
  _type = type;

  for (char i = 0; i < 2; ++i)    // ;-) trucchetto per attivare e disattivare 0 e poi 1, con cambio di tipo.
  {
    switch (last)
    {
    case Database::Outgoing: ui->outgoingBtn->setChecked(i);  break;
    case Database::Income:   ui->incomeBtn->setChecked(i);   break;
    case Database::Transfer: ui->transferBtn->setChecked(i); break;
    }
    last = type;
  }
  if (_type == Database::Transfer)
    ui->categoryButton->setText(tr("Transfer"));
  ui->categoryButton->setEnabled(type != Database::Transfer);
  if (prev == Database::Transfer)
    setCategoryButtonDefault();
  ui->accountCBox->setVisible(type != Database::Income);
  ui->accountLabel->setVisible(type != Database::Income);
  ui->toAccountCBox->setVisible(type != Database::Outgoing);
  ui->toAccountLabel->setVisible(type != Database::Outgoing);
}

int NewRecordDialog::getIdAccountByIndex(int index)
{
  return _accountIdList[index];
}

int NewRecordDialog::getIndexAccountById(int id)
{
  int i = 0;
  for (auto it = _accountIdList.begin(); it != _accountIdList.end(); ++it, ++i)
  {
    if (*it == id)
      return i;
  }
  return 0;
}

void NewRecordDialog::setCategoryButtonDefault()
{
  ui->categoryButton->setText(tr("Select a category"));
}

void NewRecordDialog::setFieldWarningsAndErrors()
{
  // WARNING ON ACCOUNTEDIT < DATEEDIT
  auto accountingWarning = [&]{
    if (*ui->accountingDateEdit < *ui->dateEdit)
      QMessageBox::warning(this, "Warning", QString("%1 is minor than %2.").arg(ui->accountingDateEdit->objectName(), ui->dateEdit->objectName()));
  };
  connect(ui->accountingDateEdit, &TDateEdit::editingFinished, this, accountingWarning);
  connect(ui->dateEdit,           &TDateEdit::editingFinished, this, accountingWarning);
}

bool NewRecordDialog::validatorFields()
{
  // Some variable for appo.
  const int   accIndex     = ui->accountCBox->currentIndex();
  const int   toAccIndex   = ui->toAccountCBox->currentIndex();
  auto accIt   = std::find_if(_accountsList.begin(), _accountsList.end(), [&](engine::SIRecord rec) { return rec.value(ACCOUNTS_ID).toInt() == _accountIdList[accIndex];   });
  auto toAccIt = std::find_if(_accountsList.begin(), _accountsList.end(), [&](engine::SIRecord rec) { return rec.value(ACCOUNTS_ID).toInt() == _accountIdList[toAccIndex]; });
  const bool accIsCash   = accIt->value(ACCOUNTS_CONTANTI).toBool();
  const bool toAccIsCash = toAccIt->value(ACCOUNTS_CONTANTI).toBool();
  const bool accIsVirt   = accIt->value(ACCOUNTS_VIRTUAL).toBool();
  const bool toAccIsVirt = toAccIt->value(ACCOUNTS_VIRTUAL).toBool();
  const QDate date         = QDate::fromString(ui->dateEdit->text(), Qt::DefaultLocaleShortDate);
  const bool  f_is_cash    = (_type == Database::Outgoing  && accIsCash) ||
                             (_type == Database::Income   && toAccIsCash) ||
                             (_type == Database::Transfer && (accIsCash || toAccIsCash));
  const bool  f_same_year  = date.year() == QDateTime::currentDateTime().date().year();
  const bool  f_same_month = f_same_year && date.month() == QDateTime::currentDateTime().date().month();
  bool        f_same_day   = false;
  bool        f_accDate_minor_date = false;
  if (!ui->accountingDateEdit->text().isEmpty())
  {
    const QDate d = QDate::fromString(ui->accountingDateEdit->text(), Qt::DefaultLocaleShortDate);
    f_same_day    = d.year() == date.year() && d.month() == date.month() && d.day() == date.day();
    if (!f_same_day)
      f_accDate_minor_date = d < date;
  }
  const bool f_out_acc_virtual = ui->accountCBox->isVisible() && accIsVirt;
  const bool f_in_acc_virtual  = ui->toAccountCBox->isVisible() && toAccIsVirt;
  ///////////////////////////////////////////////////////////////////////////

  bool ok = true;
  // Bloccanti.
  if (_type != Database::Transfer && ui->categoryButton->text() == "Select a category")
  {
    QMessageBox::warning(this, "Warning", "Select a category");
    ok = false;
  }
  else    // Eventuali
  {
    if (_type == Database::Transfer && accIndex == toAccIndex
        && QMessageBox::question(this, "Question", "Do you really want to create a transfer in the same account?") == QMessageBox::No)
    {
      ok = false;
    }
    else if (ui->amountText->getNumericValue().isZero()
             && QMessageBox::question(this, "Question", "Do you really want to create a record with zero amount?") == QMessageBox::No)
    {
      ok = false;
    }
    else if (!f_same_month
             && QMessageBox::question(this, "Question", "Attention. The month is not the same of the current month.\n"
                                                        "Do you really want to continue?") == QMessageBox::No)
    {
      ok = false;
    }
    else if (f_is_cash && !f_same_day
             && QMessageBox::question(this, "Question", "The accounting date is not equal to date for cash.\n"
                                                        "Do you really want to continue?") == QMessageBox::No)
    {
      ok = false;
    }
    else if (f_accDate_minor_date
             && QMessageBox::question(this, "Warning", "The accounting date is minor of date.\n"
                                                        "Do you really want to continue?") == QMessageBox::No)
    {
      ok = false;
    }
    else if ((f_out_acc_virtual || f_in_acc_virtual)
             && QMessageBox::question(this, tr("Question"), tr("The account is virtual\n"
                                                        "Do you really want to continue?")) == QMessageBox::No)
    {
      ok = false;
    }
  }
  return ok;
}

void NewRecordDialog::fillAccountsCBoxes(const std::vector<engine::SIRecord>& accountsList)
{
  ui->accountCBox->clear();
  ui->toAccountCBox->clear();

  ui->accountCBox->setInsertPolicy(QComboBox::InsertAtBottom);
  ui->toAccountCBox->setInsertPolicy(QComboBox::InsertAtBottom);

  _accountIdList.clear();

  for (auto it = accountsList.begin(); it != accountsList.end(); ++it)
  {
    if ((*it)[ACCOUNTS_VISIBLE].toBool())
    {
      ui->accountCBox->addItem((*it)[ACCOUNTS_NAME].toString());
      ui->toAccountCBox->addItem((*it)[ACCOUNTS_NAME].toString());
      _accountIdList.push_back((*it)[ACCOUNTS_ID].toUInt());
    }
  }
}

void NewRecordDialog::fillCategories(const std::map<QString, QStringList> &tagsTree)
{
  _tagsTree.clear();
  _tagsTree = tagsTree;
}

void NewRecordDialog::closeEvent(QCloseEvent *event)
{
  (void)event;
  if (_tagsDialog.get())
    _tagsDialog->close();
}

void NewRecordDialog::onOutgoingBtnToggled(bool checked)
{
  if (checked && _type != Database::Outgoing)
    changeType(Database::Outgoing);
  else if (!checked && _type == Database::Outgoing)
    ui->outgoingBtn->setChecked(true);
}

void NewRecordDialog::onIncomeBtnToggled(bool checked)
{
  if (checked && _type != Database::Income)
    changeType(Database::Income);
  else if (!checked && _type == Database::Income)
    ui->incomeBtn->setChecked(true);
}

void NewRecordDialog::onTransferBtnToggled(bool checked)
{
  if (checked && _type != Database::Transfer)
    changeType(Database::Transfer);
  else if (!checked && _type == Database::Transfer)
    ui->transferBtn->setChecked(true);
}

void NewRecordDialog::on_okBtn_clicked(bool)
{
  bool ok = validatorFields();
  if (ok)
  {
    QString tagText = _type == Database::Transfer ? "Transfer" : ui->categoryButton->text();

    std::map<QString, QVariant> fields;
    fields[REC_TYPE           ] = _type;
    fields[REC_TAG            ] = tagText;
    fields[REC_AMOUNT         ] = ui->amountText->text();
    fields[REC_DATE           ] = ui->dateEdit->ansi();
    fields[REC_TIME           ] = QTime::fromString(ui->timeEdit->text()).toString("hh:mm:ss");
    fields[REC_FROM_ACCOUNT_ID] = (int)getIdAccountByIndex(ui->accountCBox->currentIndex());
    fields[REC_TO_ACCOUNT_ID  ] = (int)getIdAccountByIndex(ui->toAccountCBox->currentIndex());
    fields[REC_DESCRIPTION    ] = ui->descriptionTextEdit->toPlainText();
    fields[REC_ACC_DATE       ] = !ui->accountingDateEdit->text().isEmpty() ? ui->accountingDateEdit->ansi() : "";
    fields[REC_CAUSAL         ] = !ui->causalEdit->text().isEmpty()         ? ui->causalEdit->text()         : "";
    fields[REC_NOTE           ] = !ui->noteEdit->text().isEmpty()           ? ui->noteEdit->text()           : "";
    fields[REC_PLACE          ] = !ui->placeEdit->text().isEmpty()          ? ui->placeEdit->text()          : "";
    bool okRegId;
    ui->regIdEdit->text().toUInt(&okRegId);
    if (!ui->regIdEdit->text().isEmpty() && okRegId)
      fields[REC_REGISTRATION] = ui->regIdEdit->text();

    _validToAddRecord = true;
    if (!_editMode)
      emit insertRecord(MapStringVariantRecord(fields).getInstance());
    else
    {
      fields[REC_ID] = _editId.midRef(1).toUInt();
      emit updateRecord(_editId, MapStringVariantRecord(fields).getInstance());
    }
    done(QDialog::Accepted);
  }
}

void NewRecordDialog::on_cancelBtn_clicked()
{
  reject();
}

void NewRecordDialog::on_dateEdit_editingFinished()
{
  if (!_accountingDateEdit_dirty)
    ui->accountingDateEdit->setDate(ui->dateEdit->getDate());
}

void NewRecordDialog::on_timeEdit_editingFinished()
{
  TimeValidator timeValidator;
  timeValidator(ui->timeEdit->text());

  if (!timeValidator.isValid())
  {
    QMessageBox::warning(this, "Warning", "Invalid time format", QMessageBox::Ok, QMessageBox::Ok);
    ui->timeEdit->setText(QTime::currentTime().toString(QLocale().timeFormat(QLocale::LongFormat)));
  }
  else
    ui->timeEdit->setText(timeValidator.get());
}

void NewRecordDialog::on_accountingDateEdit_editingFinished()
{
  _accountingDateEdit_dirty = true;
}

void NewRecordDialog::on_categoryButton_clicked()
{
  if (!_tagsDialog) // Lazy initialization.
  {
    _tagsDialog = std::unique_ptr<TagsDialog>(new TagsDialog(this));
    _tagsDialog->setTags(_tagsTree);
    _tagsDialog->setStyleSheet("font: 11pt \"Calibri\";"
                               "color: #354052;");
    connect(_tagsDialog.get(), &TagsDialog::tagSelected, this, &NewRecordDialog::tagSelectedEvent);
  }

  _tagsDialog->open();
  _tagsDialog->raise();
  _tagsDialog->setFocus();
}

void NewRecordDialog::tagSelectedEvent(QString text, bool isIncoming)
{
  if (!text.isEmpty())
  {
    ui->categoryButton->setText(text);
    changeType(isIncoming ? Database::Income : Database::Outgoing);
  }
  else
    appLog("tagSelectedEvent get an empty item.");
}

void NewRecordDialog::notifyAccountsListChanged(const std::vector<engine::SIRecord> &accountsList)
{
  fillAccountsCBoxes(accountsList);
}

void NewRecordDialog::notifyCategoriesListChanged(const std::map<QString, QStringList> &tagsTree)
{
  fillCategories(tagsTree);
  if (_tagsDialog)
    _tagsDialog->setTags(_tagsTree);
}

bool NewRecordDialog::setEditMode(bool editMode, QString idUpdate, engine::SIRecord fields)
{
  _editMode = editMode;
  _editId   = idUpdate;
  if (editMode)
  {
    if (fields != nullptr)
    {
      precompileFields(fields);
      ui->regTotalLbl->show();
      ui->regAmountLbl->show();
    }
    else
    {
      error(tr("Error loading fields."), this);
      return false;
    }
  }
  else
    ui->typeRecordRadio->setEnabled(true);
  setWindowTitle(editMode ? "Edit record" : "New Record");
  ui->title->setText(editMode ? "Edit Record" : "ADD TRANSACTION");
  ui->okBtn->setText(editMode ? "Edit Record" : "Add Record");
  ui->transferBtn->setEnabled(!editMode);
  return true;
}

void NewRecordDialog::preselectAccounts(int index)
{
  ui->accountCBox->setCurrentIndex(index);
  ui->toAccountCBox->setCurrentIndex(index);
}

void NewRecordDialog::precompileFields(const engine::SIRecord &fields)
{
  numeric amount(fields[REC_AMOUNT].toString().toLocal8Bit().data(), CIFRA_PRECI, CIFRA_SCALE);
  Database::RecordType type = (Database::RecordType)fields[REC_TYPE].toUInt();

  if (type != Database::Outgoing)
    changeType(type);

  if (type == Database::Transfer || type == Database::Income)
    ui->toAccountCBox->setCurrentIndex(getIndexAccountById(fields[REC_TO_ACCOUNT_ID].toInt()));

  if (type == Database::Transfer || type == Database::Outgoing)
    ui->accountCBox->setCurrentIndex(getIndexAccountById(fields[REC_FROM_ACCOUNT_ID].toInt()));

  ui->categoryButton->setText          (fields[REC_TAG].toString());
  ui->amountText->setNumeric           (numeric::abs(amount));
  ui->dateEdit->setDate                (fields[REC_DATE]       .toString());
  ui->timeEdit->setText                (fields[REC_TIME]       .toDateTime().toString("hh:mm:ss"));
  ui->descriptionTextEdit->setPlainText(fields[REC_DESCRIPTION].toString());
  ui->causalEdit->setText              (fields[REC_CAUSAL]     .toString());
  ui->noteEdit->setText                (fields[REC_NOTE]       .toString());
  ui->placeEdit->setText               (fields[REC_PLACE]      .toString());
  if (!fields[REC_ACC_DATE].isNull())
    ui->accountingDateEdit->setDate    (fields[REC_ACC_DATE].toString());
  else
    ui->accountingDateEdit->setText("");

  ui->regIdEdit->setText               (fields[REC_REGISTRATION].toString());
  ui->regAmountLbl->setText            (numeric(fields[REC_REGAMOUNT].toString().toStdString().c_str(), CIFRA_PRECI, CIFRA_SCALE).toString());
}

NewRecordDialog::NewRecordDialog(std::vector<engine::SIRecord>& accountsList, const std::map<QString, QStringList> &tagsTree, QWidget *parent)
  : QDialog(parent),
    ui(new Ui::NewRecordDialog),
    _accountsList(accountsList),
    _accountingDateEdit_dirty(false),
    _editMode(false),
    _type(Database::Outgoing),
    _validToAddRecord(false),
    _tagsTree(tagsTree)
{
  ui->setupUi(this);

  connect(ui->outgoingBtn,  &QPushButton::toggled, this, &NewRecordDialog::onOutgoingBtnToggled);
  connect(ui->incomeBtn,    &QPushButton::toggled, this, &NewRecordDialog::onIncomeBtnToggled);
  connect(ui->transferBtn,  &QPushButton::toggled, this, &NewRecordDialog::onTransferBtnToggled);

  setCategoryButtonDefault();
  ui->amountText->setPrecisionScale(14, 2);
  ui->timeEdit->setText(QTime::currentTime().toString("hh:mm:ss"));

  QListView *itemView    = new QListView();
  QListView *itemView_to = new QListView();
  ui->accountCBox->setView(itemView);
  ui->toAccountCBox->setView(itemView_to);
  ui->toAccountCBox->setVisible(false);
  ui->toAccountLabel->setVisible(false);
  fillAccountsCBoxes(accountsList);

  ui->accountingDateEdit->setNullable(true, true);
  ui->categoryButton->setFocus();

  // Hiding reg id widgets
  ui->regTotalLbl->hide();
  ui->regAmountLbl->hide();

  setFieldWarningsAndErrors();
}

NewRecordDialog::~NewRecordDialog()
{
  delete ui;
}
