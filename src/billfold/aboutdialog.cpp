#include "aboutdialog.h"
#include "ui_aboutdialog.h"

void AboutDialog::setVersionNum(QString ver)
{
  ui->version->setText(ui->version->text().arg(ver));
}

AboutDialog::AboutDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AboutDialog)
{
  ui->setupUi(this);

  ui->name->setTextInteractionFlags(Qt::TextSelectableByMouse);
  ui->version->setTextInteractionFlags(Qt::TextSelectableByMouse);
  ui->author->setTextInteractionFlags(Qt::TextSelectableByMouse);
}

AboutDialog::~AboutDialog()
{
  delete ui;
}
