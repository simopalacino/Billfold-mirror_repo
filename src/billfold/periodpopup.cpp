#include "periodpopup.h"
#include "ui_periodpopup.h"

void PeriodPopup::setValues(const QDate &dateFrom, const QDate &dateTo, bool fullMonths, bool singleMonth)
{
  ui->dateFrom->setDate(dateFrom);
  ui->dateTo->setDate(dateTo);
  ui->fullMonthCheck->setChecked(fullMonths);
  ui->singleMonthCheck->setChecked(singleMonth);
}

void PeriodPopup::setValues(const QStringList &list)
{
  QDate f = QDate::fromString(list[0].toLatin1(), "yyyy-MM-dd");
  QDate t = QString::fromLatin1(list[1].toLatin1()) == "0" ? QDate::currentDate() :
                                                             QDate::fromString(list[1].toLatin1(), "yyyy-MM-dd");
  setValues(f, t, list[2].toInt(), list[3].toInt());
}

void PeriodPopup::getValues(QDate *dateFrom, QDate *dateTo, bool *fullMonths, bool *singleMonth)
{
  if (dateFrom)
    *dateFrom = ui->dateFrom->date();
  if (dateTo)
    *dateTo = ui->dateTo->date();
  if (fullMonths)
    *fullMonths = ui->fullMonthCheck->isChecked();
  if (singleMonth)
    *singleMonth = ui->singleMonthCheck->isChecked();
}

PeriodPopup::PeriodPopup(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PeriodPopup)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Popup);
}

PeriodPopup::~PeriodPopup()
{
  delete ui;
}

void PeriodPopup::on_fullMonthCheck_stateChanged(int arg1)
{
  ui->singleMonthCheck->setEnabled(!arg1);
}

void PeriodPopup::on_singleMonthCheck_stateChanged(int arg1)
{
  ui->dateFrom->setEnabled(!arg1);
  ui->fullMonthCheck->setEnabled(!arg1);
}
