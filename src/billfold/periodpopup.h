#ifndef PERIODPOPUP_H
#define PERIODPOPUP_H

#include <QDialog>

namespace Ui {
class PeriodPopup;
}

class PeriodPopup : public QDialog
{
  Q_OBJECT
private slots:
  void on_okBtn_clicked() { accept(); }
  void on_fullMonthCheck_stateChanged(int arg1);

  void on_singleMonthCheck_stateChanged(int arg1);

public:
  void  setValues(const QDate &dateFrom, const QDate &dateTo, bool fullMonths, bool singleMonth);
  void  setValues(const QStringList &list);
  void  getValues(QDate *dateFrom, QDate *dateTo = nullptr, bool *fullMonths = nullptr, bool *singleMonth = nullptr);
  explicit PeriodPopup(QWidget *parent = nullptr);
  ~PeriodPopup();

private:
  Ui::PeriodPopup *ui;
};

#endif // PERIODPOPUP_H
