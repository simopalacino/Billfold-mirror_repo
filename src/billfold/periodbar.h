#ifndef PERIODBAR_H
#define PERIODBAR_H

#include <QWidget>
#include <engine/engine.h>
#include <threestatebutton.h>
#include "FilterPopup.h"


namespace Ui {
class PeriodBar;
}

class PeriodBar : public QWidget
{
  Q_OBJECT

  engine::Libengine* _libengine;
  std::shared_ptr<FilterPopup> _filterPopup;

  virtual void resizeEvent(QResizeEvent *event) override;
  void movePeriodButton();

private slots:
  void onNewRecordBtnClicked();
  void onSelectorStateChanged();
  void onFiltersBtnClicked();

signals:
  void newRecordBtnClicked();
  void selectorStateChanged();
  void filtersChanged(std::shared_ptr<TFilters> filters);

public:
  ThreeStateButton::State getStateSelector();
  void setLibengine(engine::Libengine* libengine)
  { _libengine = libengine; }
  void setStateSelector(ThreeStateButton::State state);
  void setSubtot(qreal subtot);
  void setText(QString str);

public slots:
  void categoriesChanged();

public:
  explicit PeriodBar(QWidget *parent = nullptr);
  ~PeriodBar();

private:
  Ui::PeriodBar *ui;
};

#endif // PERIODBAR_H
