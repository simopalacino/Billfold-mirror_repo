#ifndef TWIDGETPANEL_H
#define TWIDGETPANEL_H

#include <cstdint>

#include <QWidget>
#include <QLabel>
#include <QPainter>
#include <QResizeEvent>
#include <QStyleOption>

class TWidgetPanel : public QWidget
{
  Q_OBJECT

  QWidget *_panel;
  QLabel   _title;
  QLabel   _value;

  virtual void paintEvent(QPaintEvent *event) override;
  void         panelResized(int w, int h);

protected slots:
  virtual void resizeEvent(QResizeEvent *event) override;

public:
  void addPanel(QWidget *panel = nullptr);
  void setTitle(const QString &str);
  void setValue(const QString &str);

  explicit TWidgetPanel(QWidget *parent = nullptr);

  static uint16_t _lMargin;
  static uint16_t _rMargin;
  static uint16_t _tMargin;
  static uint16_t _bMargin;
};

#endif // TWIDGETPANEL_H
