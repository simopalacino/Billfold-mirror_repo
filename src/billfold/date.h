#ifndef TDATEEDIT_H
#define TDATEEDIT_H

#include <QLineEdit>
#include <QDate>
#include "tcustomlineedit.h"

class TDateEdit : public TCustomLineEdit
{
  Q_OBJECT

  QDate _value;
  QDate _today;
  bool  _isNull;

  void on_editFinished();
  // TCustomLineEdit interface
protected:
  virtual QString defaultText() override;
  virtual bool    isValid()     override;
  virtual QString toString()    override;

public:
  void setDate(const QString &str);
  void setDate(const QDate &date);
  virtual void setText(const QString &str) override;
  QString ansi();
  QDate   getDate();

  bool operator==(const TDateEdit& rhv);
  bool operator<(const TDateEdit& rhv);
  bool operator>(const TDateEdit& rhv);
  bool operator>=(const TDateEdit& rhv);
  bool operator<=(const TDateEdit& rhv);

  explicit TDateEdit(QWidget *parent = nullptr);
};

#endif // TDATEEDIT_H
