#include <iostream>
#include <string>
#include <map>
#include <vector>

#include <QApplication>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QDir>
#include <QTextCodec>

#include "billfoldwindow.h"
#include "billfold0200.h"
#include "logindialog.h"
#include "engine/engine.h"
#include "changepwddialog.h"
#include "utils.h"
#include "../include/version_id.h"

const char *version = BILLFOLDVERSION;
const char *author  = "Simone Palacino";
const char *email   = "simo.palacino@gmail.com";


int billfold0100(int &argc, char *argv[]);


int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  if (QDir::current() != QCoreApplication::applicationDirPath() && !QDir::setCurrent(QCoreApplication::applicationDirPath()))
    QMessageBox::critical(nullptr, "Error", "Current Working Directory is not equal to Executable Directory!");

  QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

  appLog.open("logbillfold.log");
  long p = -1;
  if (argc > 1 && argv[1][0] == '-')
    p = strtol(argv[1] + 1, NULL, 10);
  switch(p)
  {
  default: case 0:
  case 1: return billfold0100(argc, argv);  // billfold
  case 2: return billfold0200(argc, argv);  // db_translation
  }
  return 0;
}

static void initEngine(std::shared_ptr<engine::Libengine> &libengine)
{
  if (libengine != nullptr)
    return;

  libengine = std::make_shared<engine::Libengine>();
  libengine->initSettings("ODBC");
  if (libengine->initLib() != engine::Libengine::ST_READY)
  {
    appLog("Error _libengine.initLib()");
    QMessageBox::warning(nullptr, "Warning", "Error during initialization");
    libengine = nullptr;
    exit(EXIT_FAILURE);
  }
}

int billfold0100(int &argc, char *argv[])
{
  UNUSED(argc);
  UNUSED(argv);

  std::shared_ptr<engine::Libengine> libengine;

  LoginDialog dlg;
  while (true)
  {
    dlg.clearPwd();
    if (!dlg.exec())
      break;

    QString user, pwd;
    dlg.getCredentials(user, pwd);
    pwd = QString::fromLatin1(QCryptographicHash::hash(pwd.toUtf8(), QCryptographicHash::Sha512).toHex());
    appLog("pwd: %s", QSTR2CSTR(pwd));

    libengine = nullptr;
    initEngine(libengine);

    if (!libengine->login(user, pwd))
    {
      QMessageBox::warning(nullptr, "Warning", "Wrong credentials");
      continue;
    }

    if (dlg.haveToChangePwd())
    {
      ChangePwdDialog d;
      if (!d.exec())
        continue;

      QString newPwd = QString::fromLatin1(QCryptographicHash::hash(d.get_newPwdLine().toUtf8(), QCryptographicHash::Sha512).toHex());
      if (libengine->changePwd(user, pwd, newPwd))
        QMessageBox::information(&dlg, "Change Password", "Password changed");
      else
        QMessageBox::warning(&dlg, "Change Password", "Password not changed");
      continue;
    }

    BillfoldWindow w(libengine.get());
    w.show();
    int ret = qApp->exec();
    if (w.hasLogout())
      continue;
    else
      return ret;
  }
  return 0;
}
