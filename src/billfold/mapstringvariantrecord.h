#ifndef MAPSTRINGVARIANTRECORD_H
#define MAPSTRINGVARIANTRECORD_H

#include "engine/irecord.h"


class MapStringVariantRecord : public engine::IRecord
{
  std::map<QString, QVariant> _map;
public:
  virtual int      count()                                            const override;
  virtual QString  fieldName(int i)                                   const override;
  virtual QVariant value(int i)                                       const override;
  virtual QVariant value(const QString &name)                         const override;
  virtual void     setValue(int i, const QVariant &val)                     override;
  virtual void     setValue(const QString &name, const QVariant &val)       override;

  virtual engine::SIRecord getCopy() const override;

//  MapStringVariantRecord();
  MapStringVariantRecord(const std::map<QString, QVariant>& record)
    : _map(record) { }
  MapStringVariantRecord() { }
};

#endif // MAPSTRINGVARIANTRECORD_H
