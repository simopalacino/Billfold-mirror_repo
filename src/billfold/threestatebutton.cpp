#include "threestatebutton.h"
#include "ui_threestatebutton.h"
#include <QStyleOption>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>

void ThreeStateSelectorButton::paintEvent(QPaintEvent *event)
{
  (void)event;
  QPainter p(this);
  p.setBrush(_color);
  p.setPen(QColor(0xffffff));
  p.setRenderHint(QPainter::Antialiasing, true);
  p.drawEllipse(0, 0, 28, 28);
  if (_hasFocus)
  {
    p.setPen(QPen(QColor(QRgb(0x404142)), 1, Qt::DotLine));
    p.setBrush(QColor(0, 0, 0, 0));
    p.drawEllipse(2, 2, 24, 24);
  }
}

void ThreeStateSelectorButton::changeColor(QColor color)
{
  _color = color;
  update();
}

///////////////////////////////////////////////////////////////

void ThreeStateButton::paintEvent(QPaintEvent *event)
{
  (void)event;
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

ThreeStateSelectorButton *ThreeStateButton::getSelectorButton() { return ui->selector; }

void ThreeStateButton::focusInEvent(QFocusEvent *event)
{
  (void)event;
  ui->selector->getFocus();
}

void ThreeStateButton::focusOutEvent(QFocusEvent *event)
{
  (void)event;
  ui->selector->loseFocus();
}

void ThreeStateButton::keyPressEvent(QKeyEvent *event)
{
  if (hasFocus())
  {
    switch (event->key())
    {
    case Qt::Key_Left:
      if (_state == CenterV)
        setState(LeftV);
      else if (_state == RightV)
        setState(CenterV);
      break;
    case Qt::Key_Right:
      if (_state == CenterV)
        setState(RightV);
      else if (_state == LeftV)
        setState(CenterV);
      break;
    }
  }
}

void ThreeStateButton::mouseMoveEvent(QMouseEvent *event)
{
  const int w     = width();
  const int wBut  = ui->selector->width();
  const int wPerc = perc*w/100;
  const int posX  = event->pos().x();
  int x           = posX - wBut / 2;
  if (x < 0 || posX <= wPerc + wBut/2)
    x = 0;
  else if (x > w - wBut || posX >= w - wPerc - wBut/2)
    x = w - wBut;

  if (posX >= w/2 - wPerc && posX <= w/2 + wPerc)
    x = w / 2 - wBut / 2;
  ui->selector->move(x, 0);
  _mouseMoving = true;
}

void ThreeStateButton::mousePressEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton)
  {
    const int w     = width();
    const int wBut  = ui->selector->width();
    const int wPerc = perc*w/100;
    const int posX  = event->pos().x();
    if (posX <= wPerc + wBut/2)
      setState(LeftV);
    else if (posX >= w - wPerc - wBut/2)
      setState(RightV);
    else if (posX >= w/2 - wPerc && posX <= w/2 + wPerc)
      setState(CenterV);
  }
}

void ThreeStateButton::mouseReleaseEvent(QMouseEvent *event)
{
  if (_mouseMoving)
  {
    const State prevSt = _state;
    const int   posX   = event->pos().x();
    const int   w      = width();
    const int   wBut   = ui->selector->width();
    const int   wPerc  = perc*w/100;
    if (posX > wPerc && posX < w/2 - wPerc)
    {
      ui->selector->move(0, 0);
      _state = LeftV;
    }
    else if (posX > w/2 + wPerc && posX < w - wPerc)
    {
      ui->selector->move(w - wBut, 0);
      _state = RightV;
    }
    else if (posX <= wPerc + wBut/2)
      _state = LeftV;
    else if (posX >= w - wPerc - wBut/2)
      _state = RightV;
    else
      _state = CenterV;
    _mouseMoving = false;
    if (_state != prevSt)
      emit stateChanged(_state);
  }
}

void ThreeStateButton::moveSx()
{
  ui->selector->move(0, 0);
}

void ThreeStateButton::moveCx()
{
  ui->selector->move(width() / 2 - ui->selector->width() / 2, 0);
}

void ThreeStateButton::moveDx()
{
  ui->selector->move(width() - ui->selector->width(), 0);
}

void ThreeStateButton::setState(ThreeStateButton::State state)
{
  if (state != _state)
  {
    switch (state)
    {
    case LeftV:   moveSx(); break;
    case CenterV: moveCx(); break;
    case RightV:  moveDx(); break;
    }
    _state = state;
    emit stateChanged(state);
  }
}

ThreeStateButton::ThreeStateButton(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ThreeStateButton),
  _mouseMoving(false),
  _state(CenterV)
{
  ui->setupUi(this);
  ui->selector->move(width()/2 - ui->selector->width()/2, 0);
  setFocusPolicy(Qt::StrongFocus);
}

ThreeStateButton::~ThreeStateButton()
{
  delete ui;
}
