#include "tdial.h"
#include <QPainter>
#include <QPainterPath>

#define PI      3.14159265
#define VERDE   (QColor((QRgb)0x21cb87))
#define GIALLO  (QColor((QRgb)0xffa000))
#define ROSSO   (QColor((QRgb)0xfe3b2c))

class TLancet : public QWidget
{
  int  _angle;
  void paintEvent(QPaintEvent *event)
  {
    (void)event;
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setBrush(QColor(0x555657));
    p.drawRect(rect());
  }
public:
  void setAngle(int angle = -30) { _angle = angle; }
  TLancet(QWidget *parent = nullptr) : QWidget(parent) {  }
};


void TDial::paintEvent(QPaintEvent *event)
{
  (void)event;
  static const int SPESSORE   = 14;
  const int        diameter   = std::max(size().width(), size().height());
  int              startAngle = -45;
  int              spanAngle  = 90;
  QRectF           rectangle(rect());
  QPainter         painter(this);
  QPointF          C(size().width() / 2, size().height() / 2);
  QColor           colors[3] = { VERDE, GIALLO, ROSSO };
  painter.setPen(Qt::transparent);
  painter.setRenderHint(QPainter::Antialiasing, true);

  for (int i = 0; i < 3; ++i)
  {
    QPainterPath path;
    painter.setBrush(colors[i]);
    path.arcMoveTo(rectangle, startAngle);

    path.arcTo(rectangle, startAngle, spanAngle);
    path.arcTo(QRect(SPESSORE, SPESSORE, diameter - 2*SPESSORE, diameter - 2*SPESSORE), startAngle + spanAngle, -spanAngle);

    path.closeSubpath();
    painter.drawPath(path);
    startAngle += spanAngle;
  }

  painter.setBrush(QColor(0x555657));
  painter.drawEllipse(C.x() - 8, C.y() - 8, 16, 16);


}


// Static methods /////////////////////////////////////////////////////////////

//double TDial::m(QPointF A, QPointF C)
//{
//    return (C.y() - A.y()) / (A.x() - C.x());
//}

//std::pair<double, double> TDial::Px(double m, QPointF A, double d)
//{
//    double a = (1 - m*m);
//    double b = -2 * A.x() * (1 + m*m);
//    double c = (1 + m*m) * std::pow(A.x(), 2) - d*d;

//    //         _   _________
//    // x =  -b + \/b^2 -4ac
//    //     ------------------ [Formula risolutiva]
//    //            2a
//    double Px1 = (-b + std::sqrt(b*b -4*a*c)) / 2*a;
//    double Px2 = (-b - std::sqrt(b*b -4*a*c)) / 2*a;
//    return { Px1, Px2 };
//}

//double TDial::Py(double m, QPointF A, double Px)
//{
//    return m*Px + (A.y() - m*A.x());
//}

//QPointF TDial::P(QPointF A, QPointF C, double d)
//{
//    double m  = TDial::m(A, C);
//    auto   px = TDial::Px(m, A, d);
//    double Px = std::abs(A.x() - px.first) < std::abs(A.x() - px.second) ? px.first : px.second;
//    double Py = TDial::Py(m, A, Px);
//    return { Px, Py };
//}

QPointF TDial::pointInCircumference(QPointF C, double r, double a)
{
  double x = C.x() + r * std::cos(a * PI / 180.0);
  double y = C.y() + r * std::sin(a * PI / 180.0);
  y = 2*C.y() - y;
  return { x, y };
}

TDial::TDial(QWidget *parent) : QWidget(parent)
{
  resize(100, 100);
  _lancet = new TLancet(this);
  _lancet->resize(QSize(4, 24));
  _lancet->move(size().width() / 2, size().height() / 2);
}
