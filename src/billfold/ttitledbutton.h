#ifndef TTITLEDBUTTON_H
#define TTITLEDBUTTON_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QTimer>

class TTitledButton : public QWidget
{
  Q_OBJECT
  Q_PROPERTY(int buttHeight_p READ buttHeight WRITE setButtHeight)
  Q_PROPERTY(int buttWidth_p READ buttWidth WRITE setButtWidth)

  int      _buttH;
  int      _buttW;
  QRect    _rect;
  QPainter _painter;
  QFont    _font;
  QColor   _colorFill;
  QColor   _colorText;

  QString _title;
  QString _value;

  bool _selected;
  bool _toggleButton;

  QTimer _timer;

  std::map<QString, QString> _metadata;

  void paintOnScreen();
  void drawTitle();
  void drawValue();

private slots:
  void timeoutPressing();

protected:
  void paintEvent(QPaintEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void keyPressEvent(QKeyEvent *e);
  void keyReleaseEvent(QKeyEvent *e);
  void focusInEvent(QFocusEvent *e);

signals:
  void rightClickSignal(QPoint &mousePos);
  void clickedTitledBtn(QPoint &mousePos);
  void longPression(QPoint &posButton);

public:
  void addMetadata(const QString &key, const QString &value) { _metadata.insert({ key, value }); }
  QString getMetadata(const QString &key)
  {
    auto it = _metadata.find(key);
    if (it == _metadata.end())
      return "";
    return it->second;
  }
  int buttHeight() const;
  int buttWidth() { return _buttW; }

  bool isSelected() { return _selected; }

  QString getTitle() { return _title; }

  void setButtHeight(int newHeight);
  void setButtWidth(int newWidth);
  void setColorFill(QColor color) { _colorFill = color; }
  void setColorText(QColor color) { _colorText = color; }
  void setIcon() {  }
  void setSelected(bool selected = true);
  void setTitle(QString &title);
  void setTitle(const char *title);
  void setToggleButton(bool set = true);
  void setValue(QString &value);

  explicit TTitledButton(bool toggleButton = false, QWidget *parent = nullptr);
  explicit TTitledButton(QWidget *parent);
};

#endif // TACCOUNTBUTTON_H
