#ifndef TBILLFOLDWINDOW_H
#define TBILLFOLDWINDOW_H

// STL
#include <map>
#include <memory>

// QtLib
#include <QMainWindow>
#include <QStack>
#include <QSettings>

// Other includes
#include "taccountbuttonlist.h"
#include "newrecorddialog.h"
#include "newaccountdialog.h"
#include "engine/engine.h"
#include "FilterPopup.h"
#include "tperiodbutton.h"
#include "ttablewidget.h"
#include <engine/iaction.h>


namespace Ui {
class BillfoldWindow;
}

class BillfoldWindow : public QMainWindow
{
  Q_OBJECT

  Ui::BillfoldWindow *ui;

  std::vector<engine::SIRecord>     _accounts;
  std::map<uint, QString>           _mapAccounts;
  engine::Libengine                 *_libengine;
  std::unique_ptr<NewRecordDialog>  _newRecordWin;
  std::unique_ptr<NewRecordDialog>  _updateRecordWin;
  std::unique_ptr<NewAccountDialog> _newAccountWin;

  QSettings                         _settings;

  QDate                             _periodFrom;
  QDate                             _periodTo;
  std::shared_ptr<TFilters>         _filtersTable;

  QStack<std::shared_ptr<engine::IAction>> _actions;
  bool _logout{ false };
  QLabel* _statusbarCifraLabel;
  QTimer* _showInformationsSingleShot;

  void loadBalancePanel();
  void generalRefresh();
  void refreshAccounts();
  void refreshTable();
  void addRowToTable(engine::SIRecordIterator it, TTableWidget* tableWidget, int& count);
  void refreshTagsSummary();
  void setPeriod(TPeriodData &data);
  bool validateRowWFilter(engine::SIRecordIterator it);

  void setEnabledUndoAction();

  void updatingAccount(uint id);
  std::unique_ptr<NewRecordDialog> getNewRecordWin();

  void showInformations();

  template <class _TAction, typename... Args>
  int performAction(Args... args)
  {
    auto action = std::shared_ptr<engine::IAction>(new _TAction(_libengine, std::forward<Args>(args)...));
    int ret = action->exec();
    if (_actions.empty())
      setEnabledUndoAction();
    _actions.push(action);
    return ret;
  }

private slots:
  void addNewRegistration(engine::SIRecord fields);
  void accountSelectedEvent(int i);
  void closeEvent(QCloseEvent *event);
  void accountRequestEvent(TAccountButtonList::Request type, uint i);
  void newAccountEvent(const std::map<QString, QVariant>& record);
  void on_deletingRecord(QString idToDelete);
  void on_updatingRecord(QString idToUpdate);
  void on_periodBtn_periodChanged(std::shared_ptr<TPeriodData> event);
  void updateRegistration(QString oldId, engine::SIRecord fields);
  void resizeWidgetTableTab(QResizeEvent *event);
  void refreshCifraStatusbarInfos();
  void setSubtotalPeriod();

  // File menu
  void on_actionRefresh_triggered();
  void on_actionLogout_triggered();
  void on_actionExit_triggered();
  // Edit menu
  void on_actionUndo_triggered();
  void on_actionNew_Record_triggered();
  void on_actionAdd_Account_triggered();
  void on_actionAdd_Category_triggered();
  void on_actionShow_invisible_accounts_triggered(bool checked = false);

  void on_actionAbout_triggered();
  void on_recordEditAction(TTableWidget::EType type, uint id);
  void on_recordDeleteAction(TTableWidget::EType type, uint id);
  void on_recordDuplicateAction(TTableWidget::EType type, uint id);



signals:
  void accountsListChanged(const std::vector<engine::SIRecord>& accountsList);
  void categoriesListChanged(const std::map<QString, QStringList>& tagsTree);

public:
  bool hasLogout() const { return _logout; }

  static void resizeWidth(QWidget *widget, int newWidth);

  explicit BillfoldWindow(engine::Libengine *libengine, QWidget *parent = nullptr);
  ~BillfoldWindow();
};

#endif // TBILLFOLDWINDOW_H
