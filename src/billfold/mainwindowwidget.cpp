#include "mainwindowwidget.h"
#include "ui_widget.h"
#include <QHostInfo>
#include <QSqlDatabase>
#include <QStatusBar>
#include <QSqlError>
#include "tscrollarea.h"
#include "newrecorddialog.h"


static int colors[] = {
    0x00897b,
    0xffa000,
    0x2e7d32,
    0xb0bec5,
    0x64dd17,
    0x4db6ac,
    0x039b5e
};

static QString obj_names[] =
{
    "postepay_btn",
    "contoing_btn",
    "prepagataing_btn",
    "pecorella_btn",
    "dayTronic Sirio_btn",
    "buonipastobettini_btn",
    "cash_btn"
};

static QString titles[] =
{
    "PostePay",
    "Conto ING",
    "Prepagata ING",
    "Pecorella",
    "DayTronic Sirio",
    "Buoni Pasto Bettini",
    "Cash"
};

//////////////////////////////////

void Widget::resizeWidget(QResizeEvent *event)
{
    int newWidth = event->size().width();

    resizeWidth(ui->accountsBarList, newWidth);
    resizeWidth(ui->scrollAreaWidgetContents, newWidth);
    resizeWidth(ui->periodArea, newWidth);

    // Centro il periodButton.
    QSize p = ((QWidget*)ui->periodBtn->parent())->size();
    ui->periodBtn->move((p.width() - ui->periodBtn->width()) / 2, (p.height() - ui->periodBtn->height()) / 2);
    // Sposto newRecordBtn
    ui->newRecordBtn->move((p.width() - 16) - ui->newRecordBtn->width(), (p.height() - ui->newRecordBtn->height()) / 2);
}

void Widget::on_newRecordBtn_clicked()
{
    NewRecordDialog msk(this);
    msk.exec();
}

void Widget::resizeWidth(QWidget *widget, int newWidth)
{
    widget->resize(newWidth, widget->size().height());
}

Widget::Widget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::Widget)
{
    ui->setupUi(this);

    for (int i = 0; i < 7; ++i)
    {
        TAccountButton &b = ui->accountsBarList->addButton(new TAccountButton(ui->accountsBarList));
        b.setTitle(titles[i]);
        b.setObjectName(obj_names[i]);
        b.setColorText(QColor((QRgb)0xffffff));
        b.setColorFill(QColor((QRgb)colors[i]));
        b.setSelected();
    }
//    connect(ui->scrollArea, &TScrollArea::tabResized, ui->accountsBarList, &TAccountButtonList::risizeWidget);
    connect(ui->scrollArea, &TScrollArea::tabResized, this, &Widget::resizeWidget);
//    connect(ui->scrollArea, &TScrollArea::tabResized, ui->accountsBarList, &Widget::risizeWidget);


//    // Add StatusBar

//    QStatusBar *bar = new QStatusBar(this);
//    ui->gridLayout_2->addWidget(bar);

//    // Connect to DB //////////////////////////////////////////////////////////

//    QSqlDatabase db; db.addDatabase("QODBC");
//    QStringList drivers = QSqlDatabase::drivers();

//    qDebug("Drivers count:  %d", drivers.size());
//    for (int i = 0; i < drivers.size(); i++)
//    {
//        qDebug("Driver: %s", (const char*)drivers.at(i).toUtf8());
//    }
//    QString connectString = "Driver={SQL Server};";      // Driver is now {SQL Server Native Client 11.0}
//    connectString.append("Server=127.0.0.1,1433;");    // Hostname,SQL-Server Instance
//    connectString.append("Database=sp_soldi;"); // Schema
//    connectString.append("Uid=sp;");            // User
//    connectString.append("Pwd=dare96/Cry;");    // Pass
//    db.setDatabaseName(connectString);

//    if(db.open())
//    {
//        bar->showMessage("Connected");
//    }
//    else
//    {
//        bar->showMessage("Not Connected");
//        qDebug("%s", (const char*)db.lastError().driverText().toUtf8());
//    }
}

Widget::~Widget()
{
    delete ui;
}
