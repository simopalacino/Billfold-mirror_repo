#include "timevalidator.h"

#include <QLocale>
#include <QTime>

void TimeValidator::operator()(QString time, QStringList fmts)
{
  _valid = false;
  _value = time;
  if (!fmts.isEmpty())
    _fmts = fmts;

  QTime t;
  for (auto fmt : _fmts)
  {
    t = QTime::fromString(_value, fmt);
    if (t.isValid())
    {
      _valid = true;
      _value = t.toString(_fmts[0]);
      return;
    }
  }
}

TimeValidator::TimeValidator(QStringList fmts) : _valid(false)
{
  if (!fmts.isEmpty())
    _fmts = fmts;
  else
  {
    QLocale l;
    _fmts = QStringList({ l.timeFormat(QLocale::LongFormat),
                          l.timeFormat(QLocale::ShortFormat),
                          l.timeFormat(QLocale::NarrowFormat),
                          "H:m",
                          "h:m AP" });
  }
}
