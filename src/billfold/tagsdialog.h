#ifndef TAGSDIALOG_H
#define TAGSDIALOG_H

#include <vector>
#include <QDialog>
#include <QTreeWidgetItem>

struct TagsItems
{
  std::map<QString, QStringList> _items;
};

namespace Ui {
class TagsDialog;
}

class TagsDialog : public QDialog
{
  Q_OBJECT

  Ui::TagsDialog *ui;

  void itemSelected(QTreeWidgetItem* item);

private slots:
  void on_pushButton_clicked();
  void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);

signals:
  void tagSelected(QString text, bool isIncome);

public:
  void setTags(const std::map<QString, QStringList> &list);

  explicit TagsDialog(QWidget *parent = nullptr);
  ~TagsDialog();
};

#endif // TAGSDIALOG_H
