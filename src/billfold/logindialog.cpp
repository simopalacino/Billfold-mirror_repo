#include "logindialog.h"
#include "ui_logindialog.h"
#include <QSettings>
#include <SettingsDb.h>

void LoginDialog::changePwdClicked()
{
  changePwd = true;
  accept();
}

void LoginDialog::settingsBtnClicked()
{
  QSettings set("libconfig.ini", QSettings::IniFormat);
  set.beginGroup("Main");
  SettingsDb win(QString("%1:%2").arg(set.value("server_address").toString()).arg(set.value("server_port").toInt()), set.value("database_name").toString(), this);
  if (win.exec() != 0)
    return;
}

void LoginDialog::getCredentials(QString &username, QString &password)
{
  username = ui->userEdit->text();
  password = ui->pwdEdit->text();
}

void LoginDialog::clearPwd()
{
  ui->pwdEdit->setText("");
}

LoginDialog::LoginDialog(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::LoginDialog)
{
  ui->setupUi(this);
  ui->pwdEdit->setFocus();
  QSettings set("billfold.ini", QSettings::IniFormat);
  set.beginGroup("Login");
  ui->userEdit->setText(set.value("lastUsername", "admin").toString());
  setWindowTitle("Login");

  connect(ui->okBtn,       SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancelBtn,   SIGNAL(clicked()), this, SLOT(reject()));
  connect(ui->changePwd,   SIGNAL(clicked()), this, SLOT(changePwdClicked()));
  connect(ui->settingsBtn, SIGNAL(clicked()), this, SLOT(settingsBtnClicked()));
  ui->okBtn->setDefault(true);
}

LoginDialog::~LoginDialog()
{
  QSettings set("billfold.ini", QSettings::IniFormat);
  set.beginGroup("Login");
  set.setValue("lastUsername", ui->userEdit->text());
  delete ui;
}
