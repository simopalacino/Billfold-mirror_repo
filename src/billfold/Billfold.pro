QT       += core gui sql charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
INCLUDEPATH += $$PWD/../libengine/
INCLUDEPATH += $$PWD/../include/

win32:CONFIG (release, debug|release): LIBS += -L$$PWD/../../build-libengine/release/ -lengine
else:win32:CONFIG (debug, debug|release): LIBS += -L$$PWD/../../build-libengine/debug/ -lengine
else:unix: LIBS += -L$$PWD/../../build-libengine/debug/ -lengine

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../include/utils.cpp \
    ../include/Database/db_table.c \
    ../include/Database/tnumeric.cpp \
    FilterPopup.cpp \
    SettingsDb.cpp \
    aboutdialog.cpp \
    addcategorydialog.cpp \
    billfold.cpp \
    billfold0200.cpp \
    billfoldwindow.cpp \
    cardbase.cpp \
    changepwddialog.cpp \
    date.cpp \
    logindialog.cpp \
    mapstringvariantrecord.cpp \
    newaccountdialog.cpp \
    newrecorddialog.cpp \
    periodbar.cpp \
    periodpopup.cpp \
    selectorthreestate.cpp \
    taccountbuttonlist.cpp \
    tagsdialog.cpp \
    tagssummary.cpp \
    tcustomlineedit.cpp \
    tdial.cpp \
    threestatebutton.cpp \
    timevalidator.cpp \
    tnumericedit.cpp \
    tperiodbutton.cpp \
    tscrollarea.cpp \
    ttablewidget.cpp \
    ttitledbutton.cpp \
    twidgetpanel.cpp

HEADERS += \
    ../include/Database/tdate.h \
    ../include/json_categories.h \
    ../include/json_record.h \
    ../include/utils.h \
    ../include/Database/db_table.h \
    ../include/Database/records.h \
    ../include/Database/tnumeric.h \
    ../include/Database/ttableaccount.h \
    ../include/version_id.h \
    ../libengine/engine/engine.h \
    FilterPopup.h \
    SettingsDb.h \
    aboutdialog.h \
    addcategorydialog.h \
    billfold0200.h \
    billfoldwindow.h \
    buttonconfirmdefault.h \
    cardbase.h \
    categories.h \
    changepwddialog.h \
    date.h \
    logindialog.h \
    mapstringvariantrecord.h \
    newaccountdialog.h \
    newrecorddialog.h \
    periodbar.h \
    periodpopup.h \
    selectorthreestate.h \
    taccountbutton.h \
    taccountbuttonlist.h \
    tagsdialog.h \
    tagssummary.h \
    tcustomlineedit.h \
    tdial.h \
    threestatebutton.h \
    timevalidator.h \
    tnumericedit.h \
    tperiodbutton.h \
    tscrollarea.h \
    ttablewidget.h \
    ttitledbutton.h \
    twidgetpanel.h

FORMS += \
    FilterPopup.ui \
    SettingsDb.ui \
    aboutdialog.ui \
    addcategorydialog.ui \
    billfoldwindow.ui \
    changepwddialog.ui \
    logindialog.ui \
    newaccountdialog.ui \
    newrecorddialog.ui \
    periodbar.ui \
    periodpopup.ui \
    tagsdialog.ui \
    tagssummary.ui \
    threestatebutton.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

TRANSLATIONS += \
    it.ts

win32{
CONFIG(debug, debug|release) {
    RC_ICONS += icons/debug.ico
} else {
    RC_ICONS += icons/release.ico
}
}

