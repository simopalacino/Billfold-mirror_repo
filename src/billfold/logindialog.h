#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
  Q_OBJECT

  bool changePwd{ false };

private slots:
  void changePwdClicked();
  void settingsBtnClicked();

signals:
  void requestToChangePwd(QString user, QString currentPwd, QString newPwd);

public:
  bool haveToChangePwd() { bool ret = changePwd; changePwd = false; return ret; }
  void getCredentials(QString &username, QString &password);

  void clearPwd();

  explicit LoginDialog(QWidget *parent = nullptr);
  ~LoginDialog();

private:
  Ui::LoginDialog *ui;
};

#endif // LOGINDIALOG_H
