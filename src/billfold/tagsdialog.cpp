#include "tagsdialog.h"
#include "ui_tagsdialog.h"

void TagsDialog::itemSelected(QTreeWidgetItem *item)
{
  bool isIncomings;
  auto *parent = item->parent();
  isIncomings = (item && item->text(0).startsWith(tr("Incomings")))
                || (parent && parent->text(0).startsWith(tr("Incomings")));
  emit tagSelected(QString(item ? item->text(0) : ""), isIncomings);
}

void TagsDialog::on_pushButton_clicked()
{
  if (!ui->manual->text().isEmpty())
      emit tagSelected(ui->manual->text(), false);
  else
  {
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    if (item)
      itemSelected(item);
  }
  close();
}

void TagsDialog::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
  (void)column;
  itemSelected(item);
  close();
}

void TagsDialog::setTags(const std::map<QString, QStringList> &list)
{
  for (auto it = list.begin(); it != list.end(); ++it)
  {
    QString name = it->first;
    if (name.startsWith("Incomings"))
      name = tr("Incomings (Earnings)");
    QTreeWidgetItem *item = new QTreeWidgetItem((QTreeWidgetItem*)nullptr);
    item->setText(0, name);
    for (int i = 0; i < it->second.size(); ++i)
    {
      if (it->second[i] != name)
      {
        QTreeWidgetItem *child = new QTreeWidgetItem(item);
        child->setText(0, it->second[i]);
      }
    }
    ui->treeWidget->addTopLevelItem(item);
  }
  ui->treeWidget->expandAll();
}

TagsDialog::TagsDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::TagsDialog)
{
  ui->setupUi(this);
}

TagsDialog::~TagsDialog()
{
  delete ui;
}
