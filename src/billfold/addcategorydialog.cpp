#include "addcategorydialog.h"
#include "ui_addcategorydialog.h"

#include <QJsonObject>
#include <set>

QString AddCategoryDialog::getPrimaryName()
{
  return ui->pNameLine->text();
}

QString AddCategoryDialog::getGroupName()
{
  return ui->groupCBox->currentText();
}

void AddCategoryDialog::addCategories(engine::SIRecordIterator categories)
{
  ui->groupCBox->clear();
  ui->groupCBox->addItem("");
  std::set<QString> list;
  for (categories.next(); categories.isValid(); categories.next())
    list.insert({ categories["second"].toString() });
  for (auto it = list.begin(); it != list.end(); ++it)
    ui->groupCBox->addItem(*it);
}

AddCategoryDialog::AddCategoryDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AddCategoryDialog)
{
  ui->setupUi(this);
  connect(ui->okBtn,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
  ui->okBtn->setDefault(true);
}

AddCategoryDialog::~AddCategoryDialog()
{
  delete ui;
}
