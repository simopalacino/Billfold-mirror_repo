create table conti
(
    conto      int                    not null
        constraint id_conti_pk
            primary key,
    name       varchar(16)            not null,
    owner      varchar(122)           not null,
    banca      varchar(64),
    contanti   bit  default 0,
    risparmio  bit  default 0,
    init_value numeric(14, 2)         not null,
    date_init  date default getdate() not null,
    value      numeric(14, 2)         not null
)
go

alter table entrate
	add conto int default 0
go

alter table entrate
	add constraint conto_entrate_conti__fk
		foreign key (conto) references conti (conto)
go



alter table uscite
	add conto int default 0
go

alter table uscite
	add constraint conto_uscite_conti__fk
		foreign key (conto) references conti (conto)
go

alter table eliminate
	add conto int default 0
go


---------------------
CREATE or alter TRIGGER dbo.DeleteUscite
    ON dbo.uscite
    AFTER DELETE
    AS
    INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto)
    VALUES ('SI', (SELECT TOP 1 deleted.id FROM deleted),
            (SELECT TOP 1 deleted.date FROM deleted),
            (SELECT TOP 1 deleted.cifra FROM deleted),
            (SELECT TOP 1 deleted.tag FROM deleted),
            (SELECT TOP 1 deleted.description FROM deleted),
            (SELECT TOP 1 deleted.conto FROM deleted))
go

CREATE TRIGGER dbo.update_conto_uscite_on_del
    ON uscite
    AFTER DELETE
    AS
BEGIN
    DECLARE
        @id int,
        @date date,
        @cifra numeric(18, 2),
        @tag varchar(32),
        @description varchar(1024),
        @conto int;
    DECLARE del CURSOR FOR SELECT * FROM deleted
    OPEN del
    FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE dbo.conti
            SET value = value + @cifra
            WHERE conto = @conto
            FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto
        END
    CLOSE del
    DEALLOCATE del
END
go

CREATE TRIGGER dbo.update_conto_uscite_on_ins
    ON uscite
    AFTER INSERT
    AS
BEGIN
    DECLARE
        @id int,
        @date date,
        @cifra numeric(18, 2),
        @tag varchar(32),
        @description varchar(1024),
        @conto int;

    DECLARE new_ins CURSOR FOR SELECT * FROM inserted

    OPEN new_ins

    FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE dbo.conti
            SET value = value - @cifra
            WHERE conto = @conto
            FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto
        END

    CLOSE new_ins
    DEALLOCATE new_ins
END
go

CREATE TRIGGER dbo.update_conto_uscite_on_update
    ON uscite
    AFTER UPDATE
    AS
BEGIN
    DECLARE @new_value numeric(18, 2)
    DECLARE
        @a_id int,
        @a_date date,
        @a_cifra numeric(18, 2),
        @a_tag varchar(32),
        @a_description varchar(1024),
        @a_conto int;
    DECLARE
        @b_id int,
        @b_date date,
        @b_cifra numeric(18, 2),
        @b_tag varchar(32),
        @b_description varchar(1024),
        @b_conto int;

    DECLARE _after CURSOR FOR SELECT * FROM inserted
    DECLARE _before CURSOR FOR SELECT * FROM deleted

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto
    FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SELECT @new_value = value + @b_cifra - @a_cifra
            FROM dbo.conti
            WHERE conto = @a_conto;

            UPDATE dbo.conti
            SET value = @new_value
            WHERE conto = @a_conto

            FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto
            FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto
        END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go

---------------------------------------
CREATE or alter TRIGGER dbo.DeleteEntrate
    ON dbo.entrate
    AFTER DELETE
    AS
    INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto)
    VALUES ('SI', (SELECT TOP 1 deleted.id FROM deleted),
            (SELECT TOP 1 deleted.date FROM deleted),
            (SELECT TOP 1 deleted.cifra FROM deleted),
            (SELECT TOP 1 deleted.tag FROM deleted),
            (SELECT TOP 1 deleted.description FROM deleted),
            (SELECT TOP 1 deleted.conto FROM deleted))
go

CREATE TRIGGER dbo.update_conto_Entrate_on_del
    ON entrate
    AFTER DELETE
    AS
BEGIN
    DECLARE
        @id int,
        @date date,
        @cifra numeric(18, 2),
        @tag varchar(32),
        @description varchar(1024),
        @conto int;
    DECLARE del CURSOR FOR SELECT * FROM deleted
    OPEN del
    FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE dbo.conti
            SET value = value + @cifra
            WHERE conto = @conto
            FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto
        END
    CLOSE del
    DEALLOCATE del
END
go

CREATE TRIGGER dbo.update_conto_entrate_on_ins
    ON entrate
    AFTER INSERT
    AS
BEGIN
    DECLARE
        @id int,
        @date date,
        @cifra numeric(18, 2),
        @tag varchar(32),
        @description varchar(1024),
        @conto int;

    DECLARE new_ins CURSOR FOR SELECT * FROM inserted

    OPEN new_ins

    FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE dbo.conti
            SET value = value - @cifra
            WHERE conto = @conto
            FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto
        END

    CLOSE new_ins
    DEALLOCATE new_ins
END
go

CREATE TRIGGER dbo.update_conto_entrate_on_update
    ON entrate
    AFTER UPDATE
    AS
BEGIN
    DECLARE @new_value numeric(18, 2)
    DECLARE
        @a_id int,
        @a_date date,
        @a_cifra numeric(18, 2),
        @a_tag varchar(32),
        @a_description varchar(1024),
        @a_conto int;
    DECLARE
        @b_id int,
        @b_date date,
        @b_cifra numeric(18, 2),
        @b_tag varchar(32),
        @b_description varchar(1024),
        @b_conto int;

    DECLARE _after CURSOR FOR SELECT * FROM inserted
    DECLARE _before CURSOR FOR SELECT * FROM deleted

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto
    FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SELECT @new_value = value + @b_cifra - @a_cifra
            FROM dbo.conti
            WHERE conto = @a_conto;

            UPDATE dbo.conti
            SET value = @new_value
            WHERE conto = @a_conto

            FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto
            FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto
        END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Billfold Version

-------------------------------------------------------
-- users
create table users
(
    id          int identity
        constraint table_name_pk
            primary key nonclustered,
    username    varchar(32)  not null,
    password    varchar(129) not null,
    type        int          not null,
    last_update datetime2 default sysdatetime()
)
go

create unique index table_name_username_uindex
    on users (username)
go

CREATE TRIGGER dbo.update_last_update_users_on_update
    ON dbo.users
    AFTER UPDATE
    AS
BEGIN
    DECLARE @aid int
    DECLARE _cursor CURSOR FOR SELECT id FROM inserted
    OPEN _cursor
    FETCH NEXT FROM _cursor INTO @aid

    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE dbo.users
            SET last_update = SYSDATETIME()
            WHERE id = @aid;
            -- ++i
            FETCH NEXT FROM _cursor INTO @aid
        END
    CLOSE _cursor
    DEALLOCATE _cursor
END
go

INSERT INTO users (username, password, type) values
('admin','0054c78361bbb6b1f83c091be54a5fb69f1e8b49465d8d53af68b06800fb1a2fbe9e1d07a2160116cff98a5291f12b1f5084836ec79b4cff1bf5da641b304e8e',0),
('sara','3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79',1)


-------------------------------------------------------
-- registrations
create table registrations
(
    id          int identity
        constraint registrations_pk
            primary key nonclustered,
    currency    char(3)   default 'EUR' not null,
    note        varchar(1024),
    place       varchar(1024),
    deleted     bit       default 0     not null,
    user_id     int       default 0     not null
        constraint registrations_users_id_fk
            references users,
    last_update datetime2 default sysdatetime()
)
go

create unique index registrations_id_user_id_uindex
    on registrations (id, user_id)
go

CREATE TRIGGER dbo.update_last_update_registrations_on_update
    ON registrations
    AFTER UPDATE
    AS
BEGIN
    DECLARE @aid int
    DECLARE _aftera CURSOR FOR SELECT id FROM inserted
    OPEN _aftera
    FETCH NEXT FROM _aftera INTO @aid

    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE registrations
            SET last_update = SYSDATETIME()
            WHERE id = @aid;
            -- ++i
            FETCH NEXT FROM _aftera INTO @aid
        END
    CLOSE _aftera
    DEALLOCATE _aftera
END
go

-------------------------------------------------------
-- categories
create table categories
(
    name        varchar(32)                     not null,
    last_update datetime2 default sysdatetime() not null,
    user_id     int       default 0             not null
        constraint categories_users_id_fk
            references users,
    id          int identity
        constraint categories_pk
            primary key nonclustered,
    parent_id   int
        constraint categories_categories_id_fk
            references categories
)
go

create unique index categories_name_user_id_uindex
    on categories (name, user_id)
go

create unique index categories_id_user_id_uindex
    on categories (id, user_id)
go

create trigger dbo.update_last_update_category_on_update
    on categories
    after update as
begin
    declare @id int
    declare cur cursor for SELECT id from deleted
    open cur
    fetch next from cur into @id
    while @@fetch_status = 0 begin
        update categories set last_update = default where id = @id
        fetch next from cur into @id
    end
    deallocate cur
end
go

---------------------------------------------------------
-- entrate
alter table entrate
    add acc_date date
go
alter table entrate
    add nota_cred int
        constraint entrate_nc_uscite_id_fk
            references uscite
go
alter table entrate
    add time time
go
alter table entrate
    add registration int
        constraint entrate_registrations_id_fk
            references registrations
go
alter table entrate
    add last_update  datetime2      default sysdatetime()
go
alter table entrate
    add user_id      int            default 2                      not null
        constraint entrate_users_id_fk
            references users
go
alter table entrate
    add category int
        constraint entrate_categories_id_fk
            references categories
go
alter table entrate
    add causal varchar(64)
go

CREATE TRIGGER dbo.update_last_update_entrate_on_update
    ON entrate
    AFTER UPDATE
    AS
BEGIN
    DECLARE @aid int
    DECLARE _aftera CURSOR FOR SELECT id FROM inserted
    OPEN _aftera
    FETCH NEXT FROM _aftera INTO @aid

    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE entrate
            SET last_update = SYSDATETIME()
            WHERE id = @aid;
            -- ++i
            FETCH NEXT FROM _aftera INTO @aid
        END
    CLOSE _aftera
END
go

CREATE or alter TRIGGER dbo.update_conto_entrate_on_update
    ON entrate
    AFTER UPDATE
    AS
BEGIN
    DECLARE @new_value numeric(18, 2)
    DECLARE @a_cifra numeric(18, 2), @a_conto int
    DECLARE @b_cifra numeric(18, 2), @b_conto int

    DECLARE _after CURSOR FOR SELECT cifra, conto FROM inserted
    DECLARE _before CURSOR FOR SELECT cifra, conto FROM deleted

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after INTO @a_cifra, @a_conto
    FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SELECT @new_value = value + @b_cifra - @a_cifra
            FROM dbo.conti
            WHERE conto = @a_conto;

            UPDATE dbo.conti
            SET value = @new_value
            WHERE conto = @a_conto

            FETCH NEXT FROM _after INTO @a_cifra, @a_conto
            FETCH NEXT FROM _before INTO @b_cifra, @b_conto
        END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go



---------------------------------------------------------
-- uscite
alter table uscite
    add acc_date date
go
alter table uscite
    add nota_cred int
        constraint uscite_nc_uscite_id_fk
            references uscite
go
alter table uscite
    add time time
go
alter table uscite
    add registration int
        constraint uscite_registrations_id_fk
            references registrations
go
alter table uscite
    add last_update  datetime2      default sysdatetime()
go

alter table uscite
    add user_id      int            default 2                      not null
        constraint uscite_users_id_fk
            references users
go

alter table uscite
    add category int
        constraint uscite_categories_id_fk
            references categories
go
alter table uscite
    add causal varchar(64)
go

CREATE TRIGGER dbo.update_last_update_uscite_on_update
    ON uscite
    AFTER UPDATE
    AS
BEGIN
    DECLARE @aid int
    DECLARE _aftera CURSOR FOR SELECT id FROM inserted
    OPEN _aftera
    FETCH NEXT FROM _aftera INTO @aid

    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE uscite
            SET last_update = SYSDATETIME()
            WHERE id = @aid;
            -- ++i
            FETCH NEXT FROM _aftera INTO @aid
        END
    CLOSE _aftera
END
go

CREATE or alter TRIGGER dbo.update_conto_uscite_on_update
    ON uscite
    AFTER UPDATE
    AS
BEGIN
    DECLARE @new_value numeric(18, 2)
    DECLARE @a_cifra numeric(18, 2), @a_conto int
    DECLARE @b_cifra numeric(18, 2), @b_conto int

    DECLARE _after CURSOR FOR SELECT cifra, conto FROM inserted
    DECLARE _before CURSOR FOR SELECT cifra, conto FROM deleted

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after INTO @a_cifra, @a_conto
    FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SELECT @new_value = value + @b_cifra - @a_cifra
            FROM dbo.conti
            WHERE conto = @a_conto;

            UPDATE dbo.conti
            SET value = @new_value
            WHERE conto = @a_conto

            FETCH NEXT FROM _after INTO @a_cifra, @a_conto
            FETCH NEXT FROM _before INTO @b_cifra, @b_conto
        END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go


-------------------------------------------------------------
-- conversion from tag to categories
-- INSERT TAG OUT
INSERT INTO categories (name, parent_id, user_id) VALUES ('Incomings', NULL, 2);
INSERT INTO categories (name, parent_id, user_id) VALUES ('Transfer', NULL, 2);

BEGIN
    DECLARE @id int, @name VARCHAR(32)
    DECLARE cur CURSOR FOR SELECT name FROM tag_out

    OPEN cur

    FETCH NEXT FROM cur INTO @name
    WHILE @@FETCH_STATUS = 0 BEGIN
        INSERT INTO categories (name, parent_id, user_id) VALUES (@name, NULL, 2);
        FETCH NEXT FROM cur INTO @name
    END

    CLOSE cur
    DEALLOCATE cur
    select 1
END

-- UPDATE PARENT ID TAG OUT
BEGIN
    DECLARE @id int, @id_p int, @first VARCHAR(32), @second VARCHAR(32)
    DECLARE curr CURSOR FOR SELECT first, second FROM t_out_is_a

    OPEN curr
    FETCH NEXT FROM curr INTO @first, @second
    WHILE @@FETCH_STATUS = 0 BEGIN
        if @first <> @second begin
            select @id = id from categories where name = @first
            select @id_p = id from categories where name = @second
            UPDATE categories SET parent_id = @id_p WHERE id = @id
        end
        FETCH NEXT FROM curr INTO @first, @second
    END

    CLOSE curr
    DEALLOCATE curr
    select 1
END

-- INSERT INCOMINGS
BEGIN
    DECLARE @id int, @out int, @name VARCHAR(32)
    DECLARE cur CURSOR FOR SELECT name FROM tag_in

    SELECT @id = id FROM categories WHERE name = 'Incomings'
    if @id IS NOT NULL BEGIN
        OPEN cur

        FETCH NEXT FROM cur INTO @name
        WHILE @@FETCH_STATUS = 0 BEGIN
            SELECT @out = NULL
            select @out = id from categories where name = @name
            if @out is null
                INSERT INTO categories (name, parent_id, user_id) VALUES (@name, @id, 2)
            FETCH NEXT FROM cur INTO @name
        END

        CLOSE cur
    end
    DEALLOCATE cur
    select 1
END

-- UPDATE CATEGORY COLUMN IN ENTRATE AND USCITE
BEGIN
    DECLARE @id_cat int, @id int, @tag VARCHAR(32)
    DECLARE cur CURSOR FOR SELECT id, tag FROM dbo.entrate

    OPEN cur
    FETCH NEXT FROM cur INTO @id, @tag
    WHILE @@FETCH_STATUS = 0 BEGIN
        SELECT @id_cat = id from categories where name = @tag
        UPDATE entrate SET entrate.category = @id_cat WHERE id = @id

        FETCH NEXT FROM cur INTO @id, @tag
    END

    CLOSE cur
    DEALLOCATE cur
    select 1
end
BEGIN
    DECLARE @id_cat int, @id int, @tag VARCHAR(32)
    DECLARE cur CURSOR FOR SELECT id, tag FROM uscite

    OPEN cur
    FETCH NEXT FROM cur INTO @id, @tag
    WHILE @@FETCH_STATUS = 0 BEGIN
        SELECT @id_cat = id from categories where name = @tag
        UPDATE uscite SET category = @id_cat WHERE id = @id

        FETCH NEXT FROM cur INTO @id, @tag
    END

    CLOSE cur
    DEALLOCATE cur
    select 1
end

alter table uscite alter column tag varchar(32) null
go
alter table entrate alter column tag varchar(32) null
go

alter table uscite alter column category int not null
go
alter table entrate alter column category int not null
go

exec sp_rename 'conti', accounts, 'OBJECT'
go

------------------------------------
-- update eliminate
alter table eliminate
    add acc_date date
go
alter table eliminate
    add nota_cred int
go

CREATE or alter  TRIGGER dbo.DeleteUscite
ON dbo.uscite
AFTER DELETE
AS begin
INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto, acc_date, nota_cred)
VALUES ('SI', (SELECT TOP 1 deleted.id FROM deleted),
        (SELECT TOP 1 deleted.date FROM deleted),
        (SELECT TOP 1 deleted.cifra FROM deleted),
        (SELECT TOP 1 c.name FROM deleted join categories c on deleted.category = c.id),
        (SELECT TOP 1 deleted.description FROM deleted),
        (SELECT TOP 1 deleted.conto FROM deleted),
        (SELECT TOP 1 deleted.acc_date FROM deleted),
        (SELECT TOP 1 deleted.nota_cred FROM deleted))

    declare @reg int, @count int
    declare cur cursor for select registration from deleted d
    open cur

    fetch next from cur into @reg
    while @@fetch_status = 0 begin
        select @count = count(id) from (
            select id from uscite u where registration = @reg
            union
            select id from entrate u where registration = @reg
        ) a
        if @count = 0 begin
            delete from registrations where id = @reg
        end
        fetch next from cur into @reg
    end

    deallocate cur
end
go

-----------------------------------------------------------------------------------
ALTER TABLE accounts
add color       int       default 0         not null
go
ALTER TABLE accounts
add sorting     int
go
ALTER TABLE accounts
add visible     bit       default 1         not null
go
ALTER TABLE accounts
add
    virtual     bit       default 0         not null
go
ALTER TABLE accounts
add
    last_update datetime2 default sysdatetime()
go
ALTER TABLE accounts
add
    user_id     int       default 2         not null
        constraint accounts_users_id_fk
            references users
go


create unique index conti_conto_user_id_uindex
    on accounts (conto, user_id)
go

CREATE TRIGGER dbo.update_last_update_accounts_on_update
    ON accounts
    AFTER UPDATE
    AS
BEGIN
    DECLARE @aid int
    DECLARE _afteracc CURSOR FOR SELECT conto FROM inserted
    OPEN _afteracc
    FETCH NEXT FROM _afteracc INTO @aid

    WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE accounts
            SET last_update = SYSDATETIME()
            WHERE conto = @aid;
            -- ++i
            FETCH NEXT FROM _afteracc INTO @aid
        END
    CLOSE _afteracc
    deallocate _afteracc
END
go

begin
    DECLARE @aid int
    DECLARE cur CURSOR FOR SELECT conto FROM accounts
    OPEN cur
    FETCH NEXT FROM cur INTO @aid

    WHILE @@FETCH_STATUS = 0 BEGIN
        update accounts set accounts.sorting = @aid where conto = @aid
        FETCH NEXT FROM cur INTO @aid
    END
    CLOSE cur
    deallocate cur
end

ALTER TABLE accounts
ALTER COLUMN sorting     int NOT NULL
go

create unique index conti_sorting_uindex
    on accounts (sorting)
go

--------------------------
-- routines
CREATE FUNCTION dbo.amounts_registrations() returns table
    return SELECT SUM(cifra) as cifra, registration FROM (
               SELECT -cifra as cifra, registration
               FROM uscite u
               WHERE registration IS NOT NULL
               UNION
               SELECT cifra as cifra, registration
               FROM entrate e
               WHERE registration IS NOT NULL
           ) a
          GROUP BY registration
go

create   function dbo.registrations_header()
returns @aaa table (
    id          int PRIMARY KEY NOT NULL,
    first_date  date,
    description varchar(1024),
    category    int,
    amount      NUMERIC(18, 2),
    currency    char(3),
    note        varchar(1024),
    place       varchar(1024),
    last_update datetime2
) as begin
    declare @uid int, @date date, @time time, @description varchar(1024), @registration int, @ulast_update datetime2, @uuser_id int, @category int, @causal varchar(64), @currency char(3), @note varchar(1024), @place varchar(1024)
    declare cur cursor for
    select u.id, date, time, description, registration, u.last_update, category, causal, r2.currency, r2.note, r2.place
        from uscite u join registrations r2 on u.registration = r2.id
    union
    select u.id, date, time, description, registration, u.last_update, category, causal, r2.currency, r2.note, r2.place
        from entrate u join registrations r2 on u.registration = r2.id
    order by registration, date, time

    open cur
    declare @last_id int, @amount numeric(18, 2)
    select @last_id = NULL
    fetch next from cur into @uid, @date, @time, @description, @registration, @ulast_update, @category, @causal, @currency, @note, @place
    while @@fetch_status = 0 begin
        if @last_id is null or @registration <> @last_id begin
            select @amount = cifra from dbo.amounts_registrations() where registration = @registration
            insert into @aaa (id, first_date, description, category, amount, currency, note, place, last_update)
            values (@registration, @date, @description, @category, @amount, @currency, @note, @place, @ulast_update)

            select @last_id = @registration
        end

        fetch next from cur into @uid, @date, @time, @description, @registration, @ulast_update, @category, @causal, @currency, @note, @place
    end
    return
end
go

create   function dbo.get_category_id_by_name(@name as varchar(32), @user_id int) returns int as begin
    declare @id int
    select @id = id from categories where name = @name and user_id = @user_id
    return @id
end
go

create function dbo.last_id_registration() returns int as
begin
    declare @id int
    declare cur cursor for
    SELECT id from registrations order by id desc
    open cur
    fetch next from cur into @id
    deallocate cur
    return @id
end
go

CREATE  or alter TRIGGER dbo.update_conto_uscite_on_ins
ON uscite
AFTER INSERT
AS BEGIN
    DECLARE
        @cifra numeric(18,2),
        @conto int
    DECLARE new_ins CURSOR FOR SELECT cifra, conto FROM inserted
    OPEN new_ins
    FETCH NEXT FROM new_ins INTO @cifra, @conto
    WHILE @@FETCH_STATUS = 0 BEGIN
        UPDATE dbo.accounts SET value = value - @cifra WHERE conto = @conto
        FETCH NEXT FROM new_ins INTO @cifra, @conto
    END
    DEALLOCATE new_ins
END
go

CREATE or alter  TRIGGER dbo.update_conto_uscite_on_del
ON uscite
AFTER DELETE
AS BEGIN
    DECLARE
        @cifra numeric(18,2),
        @conto int

    DECLARE del CURSOR FOR select cifra, conto from deleted d
    OPEN del
    FETCH NEXT FROM del INTO @cifra, @conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        UPDATE dbo.accounts
        SET value = value + @cifra
        WHERE conto = @conto
        FETCH NEXT FROM del INTO @cifra, @conto
    END
    CLOSE del
    DEALLOCATE del
END
go

CREATE or alter TRIGGER dbo.update_conto_uscite_on_update
ON uscite
AFTER UPDATE
AS BEGIN
    DECLARE @new_value numeric(18,2)
    DECLARE
        @a_cifra numeric(18,2),
        @a_conto int
    DECLARE
        @b_cifra numeric(18,2),
        @b_conto int

    DECLARE _after  CURSOR FOR SELECT cifra, conto FROM inserted d
    DECLARE _before CURSOR FOR SELECT cifra, conto FROM deleted d

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
    FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Update conto (before)
        SELECT @new_value = value + @b_cifra
        FROM dbo.accounts
        WHERE conto = @b_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @b_conto

        -- Update conto (after)
        SELECT @new_value = value - @a_cifra
        FROM dbo.accounts
        WHERE conto = @a_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @a_conto

        -- ++i
        FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
        FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go

CREATE or alter TRIGGER dbo.update_conto_entrate_on_del
ON entrate
AFTER DELETE
AS BEGIN
    DECLARE
        @cifra numeric(18,2),
        @conto int

    DECLARE del CURSOR FOR select cifra, conto from deleted d
    OPEN del
    FETCH NEXT FROM del INTO @cifra, @conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        UPDATE dbo.accounts
        SET value = value - @cifra
        WHERE conto = @conto
        FETCH NEXT FROM del INTO @cifra, @conto
    END
    CLOSE del
    DEALLOCATE del
END
go

CREATE or alter TRIGGER dbo.update_conto_entrate_on_ins
ON entrate
AFTER INSERT
AS BEGIN
    DECLARE
        @cifra numeric(18,2),
        @conto int

    DECLARE new_ins CURSOR FOR SELECT cifra, conto FROM inserted d
    OPEN new_ins
    FETCH NEXT FROM new_ins INTO @cifra, @conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        UPDATE dbo.accounts
        SET value = value + @cifra
        WHERE conto = @conto
        FETCH NEXT FROM new_ins INTO @cifra, @conto
    END
    CLOSE new_ins
    DEALLOCATE new_ins
END
go

CREATE or alter TRIGGER dbo.update_conto_entrate_on_update
ON entrate
AFTER UPDATE
AS BEGIN
    DECLARE @new_value numeric(18,2)
    DECLARE
        @a_cifra numeric(18,2),
        @a_conto int
    DECLARE
        @b_cifra numeric(18,2),
        @b_conto int
    DECLARE _after  CURSOR FOR SELECT cifra, conto FROM inserted d
    DECLARE _before CURSOR FOR SELECT cifra, conto FROM deleted d
    OPEN _after
    OPEN _before
    FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
    FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Update conto (before)
        SELECT @new_value = value - @b_cifra
        FROM dbo.accounts
        WHERE conto = @b_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @b_conto;

        -- Update conto (after)
        SELECT @new_value = value + @a_cifra
        FROM dbo.accounts
        WHERE conto = @a_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @b_conto;

        -- ++i
        FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
        FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    END
    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END
go

CREATE or alter TRIGGER dbo.DeleteEntrate
ON dbo.entrate
AFTER DELETE
AS begin
INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto, acc_date, nota_cred)
VALUES ('NO', (SELECT TOP 1 deleted.id FROM deleted),
        (SELECT TOP 1 deleted.date FROM deleted),
        (SELECT TOP 1 deleted.cifra FROM deleted),
        (SELECT TOP 1 c.name FROM deleted join categories c on deleted.category = c.id),
        (SELECT TOP 1 deleted.description FROM deleted),
        (SELECT TOP 1 deleted.conto FROM deleted),
        (SELECT TOP 1 deleted.acc_date FROM deleted),
        (SELECT TOP 1 deleted.nota_cred FROM deleted))

    declare @reg int, @count int
    declare cur cursor for select registration from deleted d
    open cur

    fetch next from cur into @reg
    while @@fetch_status = 0 begin
        select @count = count(id) from (
            select id from uscite u where registration = @reg
            union
            select id from entrate u where registration = @reg
        ) a
        if @count = 0 begin
            delete from registrations where id = @reg
        end
        fetch next from cur into @reg
    end

    deallocate cur
end
go

CREATE or alter TRIGGER dbo.DeleteUscite
ON dbo.uscite
AFTER DELETE
AS begin
INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto, acc_date, nota_cred)
VALUES ('SI', (SELECT TOP 1 deleted.id FROM deleted),
        (SELECT TOP 1 deleted.date FROM deleted),
        (SELECT TOP 1 deleted.cifra FROM deleted),
        (SELECT TOP 1 c.name FROM deleted join categories c on deleted.category = c.id),
        (SELECT TOP 1 deleted.description FROM deleted),
        (SELECT TOP 1 deleted.conto FROM deleted),
        (SELECT TOP 1 deleted.acc_date FROM deleted),
        (SELECT TOP 1 deleted.nota_cred FROM deleted))

    declare @reg int, @count int
    declare cur cursor for select registration from deleted d
    open cur

    fetch next from cur into @reg
    while @@fetch_status = 0 begin
        select @count = count(id) from (
            select id from uscite u where registration = @reg
            union
            select id from entrate u where registration = @reg
        ) a
        if @count = 0 begin
            delete from registrations where id = @reg
        end
        fetch next from cur into @reg
    end

    deallocate cur
end
go
