#ifndef SQLRECORDRECORD_H
#define SQLRECORDRECORD_H

#include <QSqlRecord>
#include "irecord.h"

namespace engine {


class SqlRecordRecord : public IRecord
{
  QSqlRecord _sqlRecord;
  // IRecord interface
public:
  virtual int      count()                    const override { return _sqlRecord.count(); }
  virtual QString  fieldName(int i)           const override { return _sqlRecord.fieldName(i); }
  virtual QVariant value(int i)               const override { return _sqlRecord.value(i); }
  virtual QVariant value(const QString& name) const override { return _sqlRecord.value(name); }
  virtual void setValue(int i, const QVariant &val)               override { _sqlRecord.setValue(i, val); }
  virtual void setValue(const QString &name, const QVariant &val) override { _sqlRecord.setValue(name, val); }

  virtual SIRecord getCopy() const override { IRecord* p = new SqlRecordRecord(*this); return p->getInstance(); }

  SqlRecordRecord(const QSqlRecord& other) : _sqlRecord(other) {  }
};


} // ::engine::

#endif // SQLRECORDRECORD_H
