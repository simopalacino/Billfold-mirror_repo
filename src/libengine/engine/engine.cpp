#include "engine.h"

#include <cstdio>

#include <QString>
#include <QSettings>

#include <engine/db/TDatabase>
#include "logger.h"


#define CHECK_CONTROLLER if (!m_controller) \
{ \
  logger("[%s:%d] m_controller nullptr", __FILE__, __LINE__); \
  return; \
}
#define END_NAMESPACE_ENGINE

namespace engine {


////////////////////////////////////////////////////////////////////////////////
/// engine::LibengineP
////////////////////////////////////////////////////////////////////////////////

class LibengineP    // private attributes and methods of class Libengine
{
  friend Libengine;

  db::TDatabase* m_db = nullptr;
  QSettings      m_config;
  QString        m_driverType;
  QString        m_connection_name;

  static std::map<QString, std::shared_ptr<db::TDatabase>> m_externalDbDrivers;

  bool initLib();
  void setDb();
  bool updateDb();

  static void recordDriverDatabase(std::shared_ptr<db::TDatabase> myDriver, QString name);
  LibengineP();       // todo
  ~LibengineP();
};

std::map<QString, std::shared_ptr<db::TDatabase>> LibengineP::m_externalDbDrivers;

bool LibengineP::initLib()
{
  m_config.beginGroup("Main");
  m_driverType = m_config.value("driver_type", m_driverType).toString();
  m_connection_name = "LIBENGINE_CONNECTION";
  setDb();
  if (m_db)
      return m_db->connect();
  return false;
}

void LibengineP::setDb()
{
  QString s(m_driverType);
  if (s == "ODBC")
    m_db = db::TDatabase::getDatabase("ODBC", m_connection_name);
  else if (s == "SQLITE")
    m_db = db::TDatabase::getDatabase("SQLITE", "");// TSQLiteDatabase(m_connection_name, m_sqlitePath);
  else
  {
    auto it = m_externalDbDrivers.find(s);
    if (it != m_externalDbDrivers.end())
      m_db = it->second.get();
  }
}

bool LibengineP::updateDb()
{
  int tableVersion = m_config.value("table_version", 0).toUInt();
  const bool ok = m_db->update(tableVersion);
  m_config.setValue("table_version", tableVersion);
  m_config.sync();
  return ok;
}

void LibengineP::recordDriverDatabase(std::shared_ptr<db::TDatabase> myDriver, QString name)
{
  m_externalDbDrivers.insert({ name, myDriver });
}

LibengineP::LibengineP() : m_db(nullptr), m_config("libconfig.ini", QSettings::IniFormat)
{
  logger("[%s:%d] %s %s", __FUNCTION__, __LINE__, QSTR2CSTR(m_config.fileName()), (m_config.scope() == QSettings::UserScope ? "UserScope" : "SystemScope"));
}

LibengineP::~LibengineP()
{
  if (m_db)
      m_db->disconnect();
  delete m_db;
  m_db = nullptr;
}


////////////////////////////////////////////////////////////////////////////////
/// engine::db::Libengine
////////////////////////////////////////////////////////////////////////////////

void Libengine::initSettings(QString driverType)
{
  m_controller->m_driverType     = driverType;
}

Libengine::state Libengine::initLib()
{
  if (!m_controller->initLib())
    return ST_ERROR_INITLIB;
  if (!m_controller->updateDb())
    return ST_ERROR_UPDATE_DB;
  return ST_READY;
}

QString Libengine::createId(const engine::SIRecord& record, uint id) const
{
  return m_controller->m_db->createId(record, id);
}

bool Libengine::login(const QString username, const QString password)
{
  return m_controller->m_db->login(username, password);
}

bool Libengine::changePwd(QString user, QString currentPwd, QString newPwd)
{
  return m_controller->m_db->changePwd(user, currentPwd, newPwd);
}

void Libengine::recordDriverDatabase(std::shared_ptr<db::TDatabase> myDriver, QString name)
{
  m_controller->recordDriverDatabase(myDriver, name);
}




// INTERFACE IBILLFOLDDBMANAGER

SIRecord Libengine::selectRecord(const QString& idRecord) const
{
  return m_controller->m_db->selectRecord(idRecord);
}

bool Libengine::insertNewRecord(const SIRecord& record, uint* id)
{
  return m_controller->m_db->insertNewRecord(record, id);
}

bool Libengine::updateRecord(const QString& oldId, const SIRecord& record, uint* id)
{
  return m_controller->m_db->updateRecord(oldId, record, id);
}

bool Libengine::deleteRecord(const QString& idRecord)
{
  return m_controller->m_db->deleteRecord(idRecord);
}

SIRecordIterator Libengine::selectMovements(const QString& dateFrom, const QString& dateTo, const QString& accountsList) const
{
  return m_controller->m_db->selectMovements(dateFrom, dateTo, accountsList);
}

bool Libengine::insertNewAccount(const std::map<QString, QVariant>& record, uint* id)
{
  return m_controller->m_db->insertNewAccount(record, id);
}

bool Libengine::accountEditRecord(SIRecord& oldRec, SIRecord& newRec)
{
  return m_controller->m_db->accountEditRecord(oldRec, newRec);
}

bool Libengine::deleteAccount(const uint32_t id)
{
  return m_controller->m_db->deleteAccount(id);
}

SIRecordIterator Libengine::selectAccounts() const
{
  return m_controller->m_db->selectAccounts();
}

bool Libengine::insertNewCategory(const QString& primaryName, const QString& groupName, uint* id)
{
  return m_controller->m_db->insertNewCategory(primaryName, groupName, id);
}

void Libengine::categoryEditRecord(SIRecord& oldRec, SIRecord& newRec)
{
  m_controller->m_db->categoryEditRecord(oldRec, newRec);
}

bool Libengine::deleteCategory(const uint32_t id)
{
  return m_controller->m_db->deleteCategory(id);
}

SIRecordIterator Libengine::selectCategories() const
{
  return m_controller->m_db->selectCategories();
}

SIRecordIterator Libengine::customQuery(QString query)
{
  return m_controller->m_db->customQuery(query);
}

// ////////////////////////////////////////




Libengine::Libengine()
{
  m_controller = new LibengineP;
  initSettings();
  logger.open();
}

Libengine::~Libengine()
{
  delete m_controller;
  m_controller = nullptr;
}




} END_NAMESPACE_ENGINE
