#ifndef INSERTNEWACCOUNTACTION_H
#define INSERTNEWACCOUNTACTION_H

#include "engine.h"
#include "iaction.h"
#include <memory>

namespace engine {


class ENGINE_EXPORT InsertNewAccountAction : public IAction
{
  Libengine*  _lib;
  std::map<QString, QVariant> _record;
  uint        _idRecord;
  bool        _performed{ false };

  // IAction interface
public:
  virtual int exec() override;
  virtual int undo() override;

  virtual QString name() const override { return "InsertNewAccountAction"; }

  InsertNewAccountAction(Libengine* lib, const std::map<QString, QVariant>& record) : _lib(lib), _record(record) { }
};


} // ::engine::

#endif // INSERTNEWACCOUNTACTION_H
