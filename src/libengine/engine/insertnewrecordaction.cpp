#include "insertnewrecordaction.h"

int engine::InsertNewRecordAction::exec()
{
  uint id;
  if ((_performed = _lib->insertNewRecord(_record, &id)))
    _idRecord = _lib->createId(_record, id);
  return _performed ? 0 : -1;
}

int engine::InsertNewRecordAction::undo()
{
  bool ok;
  if (_performed)
    ok = _lib->deleteRecord(_idRecord);
  // performed --> deleted
  return !_performed || ok ? 0 : -1;
}
