#ifndef ENGINE_H
#define ENGINE_H

#include "engine_global.h"
#include <memory>

#include "ibillfolddbmanager.h"
#include "engine/db/tdatabase.h"


namespace engine {


class LibengineP;

////////////////////////////////////////////////////////////////
// class Libengine
////////////////////////////////////////////////////////////////
class ENGINE_EXPORT Libengine : public IBillfoldDBManager
{
  LibengineP *m_controller = nullptr;
public:
  enum state { ST_ERROR_INITLIB = 0, ST_ERROR_UPDATE_DB, ST_READY };

  void  initSettings(QString driverType = "ODBC");
  state initLib();

  QString createId(const engine::SIRecord& record, uint id) const;

  bool login(QString username, QString password);
  bool changePwd(QString user, QString currentPwd, QString newPwd);

  void recordDriverDatabase(std::shared_ptr<db::TDatabase> myDriver, QString name);

// IBillfoldDBManager interface
  virtual SIRecord selectRecord(const QString& idRecord)                                                                const override; /**< {"id": <"e" | "u">< id record >} */
  virtual bool     insertNewRecord(const engine::SIRecord& record, uint* id = nullptr)                                        override;
  virtual bool     updateRecord(const QString& oldId, const SIRecord& record, uint* id = nullptr)                             override;
  virtual bool     deleteRecord(const QString& idRecord)                                                                      override;
  virtual SIRecordIterator selectMovements(const QString& dateFrom, const QString& dateTo, const QString& accountsList) const override;

  virtual bool insertNewAccount(const std::map<QString, QVariant>& record, uint* id = nullptr)       override;
  virtual bool accountEditRecord(SIRecord& oldRec, SIRecord& newRec)                                 override;
  virtual bool deleteAccount(uint32_t id)                                                            override;
  virtual SIRecordIterator selectAccounts()                                                    const override;

  virtual bool insertNewCategory(const QString& primaryName, const QString& groupName, uint* id = nullptr)       override;
  virtual void categoryEditRecord(SIRecord& oldRec, SIRecord& newRec)                                            override;
  virtual bool deleteCategory(uint32_t id)                                                                       override;
  virtual SIRecordIterator selectCategories()                                                              const override;

  virtual SIRecordIterator customQuery(QString query) override;
// ////

  Libengine &operator=(const Libengine&) = delete;
  Libengine(const Libengine&)            = delete;
  Libengine();
  virtual ~Libengine();
};


} // namespace ::engine::

#endif // ENGINE_H
