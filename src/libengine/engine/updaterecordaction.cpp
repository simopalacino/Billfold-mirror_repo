#include "updaterecordaction.h"

int engine::UpdateRecordAction::exec()
{
  uint id;
  _oldRecord = _lib->selectRecord(_oldId);
  if (_oldRecord != nullptr)
  {
    _performed = _lib->updateRecord(_oldId, _record, &id);
    _idRecord  = _lib->createId(_record, id);
  }
  return _performed ? 0 : -1;
}

int engine::UpdateRecordAction::undo()
{
  int ok;
  if (_performed)
    ok = _lib->updateRecord(_idRecord, _oldRecord);
  // performed --> deleted
  return !_performed || ok ? 0 : -1;
}
