#include "updatersqlserverdb102.h"

#include <QSqlError>
#include <QSqlQuery>

#include "../logger.h"

bool UpdaterSQLServerDB102::exec(const QString &query)
{
  QSqlQuery sql(m_sqldb);
  if (!sql.exec(query))
  {
    engine::logger("[%s:%d] [sql.lastError]: %s\nQuery={\n%s\n}",
                       __PRETTY_FUNCTION__, __LINE__,
                       sql.lastError().databaseText().toLocal8Bit().data(),
                       query.toLocal8Bit().data());
    return false;
  }
  return m_sqldb.commit();
}

bool UpdaterSQLServerDB102::alterTriggerOnUpdateEntrate()
{
  QString query =
R"(CREATE OR ALTER TRIGGER dbo.update_conto_entrate_on_update
ON entrate
AFTER UPDATE
AS BEGIN
    DECLARE @new_value numeric(18,2)
    DECLARE
        @a_cifra numeric(18,2),
        @a_conto int
    DECLARE
        @b_cifra numeric(18,2),
        @b_conto int

    DECLARE _after  CURSOR FOR SELECT cifra, conto FROM inserted d
    DECLARE _before CURSOR FOR SELECT cifra, conto FROM deleted d

    OPEN _after
    OPEN _before

    FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
    FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Update conto (before)
        SELECT @new_value = value - @b_cifra
        FROM dbo.accounts
        WHERE conto = @b_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @b_conto;

        -- Update conto (after)
        SELECT @new_value = value + @a_cifra
        FROM dbo.accounts
        WHERE conto = @a_conto;

        UPDATE dbo.accounts
        SET value = @new_value
        WHERE conto = @a_conto;

        -- ++i
        FETCH NEXT FROM _after  INTO @a_cifra, @a_conto
        FETCH NEXT FROM _before INTO @b_cifra, @b_conto
    END

    CLOSE _after
    CLOSE _before
    DEALLOCATE _after
    DEALLOCATE _before
END)";

  return exec(query);
}
