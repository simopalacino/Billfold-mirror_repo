#ifndef QUERIES_H
#define QUERIES_H

const char *update0_query =
R"(use master;
-- drop database billfold
-- go

create database billfold;

use billfold;

create table eliminate
(
time        datetime default getdate() not null,
spesa       char(2)                    not null,
id          int                        not null,
date        date                       not null,
cifra       numeric(18, 2)             not null,
tag         varchar(32)                not null,
description varchar(1024)              not null,
conto       int      default 0,
acc_date    date,
nota_cred   int,
constraint eliminate_pk
primary key (spesa, id)
);

create table users
(
id       int identity
constraint table_name_pk
    primary key nonclustered,
username varchar(32) not null,
password varchar(64) not null,
type     int         not null
);

create table accounts
(
conto       int                         not null
constraint id_conti_pk
    primary key,
name        varchar(32)                 not null,
owner       varchar(122)                not null,
banca       varchar(64),
contanti    bit       default 0,
risparmio   bit       default 0,
init_value  numeric(14, 2)              not null,
date_init   date      default getdate() not null,
value       numeric(14, 2)              not null,
color       int       default 0         not null,
sorting     int                         not null,
visible     bit       default 1         not null,
virtual     bit       default 0         not null,
last_update datetime2 default sysdatetime(),
user_id     int       default 0         not null
constraint accounts_users_id_fk
    references users
);

create unique index conti_sorting_uindex
on accounts (sorting);

create unique index conti_conto_user_id_uindex
on accounts (conto, user_id);

CREATE TRIGGER dbo.update_last_update_accounts_on_update
ON accounts
AFTER UPDATE
AS
BEGIN
DECLARE @aid int
DECLARE _afteracc CURSOR FOR SELECT conto FROM inserted
OPEN _afteracc
FETCH NEXT FROM _afteracc INTO @aid

WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE accounts
    SET last_update = SYSDATETIME()
    WHERE conto = @aid
    -- ++i
    FETCH NEXT FROM _afteracc INTO @aid
END
CLOSE _afteracc
deallocate _afteracc
END;

create table categories
(
name        varchar(32)                     not null,
last_update datetime2 default sysdatetime() not null,
user_id     int       default 0             not null
constraint categories_users_id_fk
    references users,
id          int identity
constraint categories_pk
    primary key nonclustered,
parent_id   int
constraint categories_categories_id_fk
    references categories
);

create unique index categories_name_user_id_uindex
on categories (name, user_id);

create unique index categories_id_user_id_uindex
on categories (id, user_id);

create trigger dbo.update_last_update_category_on_update
on categories
after update as
begin
declare @id int
declare cur cursor for SELECT id from deleted
open cur
fetch next from cur into @id
while @@fetch_status = 0 begin
update categories set last_update = default where id = @id
fetch next from cur into @id
end
deallocate cur
end;

create table reg_risp
(
registration int           not null,
saving       int default 0 not null,
user_id      int default 0 not null
constraint reg_risp_users_id_fk
    references users,
constraint reg_risp_pk
primary key nonclustered (registration, saving)
);

create unique index reg_risp_registration_saving_user_id_uindex
on reg_risp (registration, saving, user_id);

CREATE TRIGGER dbo.update_conto_reg_risp_on_insert
ON dbo.reg_risp
AFTER insert
AS
BEGIN
DECLARE @reg int, @saving int, @account int, @perc NUMERIC(11, 8), @cifra NUMERIC(18, 2)
DECLARE new_ins CURSOR FOR SELECT registration, saving FROM inserted
OPEN new_ins

FETCH NEXT FROM new_ins INTO @reg, @saving
WHILE @@FETCH_STATUS = 0 BEGIN
SELECT @account = account, @perc = [percent] FROM savings s WHERE s.id = @saving
UPDATE conti SET value = dbo.amount_saving_by_id(@saving) * @perc WHERE conto = @account

FETCH NEXT FROM new_ins INTO @reg, @saving
END

CLOSE new_ins
DEALLOCATE new_ins
END;

create table registrations
(
id          int identity
constraint registrations_pk
    primary key nonclustered,
currency    char(3)   default 'EUR' not null,
note        varchar(1024),
place       varchar(1024),
deleted     bit       default 0     not null,
user_id     int       default 0     not null
constraint registrations_users_id_fk
    references users,
last_update datetime2 default sysdatetime()
);

create table entrate
(
id           int identity
constraint entrate_pk
    primary key,
date         date           default getdate()              not null,
cifra        numeric(18, 2) default '0'                    not null,
description  varchar(1024)
constraint entrate_description_default default 'other' not null,
conto        int            default 0                      not null
constraint conto_entrate_conti_fk
    references accounts,
acc_date     date,
nota_cred    int,
time         time,
registration int
constraint entrate_registrations_id_fk
    references registrations,
last_update  datetime2      default sysdatetime(),
user_id      int            default 0                      not null
constraint entrate_users_id_fk
    references users,
category     int                                           not null
constraint entrate_categories_id_fk
    references categories,
causal       varchar(64)
);

create unique index entrate_id_user_id_uindex
on entrate (id, user_id);

create index entrate_date_index
on entrate (date desc);

CREATE TRIGGER dbo.DeleteEntrate
ON dbo.entrate
AFTER DELETE
AS
begin
INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto, acc_date, nota_cred)
VALUES ('NO', (SELECT TOP 1 deleted.id FROM deleted),
    (SELECT TOP 1 deleted.date FROM deleted),
    (SELECT TOP 1 deleted.cifra FROM deleted),
    (SELECT TOP 1 c.name
     FROM deleted
              join categories c on deleted.category = c.id),
    (SELECT TOP 1 deleted.description FROM deleted),
    (SELECT TOP 1 deleted.conto FROM deleted),
    (SELECT TOP 1 deleted.acc_date FROM deleted),
    (SELECT TOP 1 deleted.nota_cred FROM deleted))

declare @reg int, @count int
declare cur cursor for select registration from deleted d
open cur

fetch next from cur into @reg
while @@fetch_status = 0 begin
select @count = count(id)
from (
         select id
         from uscite u
         where registration = @reg
         union
         select id
         from entrate u
         where registration = @reg
     ) a
if @count = 0
    begin
        delete from registrations where id = @reg
    end
fetch next from cur into @reg
end

deallocate cur
end;

CREATE TRIGGER dbo.update_conto_entrate_on_del
ON entrate
AFTER DELETE
AS
BEGIN
DECLARE
@id int,
@date date,
@cifra numeric(18, 2),
@tag varchar(32),
@description varchar(1024),
@conto int,
@acc_date date,
@nota_cred int

DECLARE del CURSOR FOR
select d.id,
       date,
       cifra,
       c.name as tag,
       description,
       conto,
       acc_date,
       nota_cred
from deleted d
         join categories c on d.category = c.id
OPEN del
FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE dbo.accounts
    SET value = value - @cifra
    WHERE conto = @conto
    FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
END
CLOSE del
DEALLOCATE del
END;

CREATE TRIGGER dbo.update_conto_entrate_on_ins
ON entrate
AFTER INSERT
AS
BEGIN
DECLARE
@id int,
@date date,
@cifra numeric(18, 2),
@tag varchar(32),
@description varchar(1024),
@conto int,
@acc_date date,
@nota_cred int

DECLARE new_ins CURSOR FOR SELECT d.id,
                              date,
                              cifra,
                              c.name as tag,
                              description,
                              conto,
                              acc_date,
                              nota_cred
                       FROM inserted d
                                join categories c on d.category = c.id
OPEN new_ins
FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE dbo.accounts
    SET value = value + @cifra
    WHERE conto = @conto
    FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
END
CLOSE new_ins
DEALLOCATE new_ins
END;

CREATE TRIGGER dbo.update_conto_entrate_on_update
ON entrate
AFTER UPDATE
AS
BEGIN
DECLARE @new_value numeric(18, 2)
DECLARE
@a_id int,
@a_date date,
@a_cifra numeric(18, 2),
@a_tag varchar(32),
@a_description varchar(1024),
@a_conto int,
@a_acc_date date,
@a_nota_cred int
DECLARE
@b_id int,
@b_date date,
@b_cifra numeric(18, 2),
@b_tag varchar(32),
@b_description varchar(1024),
@b_conto int,
@b_acc_date date,
@b_nota_cred int
DECLARE _after CURSOR FOR SELECT d.id,
                             date,
                             cifra,
                             c.name as tag,
                             description,
                             conto,
                             acc_date,
                             nota_cred
                      FROM inserted d
                               join categories c on d.category = c.id
DECLARE _before CURSOR FOR SELECT d.id,
                              date,
                              cifra,
                              c.name as tag,
                              description,
                              conto,
                              acc_date,
                              nota_cred
                       FROM deleted d
                                join categories c on d.category = c.id
OPEN _after
OPEN _before
FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto, @a_acc_date, @a_nota_cred
FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto, @b_acc_date, @b_nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    -- Update conto (before)
    SELECT @new_value = value - @b_cifra
    FROM dbo.accounts
    WHERE conto = @b_conto

    UPDATE dbo.accounts
    SET value = @new_value
    WHERE conto = @b_conto

    -- Update conto (after)
    SELECT @new_value = value + @a_cifra
    FROM dbo.accounts
    WHERE conto = @a_conto

    UPDATE dbo.accounts
    SET value = @new_value
    WHERE conto = @b_conto

    -- ++i
    FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto, @a_acc_date, @a_nota_cred
    FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto, @b_acc_date, @b_nota_cred
END
CLOSE _after
CLOSE _before
DEALLOCATE _after
DEALLOCATE _before
END;

CREATE TRIGGER dbo.update_last_update_entrate_on_update
ON entrate
AFTER UPDATE
AS
BEGIN
DECLARE @aid int
DECLARE _aftera CURSOR FOR SELECT id FROM inserted
OPEN _aftera
FETCH NEXT FROM _aftera INTO @aid

WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE entrate
    SET last_update = SYSDATETIME()
    WHERE id = @aid
    -- ++i
    FETCH NEXT FROM _aftera INTO @aid
END
CLOSE _aftera
END;

create unique index registrations_id_user_id_uindex
on registrations (id, user_id);

CREATE TRIGGER dbo.update_last_update_registrations_on_update
ON registrations
AFTER UPDATE
AS
BEGIN
DECLARE @aid int
DECLARE _aftera CURSOR FOR SELECT id FROM inserted
OPEN _aftera
FETCH NEXT FROM _aftera INTO @aid

WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE registrations
    SET last_update = SYSDATETIME()
    WHERE id = @aid
    -- ++i
    FETCH NEXT FROM _aftera INTO @aid
END
CLOSE _aftera
DEALLOCATE _aftera
END;

create table savings
(
id          int                       not null
constraint savings_pk
    primary key,
name        varchar(32),
description varchar(128),
date_init   date,
date_end    date,
account     int
constraint savings_account_conti_conto
    references accounts,
[percent]   numeric(11, 8) default 10 not null,
user_id     int            default 0  not null
constraint savings_users_id_fk
    references users
);

create unique index savings_id_user_id_uindex
on savings (id, user_id);

create table uscite
(
id           int identity
constraint uscite_pk
    primary key,
date         date           default getdate()             not null,
cifra        numeric(18, 2) default '0'                   not null,
description  varchar(1024)
constraint uscite_description_default default 'other' not null,
conto        int            default 0                     not null
constraint uscite_accounts_conto_fk
    references accounts,
acc_date     date,
nota_cred    int
constraint uscite_nc_entrate_id_fk
    references entrate,
time         time,
registration int
constraint uscite_registrations_id_fk
    references registrations,
last_update  datetime2      default sysdatetime(),
user_id      int            default 0                     not null
constraint uscite_users_id_fk
    references users,
category     int
constraint uscite_categories_id_fk
    references categories,
causal       varchar(64)
);

alter table entrate
add constraint entrate_nc_sucite_id_fk
foreign key (nota_cred) references uscite;

create index uscite_date_index
on uscite (date);

create unique index uscite_id_user_id_uindex
on uscite (id, user_id);

CREATE TRIGGER dbo.DeleteUscite
ON dbo.uscite
AFTER DELETE
AS
begin
INSERT INTO dbo.eliminate (spesa, id, date, cifra, tag, description, conto, acc_date, nota_cred)
VALUES ('SI', (SELECT TOP 1 deleted.id FROM deleted),
    (SELECT TOP 1 deleted.date FROM deleted),
    (SELECT TOP 1 deleted.cifra FROM deleted),
    (SELECT TOP 1 c.name
     FROM deleted
              join categories c on deleted.category = c.id),
    (SELECT TOP 1 deleted.description FROM deleted),
    (SELECT TOP 1 deleted.conto FROM deleted),
    (SELECT TOP 1 deleted.acc_date FROM deleted),
    (SELECT TOP 1 deleted.nota_cred FROM deleted))

declare @reg int, @count int
declare cur cursor for select registration from deleted d
open cur

fetch next from cur into @reg
while @@fetch_status = 0 begin
select @count = count(id)
from (
         select id
         from uscite u
         where registration = @reg
         union
         select id
         from entrate u
         where registration = @reg
     ) a
if @count = 0
    begin
        delete from registrations where id = @reg
    end
fetch next from cur into @reg
end

deallocate cur
end;

CREATE TRIGGER dbo.update_conto_uscite_on_del
ON uscite
AFTER DELETE
AS
BEGIN
DECLARE
@id int,
@date date,
@cifra numeric(18, 2),
@tag varchar(32),
@description varchar(1024),
@conto int,
@acc_date date,
@nota_cred int

DECLARE del CURSOR FOR
select d.id,
       date,
       cifra,
       c.name as tag,
       description,
       conto,
       acc_date,
       nota_cred
from deleted d
         join categories c on d.category = c.id
OPEN del
FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE dbo.accounts
    SET value = value + @cifra
    WHERE conto = @conto
    FETCH NEXT FROM del INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
END
CLOSE del
DEALLOCATE del
END;

CREATE TRIGGER dbo.update_conto_uscite_on_ins
ON uscite
AFTER INSERT
AS
BEGIN
DECLARE
@id int,
@date date,
@cifra numeric(18, 2),
@tag varchar(32),
@description varchar(1024),
@conto int,
@acc_date date,
@nota_cred int

DECLARE new_ins CURSOR FOR SELECT d.id,
                              date,
                              cifra,
                              c.name as tag,
                              description,
                              conto,
                              acc_date,
                              nota_cred
                       FROM inserted d
                                join categories c on d.category = c.id

OPEN new_ins

FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE dbo.accounts
    SET value = value - @cifra
    WHERE conto = @conto
    FETCH NEXT FROM new_ins INTO @id, @date, @cifra, @tag, @description, @conto, @acc_date, @nota_cred
END

CLOSE new_ins
DEALLOCATE new_ins
END;

CREATE TRIGGER dbo.update_conto_uscite_on_update
ON uscite
AFTER UPDATE
AS
BEGIN
DECLARE @new_value numeric(18, 2)
DECLARE
@a_id int,
@a_date date,
@a_cifra numeric(18, 2),
@a_tag varchar(32),
@a_description varchar(1024),
@a_conto int,
@a_acc_date date,
@a_nota_cred int
DECLARE
@b_id int,
@b_date date,
@b_cifra numeric(18, 2),
@b_tag varchar(32),
@b_description varchar(1024),
@b_conto int,
@b_acc_date date,
@b_nota_cred int

DECLARE _after CURSOR FOR SELECT d.id,
                             date,
                             cifra,
                             c.name as tag,
                             description,
                             conto,
                             acc_date,
                             nota_cred
                      FROM inserted d
                               join categories c on d.category = c.id
DECLARE _before CURSOR FOR SELECT d.id,
                              date,
                              cifra,
                              c.name as tag,
                              description,
                              conto,
                              acc_date,
                              nota_cred
                       FROM deleted d
                                join categories c on d.category = c.id

OPEN _after
OPEN _before

FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto, @a_acc_date, @a_nota_cred
FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto, @b_acc_date, @b_nota_cred
WHILE @@FETCH_STATUS = 0
BEGIN
    -- Update conto (before)
    SELECT @new_value = value + @b_cifra
    FROM dbo.accounts
    WHERE conto = @b_conto

    UPDATE dbo.accounts
    SET value = @new_value
    WHERE conto = @b_conto

    -- Update conto (after)
    SELECT @new_value = value - @a_cifra
    FROM dbo.accounts
    WHERE conto = @a_conto

    UPDATE dbo.accounts
    SET value = @new_value
    WHERE conto = @a_conto

    -- ++i
    FETCH NEXT FROM _after INTO @a_id, @a_date, @a_cifra, @a_tag, @a_description, @a_conto, @a_acc_date, @a_nota_cred
    FETCH NEXT FROM _before INTO @b_id, @b_date, @b_cifra, @b_tag, @b_description, @b_conto, @b_acc_date, @b_nota_cred
END

CLOSE _after
CLOSE _before
DEALLOCATE _after
DEALLOCATE _before
END;

CREATE TRIGGER dbo.update_last_update_uscite_on_update
ON uscite
AFTER UPDATE
AS
BEGIN
DECLARE @aid int
DECLARE _afteru CURSOR FOR SELECT id FROM inserted
OPEN _afteru
FETCH NEXT FROM _afteru INTO @aid

WHILE @@FETCH_STATUS = 0
BEGIN
    UPDATE uscite
    SET last_update = SYSDATETIME()
    WHERE id = @aid
    -- ++i
    FETCH NEXT FROM _afteru INTO @aid
END
CLOSE _afteru
deallocate _afteru
END;

create unique index table_name_username_uindex
on users (username);

CREATE VIEW unione_entrate_uscite_per_data AS
SELECT cifra.date, SUM(cifra.cifra) AS cifra
FROM (
 SELECT u.date,
        CASE
            WHEN u.cifra >= 0 THEN -u.cifra
            WHEN u.cifra < 0 THEN u.cifra
            END AS cifra
 FROM uscite u
 UNION
 SELECT e.date,
        CASE
            WHEN e.cifra >= 0 THEN e.cifra
            WHEN e.cifra < 0 THEN -e.cifra
            END AS cifra
 FROM entrate e
) cifra
GROUP BY cifra.date;

CREATE FUNCTION dbo.amount_saving_by_id(@id_saving INT) RETURNS NUMERIC(18, 2)
AS
BEGIN
DECLARE @cifra NUMERIC(18, 2)

SELECT @cifra = SUM(b.cifra)
FROM reg_risp rr
     JOIN (
SELECT SUM(cifra) as cifra, registration
FROM (
         SELECT -cifra as cifra, registration
         FROM uscite u
         WHERE registration IS NOT NULL
         UNION
         SELECT cifra as cifra, registration
         FROM entrate e
         WHERE registration IS NOT NULL
     ) a
GROUP BY registration
) b ON rr.registration = b.registration
WHERE saving = @id_saving

RETURN @cifra
END;

CREATE FUNCTION dbo.amounts_registrations()
returns table
return SELECT SUM(cifra) as cifra, registration
       FROM (
                SELECT -cifra as cifra, registration
                FROM uscite u
                WHERE registration IS NOT NULL
                UNION
                SELECT cifra as cifra, registration
                FROM entrate e
                WHERE registration IS NOT NULL
            ) a
       GROUP BY registration;

create function dbo.benzina()
returns table return
SELECT YEAR(date) AS year, SUM(cifra) AS cifra
FROM uscite
join categories c on uscite.category = c.id
WHERE c.name = 'Benzina'
GROUP BY YEAR(date);


create function dbo.get_category_id_by_name(@name as varchar(32), @user_id int) returns int as
begin
declare @id int
select @id = id from categories where name = @name and user_id = @user_id
return @id
end;

create function dbo.last_id_registration() returns int as
begin
declare @id int
declare cur cursor for
SELECT id from registrations order by id desc
open cur
fetch next from cur into @id
deallocate cur
return @id
end;

create function dbo.registrations_header()
returns @aaa table
         (
             id          int PRIMARY KEY NOT NULL,
             first_date  date,
             description varchar(1024),
             category    int,
             amount      NUMERIC(18, 2),
             currency    char(3),
             note        varchar(1024),
             place       varchar(1024),
             last_update datetime2
         ) as
begin
declare @uid int, @date date, @time time, @description varchar(1024), @registration int, @ulast_update datetime2, @uuser_id int, @category int, @causal varchar(64), @currency char(3), @note varchar(1024), @place varchar(1024)
declare cur cursor for
select u.id,
       date,
       time,
       description,
       registration,
       u.last_update,
       category,
       causal,
       r2.currency,
       r2.note,
       r2.place
from uscite u
         join registrations r2 on u.registration = r2.id
union
select u.id,
       date,
       time,
       description,
       registration,
       u.last_update,
       category,
       causal,
       r2.currency,
       r2.note,
       r2.place
from entrate u
         join registrations r2 on u.registration = r2.id
order by registration, date, time

open cur
declare @last_id int, @amount numeric(18, 2)
select @last_id = NULL
fetch next from cur into @uid, @date, @time, @description, @registration, @ulast_update, @category, @causal, @currency, @note, @place
while @@fetch_status = 0 begin
if @last_id is null or @registration <> @last_id
    begin
        select @amount = cifra from dbo.amounts_registrations() where registration = @registration
        insert into @aaa (id, first_date, description, category, amount, currency, note, place, last_update)
        values (@registration, @date, @description, @category, @amount, @currency, @note, @place, @ulast_update)

        select @last_id = @registration
    end

fetch next from cur into @uid, @date, @time, @description, @registration, @ulast_update, @category, @causal, @currency, @note, @place
end
return
end;

CREATE FUNCTION dbo.select_amounts_reg_risp()
RETURNS TABLE
RETURN SELECT rr.registration, b.cifra as amount, rr.saving
       FROM reg_risp rr
                JOIN dbo.amounts_registrations() b ON rr.registration = b.registration;

CREATE FUNCTION dbo.select_amount_registrations_per_savings()
RETURNS TABLE
return SELECT SUM(amount) as amount, saving
       FROM dbo.select_amounts_reg_risp()
       group by saving;

)";

#endif // QUERIES_H
