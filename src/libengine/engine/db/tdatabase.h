#ifndef TDATABASE_H
#define TDATABASE_H

#include "engine_global.h"
#include <QJsonObject>
#include <QSqlDatabase>

#include <memory>

#include "iupdaterdb100.h"
#include "iupdaterdb102.h"

#include <engine/ibillfolddbmanager.h>

namespace engine { namespace db {




class TDatabase : public IBillfoldDBManager
{
  QString notImplemented(const char *functionName) const;
  void    getJsonResult(QJsonObject &main, const QString &query, const QString &jsonName, const char *functionName);
protected:
  bool            logged;
  QSqlDatabase    m_sqldb;
  QString         m_spsoldiDbName;
  uint            userId;

  bool            updater(const char *updateStr);
  virtual bool    update0();
  bool update100(std::shared_ptr<IUpdaterDB100> updater);
  bool update102(std::shared_ptr<IUpdaterDB102> updater);
  virtual std::shared_ptr<IUpdaterDB100> getUpdater100() = 0;
  virtual std::shared_ptr<IUpdaterDB102> getUpdater102() = 0;

  virtual QString queryChangePwd(const QString &user, const QString &currentPwd, const QString &newPwd) const = 0;
public:
  typedef enum { UT_ADMIN = 0, UT_REGULAR = 1 } UserType;

  virtual QString getConnectionString()                           = 0;
  virtual QString getConnectionStringWithLogin(UserType userType) = 0;
  virtual QString getUsersLoginQuery(const QString &username, const QString &password) = 0;

  bool connect();
  void disconnect();
  bool login(QString username, QString password);
  bool changePwd(QString user, QString currentPwd, QString newPwd);
  /** You cannot call this if you are logged in. */
  bool update(int &tableVersion);

  virtual QString createId(const engine::SIRecord& record, uint id) const;

  // static members.
  static TDatabase *getDatabase(QString driverType, QString connectionName);
  static void      getGroupUidPsw(UserType userType, QString &uid, QString &pwd);

  // Constructors.
  TDatabase(const QString& type, const QString& connectionName);
  virtual ~TDatabase() {}
};




} } // namespace ::engine::db::

#endif // TDATABASE_H
