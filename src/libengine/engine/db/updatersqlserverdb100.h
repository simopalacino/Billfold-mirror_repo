#ifndef UPDATERSQLSERVERDB100_H
#define UPDATERSQLSERVERDB100_H

#include "iupdaterdb100.h"
#include <QSqlDatabase>

class UpdaterSQLServerDB100 : public IUpdaterDB100
{
  QSqlDatabase m_sqldb;
  bool exec(const QString &query);
  // IUpdaterDB100 interface
public:
  virtual bool alterEliminateDescriptionTo1024() override;
  virtual bool dropPlanning() override;
  virtual bool alterAccountsNameNotNull() override;
  virtual bool alterEntrateDescriptionTo1024() override;
  virtual bool alterEntrateContoNotNull() override;
  virtual bool alterEntrateDropNotaCredFK() override;
  virtual bool createEntrateUIndexIdUserId() override;
  virtual bool createEntrateIndexDate() override;
  virtual bool alterUsciteDescriptionTo1024() override;
  virtual bool alterUsciteDropExTag() override;
  virtual bool alterUsciteContoNotNull() override;
  virtual bool alterUsciteCategoryInt() override;
  virtual bool alterEntrateDropNc_suciteFK() override;
  virtual bool createUsciteUIndexIdUserId() override;
  virtual bool updateTriggerUpdateLastUpdateUsciteOnUpdate() override;
  virtual bool convertTagToCategoriesDeleteTagTables() override;

  UpdaterSQLServerDB100(QSqlDatabase &sqldb) : m_sqldb(sqldb) {  }
};

#endif // UPDATERSQLSERVERDB100_H
