#include "tdatabase.h"
#include "Database/records.h"
#include <QDebug>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlError>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include <QSettings>
#include <QDate>
#include <engine/sqlqueryrecorditerator.h>
#include <engine/sqlrecordrecord.h>

#include "queries.h"
#include "updatersqlserverdb100.h"
#include "updatersqlserverdb102.h"
#include "users.h"
#include "../logger.h"
#include <utils.h>
#include <Database/TNumeric>
#include <engine/db/accounts.h>
#include <json_record.h>
#include <json_categories.h>

#define ADMIN_USER_ID            1

#define END_NAMESPACE_ENGINE_DB

namespace engine { namespace db {

using namespace Database;


#define DBCRED_ADMIN    0
#define DBCRED_REGULAR  1
static const char *dbCredentials[][2] = {
    { "sp",      "dare96/Cry"       },  // ADMIN
    { "regular", "8P7QxnO4nEPMKHpW" }   // REGULAR
};

#define LIBCONFIG_DATABASE_NAME "database_name"

////////////////////////////////////////////////////////////////////////////////
/// engine::db::TODBCDatabase
////////////////////////////////////////////////////////////////////////////////

class TODBCDatabase : public db::TDatabase
{
  QString driverName, server;
protected:
  virtual std::shared_ptr<IUpdaterDB100> getUpdater100() override;
  virtual std::shared_ptr<IUpdaterDB102> getUpdater102() override;
public:
  virtual QString getConnectionString()                                                  override;
  virtual QString getConnectionStringWithLogin(UserType userType)                        override;
  virtual QString getUsersLoginQuery(const QString &username, const QString &password)   override;
  virtual bool    update0() override { return updater(update0_query); }
  virtual QString queryAccounts()                                                                             const;
  virtual QString queryCategories()                                                                           const;
  virtual QString queryInsert(const engine::SIRecord& record)                                                 const;
  virtual QString queryGetLastInsertedExpense()                                                               const;
  virtual QString queryGetLastInsertedIncome()                                                                const;
  virtual QString queryInsertAccount(const std::map<QString, QVariant>& record)                               const;
  virtual QString queryGetLastInsertedAccount()                                                               const;
  virtual QString queryInsertCategory(const QString& primaryName, const QString& groupName)                   const;
  virtual QString queryGetLastInsertedCategory()                                                              const;
  virtual QString queryMovements(const QString &dateFrom, const QString &dateTo, const QString &accountsList) const;
  virtual QString queryRecord(const QString &id)                                                              const;
  virtual QString queryUpdate(const SIRecord& record)                                                         const;
  virtual QString queryChangePwd(const QString &user, const QString &currentPwd, const QString &newPwd)       const override;

  // TDatabase interface
public:
  QString createId(const engine::SIRecord& record, uint id) const override;

// private table functions.
private:
  engine::SIRecord tableGetRecord(const char*  tableName, const QString& key) const;
  bool tableDeleteRecord(const char* tableName, const std::pair<QString, uint>& key);
  SIRecordIterator tableSelectAll(const char*  tableName) const;
  bool updateRecordRegId(QSqlQuery &q, const QString& newId, uint newRegId);

    // IBillfoldDBManager interface
public:
  typedef enum  _type_enum { In = 0, Out = 1 } TType;
  virtual SIRecord         selectRecord(const QString& idRecord)                                                        const override;
  virtual bool             insertNewRecord(const engine::SIRecord& record, uint* id = nullptr)                                override;
  virtual bool             updateRecord(const QString& oldId, const SIRecord& record, uint* id = nullptr)                     override;
  virtual bool             deleteRecord(const QString &idRecord)                                                              override;
  virtual SIRecordIterator selectMovements(const QString& dateFrom, const QString& dateTo, const QString& accountsList) const override;

  virtual bool             insertNewAccount(const std::map<QString, QVariant>& record, uint* id = nullptr) override;
  virtual bool             accountEditRecord(engine::SIRecord &oldRec, engine::SIRecord &newRec)       override;
  virtual bool             deleteAccount(uint32_t id)                                                  override;
  virtual SIRecordIterator selectAccounts()                                                      const override;

  virtual bool insertNewCategory(const QString& primaryName, const QString& groupName, uint* id) override;
  virtual void             categoryEditRecord(engine::SIRecord &oldRec, engine::SIRecord &newRec)       override;
  virtual bool             deleteCategory(uint32_t id)                                                  override;
  virtual SIRecordIterator selectCategories()                                                     const override;

  virtual SIRecordIterator customQuery(QString query) override;

  TODBCDatabase(const QString& connectionName, QString driverName, QString server);
};

// ////////////////////////////////////////////////////////////////////////////////////////////
// TODBCDatabase //////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<IUpdaterDB100> TODBCDatabase::getUpdater100()
{
  return std::make_shared<UpdaterSQLServerDB100>(m_sqldb);
}

std::shared_ptr<IUpdaterDB102> TODBCDatabase::getUpdater102()
{
  return std::make_shared<UpdaterSQLServerDB102>(m_sqldb);
}

QString TODBCDatabase::getConnectionString()
{
  return QString("Driver={%1};Server=%2;Database=%3;")
          .arg(driverName, server, m_spsoldiDbName);
}

QString TODBCDatabase::getConnectionStringWithLogin(TDatabase::UserType userType)
{
  QString uid, pwd;
  getGroupUidPsw(userType, uid, pwd);
  return getConnectionString() + "Uid=" + uid + ";Pwd=" + pwd + ";";
}

QString TODBCDatabase::getUsersLoginQuery(const QString &username, const QString &password)
{
  return QString("SELECT * FROM users WHERE username = '%1' AND password = '%2'")
      .arg(username, password);
}

QString TODBCDatabase::queryAccounts() const
{
  return QString("SELECT * FROM dbo.accounts WHERE user_id = %1 ORDER BY sorting;").arg(userId);
}

QString TODBCDatabase::queryCategories() const
{
  return QString("select c.name as first, e.name as second\n"
                 "from categories c\n"
                 "    join categories e on c.parent_id = e.id\n"
                   "where c.user_id = %1\n"
                   "union all\n"
                   "select c.name, c.name from categories c\n"
                   "where c.parent_id is null and c.name <> 'Transfer' and c.user_id = %1\n"
                   "order by second asc, first asc").arg(userId);
}

QString TODBCDatabase::queryInsert(const engine::SIRecord& record) const
{
  QString queryText;
  Database::RecordType type = (Database::RecordType)record[REC_TYPE].toUInt();
  QString tag  = QString("dbo.get_category_id_by_name('%1', %2)").arg(record[REC_TAG].toString()).arg(userId);

  const bool hasRegId = record[REC_REGISTRATION].isValid();

  auto querify = [&](bool usciteRec)
  {
    QString acc_date = record[REC_ACC_DATE].toString();
    QString causal   = record[REC_CAUSAL  ].toString();
    QString registration = hasRegId ? record[REC_REGISTRATION].toString() : "dbo.last_id_registration()";
    acc_date = acc_date.isEmpty() ? "NULL" : "'" + acc_date + "'";
    causal   = causal.isEmpty()   ? "NULL" : "'" + causal   + "'";
    // date, cifra, description, conto, acc_date, nota_cred, time, registration, last_update, user_id, category
    return QString("INSERT INTO %13 (date, cifra, description, conto, acc_date, nota_cred, time, registration, last_update, user_id, category, causal)\n"
                        "VALUES ('%1', %2, '%3', %4, %5, %6, '%7', %8, %9, %10, %11, %12)\n")
                    .arg(record[REC_DATE       ].toString(),                                     /* %1  date         */
                         record[REC_AMOUNT     ].toString(),                                     /* %2  cifra        */
                         record[REC_DESCRIPTION].toString().replace("'", "''"))                  /* %3  description  */
                    .arg(record[usciteRec ? REC_FROM_ACCOUNT_ID : REC_TO_ACCOUNT_ID].toInt())    /* %4  conto        */
                    .arg(acc_date,                                                               /* %5  acc_date     */
                         "NULL",                                                                 /* %6  nota_cred    */
                         record[REC_TIME].toString(),                                            /* %7  time         */
                         registration,                                                           /* %8  registration */
                         "default")                                                              /* %9  last_update  */
                    .arg(userId)                                                                 /* %10 user_id      */
                    .arg(tag)                                                                    /* %11 category     */
                    .arg(causal,                                                                 /* %12 causal       */
                         (usciteRec ? "uscite" : "entrate"));                                    /* %13              */
  };
  QString notes = record[REC_NOTE ].toString();
  QString place = record[REC_PLACE].toString();
  notes = notes.isEmpty() ? "NULL" : "'" + notes.replace("'", "''") + "'";
  place = place.isEmpty() ? "NULL" : "'" + place.replace("'", "''") + "'";

  queryText += QString("BEGIN\n");
  if (!hasRegId)
  {
    queryText += QString("insert into dbo.registrations (currency, note, place, deleted, user_id, last_update)\n"
                         "values(%1, %2, %3, %4, %5, %6)\n")
                         .arg("default",
                              notes,
                              place)
                         .arg(0)
                         .arg(userId)
                         .arg("default");
  }
  if (type == Database::Outgoing || type == Database::Transfer)
    queryText += querify(true);
  if (type == Database::Income || type == Database::Transfer)
    queryText += querify(false);
  queryText += "END\n";
  return queryText;
}

QString TODBCDatabase::queryGetLastInsertedExpense() const
{
  return QString("SELECT TOP 1 " REC_ID " FROM dbo.uscite WHERE user_id = %1 ORDER BY " REC_ID " DESC").arg(userId);
}

QString TODBCDatabase::queryGetLastInsertedIncome() const
{
  return QString("SELECT TOP 1 " REC_ID " FROM dbo.entrate WHERE user_id = %1 ORDER BY " REC_ID " DESC").arg(userId);
}

#define GET_STRING_OR_EMPTY(field, string_funct) \
    ({ \
      QString s; \
      auto it = record.find(field); \
      if (it != record.end()) \
        s = it->second.string_funct; \
      s; \
    })
#define GET_BOOL_OR_DEFAULT(field_name) \
    ({ \
      QString s; \
      auto it = record.find(field_name); \
      s = it != record.end() ? (it->second.toBool() ? "1" : "0") : "DEFAULT"; \
    })
QString TODBCDatabase::queryInsertAccount(const std::map<QString, QVariant>& record) const
{
  QString owner = GET_STRING_OR_EMPTY(ACCOUNTS_OWNER,     toString());
  QString bank  = GET_STRING_OR_EMPTY(ACCOUNTS_BANCA,     toString());
  QString color = GET_STRING_OR_EMPTY(ACCOUNTS_COLOR,     toString());
  QString dInit = GET_STRING_OR_EMPTY(ACCOUNTS_DATE_INIT, toDate().toString(ANSI_DATE_FORMAT));

  owner = owner.isEmpty() ? "NULL"    : "'" + owner + "'";
  bank  = bank.isEmpty()  ? "NULL"    : "'" + bank  + "'";
  color = color.isEmpty() ? "DEFAULT" : color;
  dInit = dInit.isEmpty() ? "DEFAULT" : "'" + dInit + "'";

  const QString contanti  = GET_BOOL_OR_DEFAULT(ACCOUNTS_CONTANTI);
  const QString risparmio = GET_BOOL_OR_DEFAULT(ACCOUNTS_RISPARMIO);
  const QString visible   = GET_BOOL_OR_DEFAULT(ACCOUNTS_VISIBLE);
  const QString virt      = GET_BOOL_OR_DEFAULT(ACCOUNTS_VIRTUAL);

  return QString("declare @id int\n"
                 "select top 1 @id = conto + 1 from accounts order by conto desc\n"
                 "INSERT INTO dbo.accounts (conto, name, owner, banca, contanti, risparmio, init_value, date_init, value, color, sorting, visible, virtual, user_id)\n"
                 "VALUES (@id, %1, %2, %3, %4, %5, %6, %7, %8, %9, @id, %10, %11, %12)\n"
                 "SELECT @id as conto")
          /* %1  name       */ .arg("'" + GET_STRING_OR_EMPTY(ACCOUNTS_NAME, toString()) + "'")
          /* %2  owner      */ .arg(owner)
          /* %3  banca      */ .arg(bank)
          /* %4  contanti   */ .arg(contanti)
          /* %5  risparmio  */ .arg(risparmio)
          /* %6  init_value */ .arg(GET_STRING_OR_EMPTY(ACCOUNTS_INIT_VALUE, toString()))
          /* %7  date_init  */ .arg(dInit)
          /* %8  value      */ .arg(GET_STRING_OR_EMPTY(ACCOUNTS_INIT_VALUE, toString()),   // Start with the same value.
          /* %9  color      */      color)
          /* %10 visible    */ .arg(visible,
          /* %11 virtual    */      virt)
          /* %12 user_id    */ .arg(userId);
}
#undef GET_STRING_OR_EMPTY
#undef GET_BOOL_OR_DEFAULT

QString TODBCDatabase::queryGetLastInsertedAccount() const
{
  return QString("SELECT TOP 1 " ACCOUNTS_ID " FROM accounts WHERE " ACCOUNTS_USER_ID " = %1 ORDER BY " ACCOUNTS_ID " DESC").arg(userId);
}

QString TODBCDatabase::queryInsertCategory(const QString& primaryName, const QString& groupName) const
{
  QString query;

  if (!groupName.isEmpty())
  {
    query = QString("DECLARE @p_id INT\n"
                   "SELECT TOP 1 @p_id = id FROM categories WHERE name = '%1'\n"
                   "INSERT INTO categories (name, user_id, parent_id) VALUES ('%2', %3, @p_id)")
              .arg(groupName, primaryName)
              .arg(userId);
  }
  else
  {
    query = QString("INSERT INTO categories (name, user_id, parent_id) VALUES ('%1', %2, NULL)")
              .arg(primaryName)
              .arg(userId);
  }
  return query;
}

QString TODBCDatabase::queryGetLastInsertedCategory() const
{
  return QString("SELECT TOP 1 " CAT_ID " FROM categories WHERE " CAT_USER_ID " = %1 ORDER BY " CAT_ID " DESC").arg(userId);
}

QString TODBCDatabase::queryMovements(const QString &dateFrom, const QString &dateTo, const QString &accountsList) const
{
  return QString("SELECT a.*, rh.amount as regAmount, rh.currency as currency, rh.note as note, rh.place as place FROM (\n"
                 "    SELECT 0 as type, u.id, date, cifra AS cifra, c.name AS tag, description, conto, acc_date, CAST(time AS datetime) as [time], registration, causal FROM uscite u JOIN categories c on u.category = c.id\n"
                 "        WHERE conto IN (%1) AND u.user_id = %2 AND u.date >= '%3' AND u.date <= '%4'\n"
                 "    UNION ALL\n"
                 "    SELECT 1 as type, u.id, date, cifra AS cifra, c.name AS tag, description, conto, acc_date, CAST(time AS datetime) as [time], registration, causal FROM entrate u JOIN categories c on u.category = c.id\n"
                 "    WHERE conto IN (%1) AND u.user_id = %2 AND u.date >= '%3' AND u.date <= '%4') a\n"
                 "LEFT JOIN dbo.registrations_header() rh ON rh.id = a.registration\n"
                 "ORDER BY date DESC, time DESC, id DESC;")
          .arg(accountsList)
          .arg(userId)
          .arg(dateFrom, dateTo);
}

QString TODBCDatabase::queryRecord(const QString &id) const
{
  bool entrate = id[0] == 'e';
  const uint i = id.midRef(1).toUInt();
  return QString("SELECT '%6' as [type], a.*, z.conto as %1_account_id, rh.amount as regAmount, rh.currency as currency, rh.note as note, rh.place as place FROM (\n"
                 "    SELECT u.id, date, cifra, c.name AS tag, description, conto AS %2_account_id, acc_date, nota_cred, CAST(time AS datetime) as [time], registration, causal FROM %3 u JOIN categories c on u.category = c.id\n"
                 "        WHERE u.user_id = %4 AND u.id = %5 ) a\n"
                 "LEFT JOIN dbo.registrations_header() rh ON rh.id = a.registration\n"
                 "LEFT JOIN entrate z on z.registration = a.registration AND tag = 'Transfer'\n"
                 "ORDER BY date DESC, time DESC, id DESC;")
          .arg(entrate ? "from" : "to",        // %1
               entrate ? "to" : "from",        // %2
               entrate ? "entrate" : "uscite") // %3
          .arg(userId)                         // %4
          .arg(i)                              // %5
          .arg(entrate ? Database::Income : Database::Outgoing);  // %6
}

// TODO: Consider when registration id is modify!!
QString TODBCDatabase::queryUpdate(const SIRecord& record) const // TODO: TEST
{
  QString queryText;
  Database::RecordType type = (Database::RecordType)record[REC_TYPE].toUInt();
  QString tag  = QString("dbo.get_category_id_by_name('%1', %2)").arg(record[REC_TAG].toString()).arg(userId);

  auto querify = [&](bool usciteRec)
  {
    QString acc_date = record[REC_ACC_DATE].toString();
    QString causal   = record[REC_CAUSAL].toString();
    acc_date = acc_date.isEmpty() ? "NULL" : "'" + acc_date + "'";
    causal   = causal.isEmpty()   ? "NULL" : "'" + causal   + "'";
    // date, cifra, description, conto, acc_date, nota_cred, time, registration, last_update, user_id, category
    return QString("UPDATE %13 SET date = '%1', cifra = %2, description = '%3', conto = %4, acc_date = %5, nota_cred = %6, time = '%7', registration = %8, user_id = %9, category = %10, causal = %11\n"
                   "  WHERE id = %12\n")
                    .arg(record[REC_DATE       ].toString())                                     /* %1  date         */
                    .arg(record[REC_AMOUNT     ].toString())                                     /* %2  cifra        */
                    .arg(record[REC_DESCRIPTION].toString().replace("'", "''"))                  /* %3  description  */
                    .arg(record[usciteRec ? REC_FROM_ACCOUNT_ID : REC_TO_ACCOUNT_ID].toInt())    /* %4  conto        */
                    .arg(acc_date)                                                               /* %5  acc_date     */
                    .arg("NULL")                                                                 /* %6  nota_cred    */
                    .arg(record[REC_TIME].toString())                                            /* %7  time         */
                    .arg(record[REC_REGISTRATION].toUInt())                                      /* %8  registration */
                    .arg(userId)                                                                 /* %9  user_id      */
                    .arg(tag)                                                                    /* %10 category     */
                    .arg(causal)                                                                 /* %11 causal       */
                    .arg(record[REC_ID].toUInt())                                                /* %12 id           */
                    .arg(usciteRec ? "uscite" : "entrate");                                      /* %13 { table }    */
  };

  QString notes = record[REC_NOTE ].toString();
  QString place = record[REC_PLACE].toString();
  notes = notes.isEmpty() ? "NULL" : "'" + notes.replace("'", "''") + "'";
  place = place.isEmpty() ? "NULL" : "'" + place.replace("'", "''") + "'";

  queryText += QString("BEGIN\n"
                       "UPDATE dbo.registrations SET currency = %1, note = %2, place = %3, deleted = %4, user_id = %5\n"
                       "  WHERE id = %6\n")
                       .arg("default")
                       .arg(notes)
                       .arg(place)
                       .arg("0")
                       .arg(userId)
                       .arg(record[REC_REGISTRATION].toUInt());
  if (type == Database::Outgoing || type == Database::Transfer)
    queryText += querify(true);
  if (type == Database::Income || type == Database::Transfer)
    queryText += querify(false);
  queryText += "END\n";
  return queryText;
}

QString TODBCDatabase::queryChangePwd(const QString &user, const QString &currentPwd, const QString &newPwd) const
{
  QString query("UPDATE dbo.users SET password = '%1' WHERE username = '%2' AND password = '%3';");
  query = query.arg(newPwd, user, currentPwd);
  return query;
}

QString TODBCDatabase::createId(const engine::SIRecord& record, uint id) const
{
  return (record[REC_TYPE].toUInt() == Database::Outgoing ? "u" : "e") + QString::number(id);
}

SIRecord TODBCDatabase::tableGetRecord(const char* tableName, const QString &key) const
{
  QSqlQuery q(m_sqldb);
  QString query = QString("SELECT * FROM [%1] WHERE id = %2 AND user_id = %3").arg(tableName, key).arg(userId);
  if (!(q).exec((query)))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  q.next();
  auto r = new SqlRecordRecord(q.record());
  SIRecord rec = r->getInstance();
  return rec;
}

bool TODBCDatabase::tableDeleteRecord(const char* tableName, const std::pair<QString, uint>& key)
{
  QSqlQuery q(m_sqldb);
  QString query = QString("DELETE FROM [%1] WHERE %2 = %3 AND user_id = %4").arg(tableName, key.first).arg(key.second).arg(userId);
  if (!(q).exec((query)))
  {
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
    return false;
  }
  return m_sqldb.commit();
}

SIRecordIterator TODBCDatabase::tableSelectAll(const char* tableName) const
{
  SIRecordIterator q = SIRecordIterator(new SqlQueryRecordIterator(m_sqldb));
  QString query = QString("SELECT * FROM [%1] WHERE user_id = %3").arg(tableName).arg(userId);
  if (!q.exec((query)))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  return q;
}

bool TODBCDatabase::updateRecordRegId(QSqlQuery &q, const QString &newId, uint newRegId)
{
  const Database::RecordType type = newId[0] == "e" ? Income : Outgoing;
  const uint recId = newId.midRef(1).toUInt();
  const QString query = QString("UPDATE %1 SET " REC_REGISTRATION " = %2 WHERE id = %3 AND user_id = %4")
      .arg(type == Outgoing ? "uscite" : "entrate")
      .arg(newRegId)
      .arg(recId)
      .arg(userId);
  if (!q.exec(query))
  {
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data() << q.lastError().text();
    return false;
  }
  return m_sqldb.commit();
}




// IBillfoldInterface Methods Implementations /////////////////////////////////////////////////

SIRecord TODBCDatabase::selectRecord(const QString &idRecord) const
{
  SIRecordIterator q = (new SqlQueryRecordIterator(m_sqldb))->getInstance();
  QString query = queryRecord(idRecord);
  if (!q.exec(query))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  q.next();
  return q.get();
}

bool TODBCDatabase::insertNewRecord(const engine::SIRecord& record, uint *id)
{
  bool ok = false;
  if (logged)
  {
    QSqlQuery sql(m_sqldb);
    QString   query = queryInsert(record);
    logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    if (sql.exec(query))
    {
      ok = true;
      if (id)
      {
        if (record[REC_TYPE].toUInt() == Database::Outgoing)
          query = queryGetLastInsertedExpense();
        else
          query = queryGetLastInsertedIncome();
        logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
        if (sql.exec(query))
        {
          sql.first();
          *id = sql.value(REC_ID).toUInt();
        }
      }
    }
  }
  return ok;
}

bool TODBCDatabase::updateRecord(const QString& oldId, const SIRecord& record, uint* id)
{
  bool ok = false;
  SIRecord old = selectRecord(oldId);
  SIRecord rec = record.getCopy();
  const uint oldRegId = old[REC_REGISTRATION].toUInt();
  const uint newRegId = rec.value(REC_REGISTRATION).toUInt();
  rec.setValue(REC_REGISTRATION, oldRegId);
  // If same type no problems...
  // If types are different, delete from one table and insert in the other.
  // Mantain the same registration number!
  // Return the new id in id param.
  if (old[REC_TYPE].toUInt() == record[REC_TYPE].toUInt())
  {
    const QString query = queryUpdate(rec);
    logger("[%s:%d]\n%s", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    QSqlQuery sql(m_sqldb);
    if (sql.exec(query))
      ok = true;
    uint recId;
    if (ok)
    {
      recId = record[REC_ID].toUInt();
      if (oldRegId != newRegId)
        updateRecordRegId(sql, createId(rec, recId), newRegId);
      if (id)
        *id = recId;
    }
  }
  else
  {
    uint newId;
    ok = insertNewRecord(rec, &newId) && deleteRecord(oldId);
    if (ok)
    {
      rec.setValue(REC_ID, newId);
      if (id)
        *id = newId;
      const QString recId = createId(rec, newId);
      ok = updateRecord(recId, rec);  // To apply the edits on registration's fields;
      if (oldRegId != newRegId)
      {
        QSqlQuery sql(m_sqldb);
        ok = ok && updateRecordRegId(sql, recId, newRegId);
      }
    }
  }
  return ok;
}

bool TODBCDatabase::deleteRecord(const QString &idRecord)
{
  QString num = idRecord;
  num = num.mid(1);
  if (idRecord[0] == "e")
    return tableDeleteRecord("entrate", { "id", num.toUInt() });
  else
    return tableDeleteRecord("uscite", { "id", num.toUInt() });
}

SIRecordIterator TODBCDatabase::selectMovements(const QString &dateFrom, const QString &dateTo, const QString &accountsList) const
{
  SIRecordIterator q = SIRecordIterator(new SqlQueryRecordIterator(m_sqldb));
  QString query = queryMovements(dateFrom, dateTo, accountsList);
  if (!q.exec(query))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  return q;
}

bool TODBCDatabase::insertNewAccount(const std::map<QString, QVariant>& record, uint* id)
{
  bool ok = false;
  if (logged)
  {
    QSqlQuery sql(m_sqldb);
    QString   query = queryInsertAccount(record);
    logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    if (sql.exec(query))
    {
      ok = true;
      if (id)
      {
        query = queryGetLastInsertedAccount();
        logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
        if (sql.exec(query))
        {
          sql.first();
          *id = sql.value(ACCOUNTS_ID).toUInt();
        }
      }
    }
  }
  return ok;
}

bool TODBCDatabase::accountEditRecord(SIRecord &oldRec, SIRecord &newRec)
{
  UNUSED(oldRec)

  bool ok = false;
  if (logged)
  {
    QSqlQuery sql(m_sqldb);
    QString owner = newRec.value(ACCOUNTS_OWNER).toString();
    QString bank  = newRec.value(ACCOUNTS_BANCA).toString();
    QString color = newRec.value(ACCOUNTS_COLOR).toString();
    owner = owner.isEmpty() ? "NULL"    : "'" + owner + "'";
    bank  = bank.isEmpty()  ? "NULL"    : "'" + bank  + "'";
    color = color.isEmpty() ? "DEFAULT" : color;
    QString query = QString("UPDATE [dbo].[accounts] SET name = %1, owner = %2, banca = %3, contanti = %4, risparmio = %5, init_value = %6, date_init = %7, value = %8, color = %9, sorting = %10, visible = %11, virtual = %12\n"
                            "WHERE user_id = %13 AND conto = %14")
            /* %1  name       */ .arg("'" + newRec.value(ACCOUNTS_NAME).toString() + "'",
            /* %2  owner      */      owner,
            /* %3  banca      */      bank,
            /* %4  contanti   */      newRec.value(ACCOUNTS_CONTANTI  ).toBool() ? "1" : "0",
            /* %5  risparmio  */      newRec.value(ACCOUNTS_RISPARMIO ).toBool() ? "1" : "0",
            /* %6  init_value */      newRec.value(ACCOUNTS_INIT_VALUE).toString(),
            /* %7  date_init  */      "'" + newRec.value(ACCOUNTS_DATE_INIT).toDate().toString(ANSI_DATE_FORMAT) + "'",
            /* %8  value      */      newRec.value(ACCOUNTS_VALUE).toString(),
            /* %9  color      */      color)
            /* %10 sorting    */ .arg(newRec.value(ACCOUNTS_SORTING).toUInt())
            /* %11 visible    */ .arg(newRec.value(ACCOUNTS_VISIBLE).toBool() ? "1" : "0",
            /* %12 virtual    */      newRec.value(ACCOUNTS_VIRTUAL).toBool() ? "1" : "0")
            /* %13 user_id    */ .arg(userId)
            /* %14 id{conto}  */ .arg(newRec.value(ACCOUNTS_ID).toUInt());
    logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    if (sql.exec(query))
      ok = true;
  }
  return ok;
}

bool TODBCDatabase::deleteAccount(uint32_t id)
{
  return tableDeleteRecord("accounts", { "conto", id });
}

SIRecordIterator TODBCDatabase::selectAccounts() const
{
  SIRecordIterator q = SIRecordIterator(new SqlQueryRecordIterator(m_sqldb));
  QString query = queryAccounts();
  if (!q.exec(query))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  return q;
}

bool TODBCDatabase::insertNewCategory(const QString &primaryName, const QString &groupName, uint *id)
{
  bool ok = false;
  if (logged)
  {
    QSqlQuery sql(m_sqldb);
    QString   query = queryInsertCategory(primaryName, groupName);
    logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    if (sql.exec(query))
    {
      ok = true;
      if (id)
      {
        query = queryGetLastInsertedCategory();
        logger("[%s:%d]\n%s\n", __FUNCTION__, __LINE__, QSTR2CSTR(query));
        if (sql.exec(query))
        {
          sql.first();
          *id = sql.value(CAT_ID).toUInt();
        }
      }
    }
  }
  return ok;
}

void TODBCDatabase::categoryEditRecord(SIRecord &oldRec, SIRecord &newRec)
{
  UNUSED(oldRec)
  UNUSED(newRec)
  logger("%s not implemented.", __PRETTY_FUNCTION__);
}

bool TODBCDatabase::deleteCategory(uint32_t id)
{
  return tableDeleteRecord("categories", { "id", id });
}

SIRecordIterator TODBCDatabase::selectCategories() const
{
  SIRecordIterator q = SIRecordIterator(new SqlQueryRecordIterator(m_sqldb));
  QString query = queryCategories();
  if (!q.exec(query))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  return q;
}

SIRecordIterator TODBCDatabase::customQuery(QString query)
{
  SIRecordIterator q = SIRecordIterator(new SqlQueryRecordIterator(m_sqldb));
  if (!q.exec(query))
    qDebug() << QString("exec err %1").arg(query).toLocal8Bit().data();
  return q;
}

TODBCDatabase::TODBCDatabase(const QString &connectionName, QString driverName, QString server)
  : TDatabase("QODBC", connectionName),
    driverName(driverName),
    server(server)
{
  QSettings sets("libconfig.ini", QSettings::IniFormat);
  sets.beginGroup("Main");
  m_spsoldiDbName = sets.value(LIBCONFIG_DATABASE_NAME, "sp_soldi").toString();
}


////////////////////////////////////////////////////////////////////////////////
/// engine::db::TSQLiteDatabase
////////////////////////////////////////////////////////////////////////////////

class TSQLiteDatabase : public db::TDatabase
{
  QFileInfo m_path;
public:
  TSQLiteDatabase(const QString& connectionName, const QString &path = "C:/Users/Simone/newsqlite.db")
    : TDatabase("QSQLITE", connectionName),
          m_path(path)
    {  }
};




////////////////////////////////////////////////////////////////////////////////
/// engine::db::TDatabase
////////////////////////////////////////////////////////////////////////////////

QString TDatabase::notImplemented(const char *functionName) const
{
  logger("%s not implemented.", functionName);
  return "__ERROR__";
}

bool TDatabase::updater(const char *updateStr)
{
  QString     tok(updateStr);
  QStringList list = tok.split(';');
  QSqlQuery sql(m_sqldb);
  bool ok = true;
  foreach (const QString &string, list)
  {
    ok &= sql.exec(string);
    if (!ok)
    {
      logger("[%s:%d] [sql.lastError]: %s\nQuery={\n%s\n}",
             __FUNCTION__, __LINE__,
             sql.lastError().databaseText().toLocal8Bit().data(),
             string.toLocal8Bit().data());
      break;
    }
  }
  return ok;
}

bool TDatabase::update0()
{
  notImplemented(__FUNCTION__);
  return false;
}

bool TDatabase::update100(std::shared_ptr<IUpdaterDB100> updater)
{
  updater->alterEliminateDescriptionTo1024();
  updater->dropPlanning();
  updater->alterAccountsNameNotNull();
  updater->alterEntrateDescriptionTo1024();
  updater->alterEntrateContoNotNull();
  updater->alterEntrateDropNotaCredFK();
  updater->createEntrateUIndexIdUserId();
  updater->createEntrateIndexDate();
  updater->alterUsciteDescriptionTo1024();
  updater->alterUsciteDropExTag();
  updater->alterUsciteContoNotNull();
  updater->alterUsciteCategoryInt();
  updater->alterEntrateDropNc_suciteFK();
  updater->createUsciteUIndexIdUserId();
  updater->updateTriggerUpdateLastUpdateUsciteOnUpdate();
//    ok &= updater->convertTagToCategoriesDeleteTagTables();
  return true;
}

bool TDatabase::update102(std::shared_ptr<IUpdaterDB102> updater)
{
  return updater->alterTriggerOnUpdateEntrate();
}

bool TDatabase::connect()
{
  QString connStr = getConnectionStringWithLogin(UT_ADMIN);
  m_sqldb.setDatabaseName(connStr);
  if (!m_sqldb.open())
  {
    logger("[%s:%d] %s\n%s\nconnStr:%s", __FUNCTION__, __LINE__, QSTR2CSTR(m_sqldb.lastError().driverText()),
           QSTR2CSTR(m_sqldb.lastError().databaseText()), QSTR2CSTR(connStr));
    return false;
  }
  QSqlQuery sql(m_sqldb);
  if (!sql.exec("USE " + m_spsoldiDbName + ";"))
  {
    logger("[%s:%d] %s", __FUNCTION__, __LINE__, QSTR2CSTR(sql.lastError().databaseText()));
    return false;
  }
  return true;
}

void TDatabase::disconnect()
{
  m_sqldb.close();
}

bool TDatabase::login(QString username, QString password)
{
  QSqlQuery sql(m_sqldb);
  if (!sql.exec(getUsersLoginQuery(username, password)))
  {
    logger("[%s:%d] ERROR: %s\n%s", __FUNCTION__, __LINE__, (const char*)m_sqldb.lastError().driverText().toUtf8(),
                            (const char*)sql.lastError().databaseText().toUtf8());
    return false;
  }
  if (!sql.first())
  {
    logger("[%s:%d] Login Error! {%s, %s}", __FUNCTION__, __LINE__, username.toLocal8Bit().data(), password.toLocal8Bit().data());
    return false;
  }
  userId = sql.value(USERS_ID).toInt();
  UserType userType = (UserType)sql.value(USERS_TYPE).toInt();
  QString connStr = getConnectionStringWithLogin(userType);
  logger("[%s:%d] getConnectionStringWithLogin(%d): %s", __FUNCTION__, __LINE__, userId, QSTR2CSTR(connStr));
  m_sqldb.setDatabaseName(connStr);
  if (!m_sqldb.open())
  {
    logger("[%s:%d] ERROR: %s\n%s", __FUNCTION__, __LINE__, (const char*)m_sqldb.lastError().driverText().toUtf8(),
                            sql.lastError().databaseText().toLocal8Bit().data());
    return false;
  }
  QString useCmd = "USE " + m_spsoldiDbName + ";";
  if (!sql.exec(useCmd))
  {
    logger("[%s:%d] \"%s\" Failed: %s", __FUNCTION__, __LINE__, useCmd.toLocal8Bit().data(), sql.lastError().databaseText().toLocal8Bit().data());
    return false;
  }
  logged = true;
  return true;
}

void sanitize(QString& str)
{
  str = str.remove(QChar(';'));
  str = str.remove("--");
}

bool db::TDatabase::changePwd(QString user, QString currentPwd, QString newPwd)
{
  QSqlQuery sql(m_sqldb);
  sanitize(user);
  sanitize(currentPwd);
  sanitize(newPwd);
  QString str = queryChangePwd(user, currentPwd, newPwd);
  bool res = sql.exec(str);
  if (!res)
    logger("[%s:%d] ERROR: %s\n%s", __FUNCTION__, __LINE__, (const char*)m_sqldb.lastError().driverText().toUtf8(),
                            (const char*)sql.lastError().databaseText().toUtf8());
  else
    m_sqldb.commit();
  return res;
}

bool TDatabase::update(int &tableVersion)
{
  bool ok = false;
  if (!logged)
  {
    ok = true;
    switch (tableVersion)
    {
    case 0:
      ok &= update0();
      if (ok)
        tableVersion = 100; // First version number.
      else
        break;
    case 100:
      ok &= update100(getUpdater100());
      if (ok)
        tableVersion += 2;
      else
        break;
    case 102:
      ok &= update102(getUpdater102());
      if (ok)
        tableVersion += 2;
      else
        break;
    default: break;
    }
  }
  return ok;
}

void TDatabase::getJsonResult(QJsonObject &main, const QString &query, const QString &jsonName, const char *functionName)
{
  if (logged)
  {
    QSqlQuery sql(m_sqldb);
    logger("[%s:%d] %s", __FUNCTION__, __LINE__, QSTR2CSTR(query));
    if (sql.exec(query))
    {
      QJsonArray arr;
      for (bool ok = sql.first(); ok; ok = sql.next())
      {
        QJsonObject rec;
        const int columns = sql.record().count();
        for (int i = 0; i < columns; ++i)
            rec.insert(sql.record().fieldName(i), QJsonValue::fromVariant(sql.value(i)));
        arr.append(rec);
      }
      if (!arr.empty())
        main.insert("data", arr);
      QFile file(jsonName + ".json");
      file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
      file.write(QJsonDocument(main).toJson());
    }
    else
      logger("[%s:%d] ERROR %s(): %s", __FUNCTION__, __LINE__, functionName, sql.lastError().databaseText().toLocal8Bit().data());
  }
}

QString TDatabase::createId(const engine::SIRecord& record, uint id) const
{
  (void)record;
  return QString::number(id);
}

// Static zone /////////////////////////////////////////////////

TDatabase *TDatabase::getDatabase(QString driverType, QString connectionName)
{
  if (driverType == "ODBC")
  {
    QSettings sets("libconfig.ini", QSettings::IniFormat);
    sets.beginGroup("Main");
    QString server = sets.value("server_address", "localhost\\SQLEXPRESS01").toString();
    return new TODBCDatabase(connectionName, "ODBC Driver 17 for SQL Server", server);
  }
  return nullptr;
}

void TDatabase::getGroupUidPsw(TDatabase::UserType userType, QString &uid, QString &pwd)
{
  switch (userType)
  {
  case TDatabase::UT_ADMIN:
    uid = dbCredentials[DBCRED_ADMIN][0];
    pwd = dbCredentials[DBCRED_ADMIN][1];
    break;
  case TDatabase::UT_REGULAR:
    uid = dbCredentials[DBCRED_REGULAR][0];
    pwd = dbCredentials[DBCRED_REGULAR][1];
    break;
  }
}

// Constructors zone ///////////////////////////////////////////

TDatabase::TDatabase(const QString& type, const QString& connectionName) : logged(false), userId(ADMIN_USER_ID)
{
  m_sqldb = QSqlDatabase::addDatabase(type, connectionName);
}




} } END_NAMESPACE_ENGINE_DB
