#ifndef UPDATERSQLSERVERDB102_H
#define UPDATERSQLSERVERDB102_H

#include <QSqlDatabase>
#include "iupdaterdb102.h"


class UpdaterSQLServerDB102 : public IUpdaterDB102
{
  QSqlDatabase m_sqldb;
  bool exec(const QString &query);
  // IUpdaterDB102 interface
public:
  virtual bool alterTriggerOnUpdateEntrate() override;
  UpdaterSQLServerDB102(QSqlDatabase &sqldb) : m_sqldb(sqldb) {  }
};

#endif // UPDATERSQLSERVERDB102_H
