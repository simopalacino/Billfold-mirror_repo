#ifndef IUPDATERDB100_H
#define IUPDATERDB100_H


class IUpdaterDB100
{
public:
  virtual bool alterEliminateDescriptionTo1024() = 0;
  virtual bool dropPlanning()                    = 0;
  virtual bool alterAccountsNameNotNull()        = 0;
  virtual bool alterEntrateDescriptionTo1024()   = 0;
  virtual bool alterEntrateContoNotNull()        = 0;
  virtual bool alterEntrateDropNotaCredFK()      = 0;
  virtual bool createEntrateUIndexIdUserId()     = 0;
  virtual bool createEntrateIndexDate()          = 0;

  virtual bool alterUsciteDescriptionTo1024()    = 0;
  virtual bool alterUsciteDropExTag()            = 0;
  virtual bool alterUsciteContoNotNull()         = 0;
  virtual bool alterUsciteCategoryInt()          = 0;

  virtual bool alterEntrateDropNc_suciteFK()                 = 0;
  virtual bool createUsciteUIndexIdUserId()                  = 0;
  virtual bool updateTriggerUpdateLastUpdateUsciteOnUpdate() = 0;

  virtual bool convertTagToCategoriesDeleteTagTables() = 0;
};

#endif // IUPDATERDB100_H
