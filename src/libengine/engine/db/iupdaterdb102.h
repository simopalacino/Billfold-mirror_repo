#ifndef IUPDATERDB102_H
#define IUPDATERDB102_H

class IUpdaterDB102
{
public:
  virtual bool alterTriggerOnUpdateEntrate() = 0;
};

#endif // IUPDATERDB102_H
