#include "updatersqlserverdb100.h"

#include <QSqlQuery>
#include <QSqlError>

#include "../logger.h"

bool UpdaterSQLServerDB100::exec(const QString &query)
{
  QSqlQuery sql(m_sqldb);
  if (!sql.exec(query))
  {
    engine::logger("[%s:%d] [sql.lastError]: %s\nQuery={\n%s\n}",
                       __FUNCTION__, __LINE__,
                       sql.lastError().databaseText().toLocal8Bit().data(),
                       query.toLocal8Bit().data());
    return false;
  }
  return m_sqldb.commit();
}

bool UpdaterSQLServerDB100::alterEliminateDescriptionTo1024()
{
  QString q = "alter table eliminate alter column description varchar(1024) not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::dropPlanning()
{
  QString q = "drop table planning";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterAccountsNameNotNull()
{
  QString q = "alter table accounts alter column name varchar(32) not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterEntrateDescriptionTo1024()
{
  QString q = "alter table entrate alter column description varchar(1024) not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterEntrateContoNotNull()
{
  QString q = "alter table entrate alter column conto int not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterEntrateDropNotaCredFK()
{
  QString q = "alter table entrate drop constraint entrate_nc_uscite_id_fk";
  return exec(q);
}

bool UpdaterSQLServerDB100::createEntrateUIndexIdUserId()
{
  QString q = "create unique index entrate_id_user_id_uindex on entrate (id, user_id)";
  return exec(q);
}

bool UpdaterSQLServerDB100::createEntrateIndexDate()
{
  QString q = "create index entrate_date_index on entrate (date desc)";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterUsciteDescriptionTo1024()
{
  QString q = "alter table uscite alter column description varchar(1024) not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterUsciteDropExTag()
{
  QString q = "alter table uscite drop constraint uscite_tag_out_name_fk";
  bool ok = exec(q);

  q = "alter table uscite drop constraint DF__uscite__tag__5535A963";
  ok &= exec(q);

  q = "alter table uscite drop column _ex_tag";
  ok &= exec(q);
  return ok;
}

bool UpdaterSQLServerDB100::alterUsciteContoNotNull()
{
  QString q = "alter table uscite alter column conto int not null";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterUsciteCategoryInt()
{
  QString q = "alter table uscite alter column category int";
  return exec(q);
}

bool UpdaterSQLServerDB100::alterEntrateDropNc_suciteFK()
{
  QString q = "alter table entrate drop constraint entrate_nc_sucite_id_fk";
  return exec(q);
}

bool UpdaterSQLServerDB100::createUsciteUIndexIdUserId()
{
  QString q = "create unique index uscite_id_user_id_uindex on uscite (id, user_id)";
  return exec(q);
}

bool UpdaterSQLServerDB100::updateTriggerUpdateLastUpdateUsciteOnUpdate()
{
  QString q = R"(CREATE OR ALTER TRIGGER dbo.update_last_update_uscite_on_update
ON uscite
AFTER UPDATE
AS BEGIN
    DECLARE @aid int
    DECLARE _afteru CURSOR FOR SELECT id FROM inserted
    OPEN _afteru
    FETCH NEXT FROM _afteru INTO @aid

    WHILE @@FETCH_STATUS = 0
    BEGIN
        UPDATE uscite
        SET last_update = SYSDATETIME()
        WHERE id = @aid;
        -- ++i
        FETCH NEXT FROM _afteru INTO @aid
    END
    CLOSE _afteru
    deallocate _afteru
END)";
  return exec(q);
}

bool UpdaterSQLServerDB100::convertTagToCategoriesDeleteTagTables()
{
  QString q;
  bool ok = true;

  q = R"(SELECT top 0 *
into #tmp_table
from categories

insert into #tmp_table (name, user_id, last_update, parent_id)
select name, 2, current_timestamp, 17 from tag_in)";
  ok &= exec(q);

  q = "drop table tag_in";
  ok &= exec(q);

  q = "drop table t_out_is_a";
  ok &= exec(q);

  q = "drop table tag_out";
  ok &= exec(q);
  return ok;
}
