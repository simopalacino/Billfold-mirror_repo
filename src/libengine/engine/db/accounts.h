#ifndef ACCOUNTS_H
#define ACCOUNTS_H

#define ACCOUNTS_ID             "conto"
#define ACCOUNTS_NAME           "name"
#define ACCOUNTS_OWNER          "owner"
#define ACCOUNTS_BANCA          "banca"
#define ACCOUNTS_CONTANTI       "contanti"
#define ACCOUNTS_RISPARMIO      "risparmio"
#define ACCOUNTS_INIT_VALUE     "init_value"
#define ACCOUNTS_DATE_INIT      "date_init"
#define ACCOUNTS_VALUE          "value"
#define ACCOUNTS_COLOR          "color"
#define ACCOUNTS_SORTING        "sorting"
#define ACCOUNTS_VISIBLE        "visible"
#define ACCOUNTS_VIRTUAL        "virtual"
#define ACCOUNTS_LAST_UPDATE    "last_update"
#define ACCOUNTS_USER_ID        "user_id"
#define ACCOUNTS_TYPE           "type"

#endif // ACCOUNTS_H
