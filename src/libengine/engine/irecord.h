#ifndef IRECORD_H
#define IRECORD_H

#include "shareable.h"

#include <memory>
#include <QString>
#include <QVariant>

namespace engine {


class SIRecord;
class IRecord
{
public:
  virtual int      count()                    const = 0;
  virtual QString  fieldName(int i)           const = 0;
  virtual QVariant value(int i)               const = 0;
  virtual QVariant value(const QString& name) const = 0;
  virtual void     setValue(int i, const QVariant& val)               = 0;
  virtual void     setValue(const QString& name, const QVariant& val) = 0;

  virtual SIRecord getCopy() const = 0;

  QVariant operator[](int i)               const { return value(i); }
  QVariant operator[](const QString& name) const { return value(name); }

  SIRecord getInstance();
};

// Da utilizzare al posto dell'interfaccia.
// Instanziabile.
class SIRecord : public IRecord, public shareable<IRecord>
{
public:

  virtual int      count()                    const override { return _ptr->count(); }
  virtual QString  fieldName(int i)           const override { return _ptr->fieldName(i); }
  virtual QVariant value(int i)               const override { return _ptr->value(i); }
  virtual QVariant value(const QString &name) const override { return _ptr->value(name); }
  virtual void     setValue(int i, const QVariant &val)               override { return _ptr->setValue(i, val); }
  virtual void     setValue(const QString &name, const QVariant &val) override { return _ptr->setValue(name, val); }

  virtual SIRecord getCopy() const override { return _ptr->getCopy(); }

  explicit SIRecord(IRecord* ptr) : shareable(ptr) {  }
};

inline SIRecord IRecord::getInstance()
{
  return SIRecord(this);
}


} // ::engine::

#endif // IRECORD_H
