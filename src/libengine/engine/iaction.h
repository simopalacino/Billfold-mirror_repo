#ifndef IACTION_H
#define IACTION_H

#include <QString>

namespace engine {


class IAction
{
public:
  virtual int exec() = 0;
  virtual int undo() = 0;

  virtual QString name() const = 0;
};


} // ::engine::
#endif // IACTION_H
