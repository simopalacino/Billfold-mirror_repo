#ifndef INSERTNEWRECORDACTION_H
#define INSERTNEWRECORDACTION_H

#include "engine.h"
#include "iaction.h"
#include <memory>

namespace engine {


class ENGINE_EXPORT InsertNewRecordAction : public IAction
{
  Libengine*  _lib;
  engine::SIRecord _record;
  QString     _idRecord;
  bool        _performed{ false };

  // IAction interface
public:
  virtual int exec() override;
  virtual int undo() override;

  virtual QString name() const override { return "InsertNewRecordAction"; }

  InsertNewRecordAction(Libengine* lib, engine::SIRecord record) : _lib(lib), _record(record) { }
};


} // ::engine::

#endif // INSERTNEWRECORDACTION_H
