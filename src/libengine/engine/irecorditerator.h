#ifndef IRECORDITERATOR_H
#define IRECORDITERATOR_H

#include <memory>
#include <QVariant>
#include <QString>
#include "irecord.h"

namespace engine {


class SIRecordIterator;
// Adapter to Query a db
class IRecordIterator
{
public:
  virtual SIRecord get()                      const = 0;
  virtual bool     isValid()                  const = 0;
  virtual QVariant value(const QString &name) const = 0;
  virtual QVariant value(int i)               const = 0;
  virtual bool     next()                           = 0;
  virtual bool     exec(const QString& query)       = 0;

  QVariant operator[](const QString &name) const { return value(name); }
  QVariant operator[](int i)               const { return value(i);    }

  SIRecordIterator getInstance();
};


class SIRecordIterator : public IRecordIterator, public shareable<IRecordIterator>
{
public:
  virtual SIRecord get()                      const override { return _ptr->get();       }
  virtual bool     isValid()                  const override { return _ptr->isValid();   }
  virtual QVariant value(const QString &name) const override { return _ptr->value(name); }
  virtual QVariant value(int i)               const override { return _ptr->value(i);    }
  virtual bool     next()                           override { return _ptr->next();      }
  virtual bool     exec(const QString &query)       override { return _ptr->exec(query); }

  SIRecordIterator(IRecordIterator* ptr) : shareable(ptr) {  }
};

inline SIRecordIterator IRecordIterator::getInstance()
{
  return SIRecordIterator(this);
}


} // ::engine::


#endif // IRECORDITERATOR_H
