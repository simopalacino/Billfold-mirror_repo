#ifndef RECORDELEMENT_H
#define RECORDELEMENT_H

#include <QString>
#include <QVariant>
#include <memory>

namespace engine {


class Visitor;
class RecordElement;

using RecordElementMap = std::map<QString, RecordElement*>;

class RecordElement
{
public:
  virtual void accept(Visitor& visitor) = 0;
  virtual QVariant get() = 0;
  static std::shared_ptr<RecordElementMap> getContainer() { return std::make_shared<RecordElementMap>(); }
};


} // ::engine::

#endif // RECORDELEMENT_H
