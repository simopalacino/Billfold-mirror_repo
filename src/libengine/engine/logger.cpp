#include "logger.h"
//#ifdef DBG
#include <QDebug>
//#endif

namespace engine {


Logger logger;

static std::mutex s_mutex;

void Logger::open(const char *logFile)
{
  fout = fopen(logFile, "a");
  if (!fout)
    fprintf(stderr, "Error opening %s file\n", logFile);
}

void Logger::operator()(const char *format, ...)
{
  va_list   l;
  time_t    timer;
  char      buffer[26];
  struct tm *tm_info;

  timer   = time(NULL);
  tm_info = localtime(&timer);
  strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

  if (fout)
  {
    s_mutex.lock();
    std::fprintf(fout, "[%s] ", buffer);

    va_start(l, format);
    std::vfprintf(fout, format, l);
    va_end(l);
    std::fprintf(fout, "\n");
//#ifdef DBG
    {
      static int size  = 64;
      char       *buff = nullptr;
      while (1)
      {
        va_start(l, format);
        buff    = new char[size];
        int ret = vsnprintf(buff, size, format, l);
        va_end(l);
        if (ret >= size - 1)
        {
          size <<= 1;
          delete[] buff;
          continue;
        }
        break;
      }
      qDebug("%s\n", buff);
      delete[] buff;
    }
//#endif
    s_mutex.unlock();
  }
}


}
