#include "insertnewcategoryaction.h"

int engine::InsertNewCategoryAction::exec()
{
  uint id;
  if ((_performed = _lib->insertNewCategory(_primaryName, _groupName, &id)))
    _idRecord = id;
  return _performed ? 0 : -1;
}

int engine::InsertNewCategoryAction::undo()
{
  bool ok;
  if (_performed)
    ok = _lib->deleteCategory(_idRecord);
  // performed --> deleted
  return !_performed || ok ? 0 : -1;
}

engine::InsertNewCategoryAction::InsertNewCategoryAction(Libengine *lib, const QString &primaryName, const QString &groupName)
  : _lib(lib), _primaryName(primaryName), _groupName(groupName)
{ }
