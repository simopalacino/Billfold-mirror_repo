#ifndef SQLQUERYRECORDITERATOR_H
#define SQLQUERYRECORDITERATOR_H

#include <QSqlQuery>
#include <QSqlResult>
#include "irecorditerator.h"
#include "sqlrecordrecord.h"


namespace engine {


class SqlQueryRecordIterator : public IRecordIterator
{
  QSqlQuery _sqlQuery;

public:
  virtual SIRecord get()                      const override { return (new SqlRecordRecord(_sqlQuery.record()))->getInstance(); }
  virtual bool     isValid()                  const override { return _sqlQuery.isValid(); }
  virtual QVariant value(const QString &name) const override { return _sqlQuery.value(name); }
  virtual QVariant value(int i)               const override { return _sqlQuery.value(i); }
  virtual bool     next()                           override { return _sqlQuery.next(); }
  virtual bool     exec(const QString& query)       override { return _sqlQuery.exec(query); }

  explicit SqlQueryRecordIterator(QSqlResult *r) : _sqlQuery(r) { }
  explicit SqlQueryRecordIterator(const QString& query = QString(), QSqlDatabase db = QSqlDatabase()) : _sqlQuery(query, db) { }
  explicit SqlQueryRecordIterator(QSqlDatabase db) : _sqlQuery(db) { }
  SqlQueryRecordIterator(const QSqlQuery& other) : _sqlQuery(other) { }
};


} // ::engine::

#endif // SQLQUERYRECORDITERATOR_H
