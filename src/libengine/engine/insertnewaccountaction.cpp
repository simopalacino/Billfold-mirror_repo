#include "insertnewaccountaction.h"

int engine::InsertNewAccountAction::exec()
{
  uint id;
  if ((_performed = _lib->insertNewAccount(_record, &id)))
    _idRecord = id;
  return _performed ? 0 : -1;
}

int engine::InsertNewAccountAction::undo()
{
  int ok;
  if (_performed)
    ok = _lib->deleteAccount(_idRecord);
  // performed --> deleted
  return !_performed || ok ? 0 : -1;
}
