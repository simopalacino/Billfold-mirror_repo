#ifndef SHAREABLE_H
#define SHAREABLE_H

#include <memory>
#include <QDebug>

namespace engine {


template<class T>
class shareable
{
protected:
  std::shared_ptr<T> _ptr;
public:
  bool operator==(std::nullptr_t nptr) { return _ptr == nptr; }
  bool operator!=(std::nullptr_t nptr) { return _ptr != nptr; }

  shareable& operator=(shareable&& other)      { _ptr.swap(other._ptr); return *this; }
  shareable& operator=(const shareable& other) { _ptr = other._ptr;     return *this; }
  shareable(shareable&& other)                 { _ptr.swap(other._ptr); }
  shareable(shareable& other)                  { _ptr = other._ptr; }
  explicit shareable(T* ptr)                   { _ptr = std::shared_ptr<T>(ptr); }
  virtual ~shareable()                         { _ptr = nullptr; }
};


} // ::engine::

#endif // SHAREABLE_H
