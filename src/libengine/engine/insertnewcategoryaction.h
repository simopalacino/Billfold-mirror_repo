#ifndef INSERTNEWCATEGORYACTION_H
#define INSERTNEWCATEGORYACTION_H

#include "engine.h"
#include "iaction.h"
#include <memory>

namespace engine {


class ENGINE_EXPORT InsertNewCategoryAction : public IAction
{
  Libengine* _lib;
  QString    _primaryName;
  QString    _groupName;
  uint       _idRecord;
  bool       _performed{ false };

  // IAction interface
public:
  virtual int exec() override;
  virtual int undo() override;

  virtual QString name() const override { return "InsertNewCategoryAction"; }

  InsertNewCategoryAction(Libengine* lib, const QString& primaryName, const QString& groupName);
};


} // ::engine::

#endif // INSERTNEWCATEGORYACTION_H
