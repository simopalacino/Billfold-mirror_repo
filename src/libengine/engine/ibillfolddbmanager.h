#ifndef IBILLFOLDDBMANAGER_H
#define IBILLFOLDDBMANAGER_H

#include <memory>
#include <QSqlQuery>
#include "irecorditerator.h"


namespace engine {


class IBillfoldDBManager
{
public:
  virtual SIRecord         selectRecord(const QString& idRecord)                                                        const = 0;
  virtual bool             insertNewRecord(const engine::SIRecord& record, uint* id = nullptr)                                = 0;
  virtual bool             updateRecord(const QString& oldId, const SIRecord& record, uint* id = nullptr)                     = 0;
  virtual bool             deleteRecord(const QString &idRecord)                                                              = 0;
  virtual SIRecordIterator selectMovements(const QString& dateFrom, const QString& dateTo, const QString& accountsList) const = 0;

  virtual bool             insertNewAccount(const std::map<QString, QVariant>& record, uint* id = nullptr) = 0;
  virtual bool             accountEditRecord(SIRecord& oldRec, SIRecord& newRec)           = 0;
  virtual bool             deleteAccount(uint32_t id)                                      = 0;
  virtual SIRecordIterator selectAccounts()                                          const = 0;

  virtual bool             insertNewCategory(const QString& primaryName, const QString& groupName, uint* id = nullptr)       = 0;
  virtual void             categoryEditRecord(SIRecord& oldRec, SIRecord& newRec)                                            = 0;
  virtual bool             deleteCategory(uint32_t id)                                                                       = 0;
  virtual SIRecordIterator selectCategories()                                                                          const = 0;

  virtual SIRecordIterator customQuery(QString query) = 0;
};


} // ::engine::

#endif // IBILLFOLDDBMANAGER_H
