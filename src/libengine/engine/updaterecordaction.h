#ifndef UPDATERECORDACTION_H
#define UPDATERECORDACTION_H

#include "engine.h"
#include "iaction.h"

namespace engine {


class ENGINE_EXPORT UpdateRecordAction : public IAction
{
  Libengine*  _lib;
  QString     _oldId;
  QString     _idRecord;
  bool        _performed{ false };
  const SIRecord& _record;
  SIRecord        _oldRecord{ nullptr };

public:
  virtual int exec() override;
  virtual int undo() override;

  virtual QString name() const override { return "UpdateRecordAction"; }
public:
  UpdateRecordAction(Libengine* lib, const QString& oldId, const SIRecord& record) : _lib(lib), _oldId(oldId), _record(record) { }
};


} // ::engine::

#endif // UPDATERECORDACTION_H
