#ifndef LOGGER_H
#define LOGGER_H

#include <mutex>
#include <cstdarg>
#include "engine_global.h"
#include <QString>

#define QSTR2CSTR(str) (str).toLocal8Bit().data()


namespace engine {



class ENGINE_EXPORT Logger
{
  QString append;
public:
  FILE *fout;
  explicit Logger() noexcept = default;
  ~Logger() = default;
  Logger(const Logger&) = delete;
  Logger operator=(const Logger&) = delete;

  void open(const char *logFile = "liblog.log");
  /** Thread safe. */
  void operator()(const char* format, ...);
  inline Logger &operator<<(QString &str)
  {
    append = str + append;
    (*this)("%s", str.toLocal8Bit().data());
    return *this;
  }
};

extern Logger logger;

}

#endif // LOGGER_H
