QT -= gui
QT += network
QT += sql

TEMPLATE = lib
DEFINES += ENGINE_LIBRARY

CONFIG += c++11
INCLUDEPATH += $$PWD/../include/

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../include/utils.cpp \
    ../include/Database/db_table.c \
    ../include/Database/tnumeric.cpp \
    engine/db/tdatabase.cpp \
    engine/db/updatersqlserverdb100.cpp \
    engine/db/updatersqlserverdb102.cpp \
    engine/engine.cpp \
    engine/insertnewaccountaction.cpp \
    engine/insertnewcategoryaction.cpp \
    engine/insertnewrecordaction.cpp \
    engine/logger.cpp \
    engine/updaterecordaction.cpp

HEADERS += \
    ../include/Database/tdate.h \
    ../include/json_categories.h \
    ../include/json_record.h \
    ../include/utils.h \
    ../include/Database/db_table.h \
    ../include/Database/records.h \
    ../include/Database/tnumeric.h \
    engine/db/accounts.h \
    engine/db/iupdaterdb100.h \
    engine/db/iupdaterdb102.h \
    engine/db/queries.h \
    engine/db/tdatabase.h \
    engine/db/updatersqlserverdb100.h \
    engine/db/updatersqlserverdb102.h \
    engine/db/users.h \
    engine/engine.h \
    engine/iaction.h \
    engine/ibillfolddbmanager.h \
    engine/insertnewaccountaction.h \
    engine/insertnewcategoryaction.h \
    engine/insertnewrecordaction.h \
    engine/irecord.h \
    engine/irecorditerator.h \
    engine/logger.h \
    engine/recordelement.h \
    engine/shareable.h \
    engine/sqlqueryrecorditerator.h \
    engine/sqlrecordrecord.h \
    engine/updaterecordaction.h \
    engine_global.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
