#ifndef JSON_CATEGORIES_H
#define JSON_CATEGORIES_H

#define CAT_NAME        "name"
#define CAT_LAST_UPDATE "last_update"
#define CAT_USER_ID     "user_id"
#define CAT_ID          "id"
#define CAT_PARENT_ID   "parent_id"

#endif // JSON_CATEGORIES_H
