#include "utils.h"
#include <QString>

#ifdef QT_WIDGETS_LIB
#include <QMessageBox>
void error(const QString &str, QWidget *parent)
{
  QMessageBox::critical(parent, "error", str);
  appLog("%s", str.toUtf8().data());
}
#endif

engine::Logger appLog;


static class TString_motor
{
  std::vector<std::vector<std::string*>> _tempVect;
} _string_motor;



std::string &getTempString()
{
  static std::vector<std::string> _tempVect;
  static uint32_t                 _idx       = 0;
  static uint32_t                 _dimension = 256;

  if (_tempVect.capacity() < _dimension)
    _tempVect.reserve(_dimension);
  if (_tempVect.size() < _tempVect.capacity())
    _tempVect.emplace_back();

  uint32_t appo = _idx;
  _idx = (_idx + 1) % _dimension;
  _tempVect.operator[](appo).clear();
  return _tempVect.operator[](appo);
}

//std::string *getNewNTempStrings(uint32_t n)
//{
//    return _string_motor.emplace_and_return(n);
//}

std::string &getReservedString()
{
  static std::vector<std::string*> vect;
  vect.emplace_back(new std::string);
  return **(vect.end() - 1);
}

QString &doppiApici(QString &str)
{
  int p = 0;
  while ( (p = str.indexOf("'", p)) != -1 )
  {
    str.replace(p, 1, "''");
  }
  return str;
}
