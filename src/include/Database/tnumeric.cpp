#include "tnumeric.h"
#include <cstring>
#include <cmath>
#include <memory>
#include "utils.h"

#define PRECISION_SIZE 1
#define SCALE_SIZE     1


namespace Database
{

const std::pair<TNumeric::ErrorId, const char*> TNumeric::_errorMessages[32] = {
    { Overflow,   "Arithmetic overflow error converting to data type numeric." },
    { Convertion, "Error converting data type to numeric." },
    { OutOfRange, "The number '%1' is out of the range for numeric " \
      "representation (maximum precision %2)." }
};


const TNumeric TNumeric::ZERO = TNumeric("0");


void TNumeric::initializeSpace(uint8_t precision, uint8_t scale)
{
    delete _data;
    _data = new uint8_t[PRECISION_SIZE + SCALE_SIZE + calcSizeNum(precision)];
    _data[PRECISION_SIZE - 1]              = precision;
    _data[PRECISION_SIZE + SCALE_SIZE - 1] = scale;
    for (size_t i = 0; i < precision; ++i)
        _data[PRECISION_SIZE + SCALE_SIZE + i] = 0;
}

void TNumeric::setError(const ErrorId id, const bool setInvalid, const std::vector<QString> *args)
{
    _last_error_validity = _errorMessages[id].second;
    if (id == OutOfRange && args && args->size() == 2)
    {
        _last_error_validity = _last_error_validity.arg((*args)[0]).arg((*args)[1]);
    }
    if (setInvalid)
        _valid = false;
}

void TNumeric::setError(const QString &msg, const bool setInvalid)
{
    _last_error_validity = msg;
    for (uint8_t i = 0; i < precision(); ++i)
        _data[2 + i] = 0;
    if (setInvalid)
        _valid = false;
}

void TNumeric::setInvalidNumeric()
{
    if (!_data)
    {
        _data                                  = new uint8_t[PRECISION_SIZE + SCALE_SIZE + 1];
        _data[PRECISION_SIZE - 1]              = -1;
        _data[PRECISION_SIZE + SCALE_SIZE - 1] = -1;
        _data[PRECISION_SIZE + SCALE_SIZE]     = 0;
    }
    else
    {
        for (uint32_t i = 0; i < precision(); ++i)
            _data[i + 2] = 0;
    }
    _valid = false;
}

void TNumeric::setNumeric(const char *str, const bool allocDynamicSpace)
{
    std::vector<uint8_t> v_data;

    const char *start_pos = std::strstr(str, ".");

    size_t  len        = std::strlen(str);
    uint8_t i          = 0;
    size_t  decimalPos = start_pos ? start_pos - str : len;
    // Skip space in front.
    for (; str[i] == ' ' && i < decimalPos - 1; ++i)
    {  }
    bool negative = str[i] == '-';
    if (negative) ++i;
    // Skip space in front.
    for (; str[i] == ' ' && i < decimalPos - 1; ++i)
    {  }
    // Skip 0s in front.
    for (; str[i] == '0' && i < decimalPos - 1; ++i)
    {  }

    int32_t dim = (int)len - i - (start_pos ? 1 : 0);
    if (dim <= 0)
    {
        setError(Convertion);
        setInvalidNumeric();
    }
    else if (dim > MAX_PRECISION)
    {
        setError(QString::fromUtf8("The number '%1' is out of the range for numeric " \
                                   "representation (maximum precision %2).").arg(str).arg(MAX_PRECISION));
        setInvalidNumeric();
    }
    else
    {
        uint8_t _precision = 0;
        uint8_t _scale     = 0;
        if (!allocDynamicSpace)
        {
            _precision = precision();
            _scale     = scale();
        }
        v_data.reserve(dim);
        if (start_pos)
        {
            uint8_t dimParteIntera = 0;
            // Add all digits till dot.
            for (; i < decimalPos; ++i, ++dimParteIntera)
            {
                int nn = str[i] - '0';
                if (0 > nn || nn > 9)
                {
                    setError(Convertion);
                    setInvalidNumeric();
                    return;
                }
                v_data.emplace_back(nn);
            }

            // Skip dot. Add decimals
            ++i;
            uint8_t decimals = 0;
            for (; i < len; ++i, ++decimals)
            {
                int nn = str[i] - '0';
                if (0 > nn || nn > 9)
                {
                    setError(Convertion);
                    setInvalidNumeric();
                    return;
                }
                v_data.emplace_back(nn);
            }

            if (!allocDynamicSpace)
            {
                for (int i = 0; i < _scale - decimals; ++i)
                    v_data.emplace_back(0);
                if (arrotondaDecimali(v_data, dimParteIntera, _precision, _scale) == NUM_OVERFLOW)
                {
                    setError(Overflow);
                    setInvalidNumeric();
                    return;
                }
            }
            else
            {
                _precision = dimParteIntera + decimals;
                _scale     = decimals;
            }
        }
        else
        {
            // Add all digits.
            for (; i < len; ++i)
            {
                int nn = str[i] - '0';
                if (0 > nn || nn > 9)
                {
                    setError(Convertion);
                    setInvalidNumeric();
                    return;
                }
                if (allocDynamicSpace)
                    ++_precision;
                v_data.emplace_back(nn);
            }
            if (!allocDynamicSpace)
            {
                for (uint8_t i = 0; i < _scale; ++i)
                    v_data.emplace_back(0);
            }
        }

        if (allocDynamicSpace)
        {
            if (!validPrecisionScale(_precision, _scale))
            {
                setError(Convertion);
                setInvalidNumeric();
                return;
            }
            initializeSpace(_precision, _scale);
        }

        for (uint32_t i = 0; i < _precision - v_data.size(); ++i) _data[2 + i] = 0;

        memcpy(_data + 2 + _precision - v_data.size(), &v_data[0], v_data.size());
        // Se negativo aggiungo il bit.
        if (!isZero())
            setSign(!negative);
        _valid = true;
    }
}

TNumeric &TNumeric::setSign(const bool positive)
{
    if (positive)
        _data[2] &= 0x7f;
    else
        _data[2] |= 0x80;
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    return *this;
}

TNumeric::Res TNumeric::arrotondaDecimali(std::vector<uint8_t> &data, const uint8_t decimalPos, const uint8_t precision, const uint8_t scale)
{
    const size_t len = data.size();
    if (len - (decimalPos) > scale)
    {
        uint8_t n = data[decimalPos + scale];
        data.erase(data.begin() + decimalPos + scale, data.end());
        if (5 <= n && n <= 9)
        {
            size_t  i = decimalPos + scale - 1;
            for (auto it = data.begin() + i; ; --it, --i)
            {
                if (*it < 9)
                {
                    ++*it;
                    break;
                }
                else
                    *it = 0;

                // Main exit condition
                if (!i)
                {
                    if (len == precision)
                        return NUM_OVERFLOW;
                    data.insert(data.begin(), 1);
                    return NUM_OK;
                }
            }
        }
    }
    return NUM_OK;
}

uint8_t TNumeric::calcSizeNum(uint8_t digit)
{
//    return (uint8_t)((std::log2(std::pow(10, digit + 1) - 1) + 1) / 8);
    return digit;
}

TNumeric &TNumeric::changeSign()
{
    if (!isZero())
        setSign(!getSign());
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    return *this;
}

int TNumeric::compare(const TNumeric &rhv) const
{
    std::vector<uint8_t> val1, val2;
    uint8_t pIntera   = std::max(precision() - scale(), rhv.precision() - rhv.scale());
    uint8_t pDecimali = std::max(scale(), rhv.scale());
    val1.reserve(pIntera + pDecimali);
    val2.reserve(pIntera + pDecimali);
    for (uint32_t i = 0; i < pIntera + pDecimali; ++i)
    {
        val1.emplace_back(0);
        val2.emplace_back(0);
    }

    const uint8_t _precision1 = precision(), _precision2 = rhv.precision();
    const uint8_t _scale1     = scale(),     _scale2     = rhv.scale();
    for (uint32_t i = 0; i < _precision1; ++i)
        val1[pIntera - (_precision1 - _scale1) + i] = _data[i + 2];
    for (uint32_t i = 0; i < _precision2; ++i)
        val2[pIntera - (_precision2 - _scale2) + i] = rhv._data[i + 2];


    for (uint32_t i = 0; i < pIntera + pDecimali; ++i)
    {
        if (val1[i] < val2[i])
            return -1;
        else if (val1[i] > val2[i])
            return 1;
    }
    return 0;
}

std::vector<uint8_t> TNumeric::getDataRaw() const
{
    std::vector<uint8_t> data;
    uint32_t size = _valid ? 2 + calcSizeNum(precision()) : 3;
    data.reserve(size);
    for (uint32_t i = 0; i < size ; ++i)
        data.emplace_back(_data[i]);
    return data;
}

bool TNumeric::isZero() const
{
    bool flag = false;
    if (isValid())
        for (uint32_t i = 0; i < precision() && (flag = _data[i + 2] == 0); ++i) {  }
    return flag;
}

QString TNumeric::getError() const
{
    return _last_error_validity;
}

bool TNumeric::getSign() const
{
    return !(_data[2] & 0x80);
}

TNumeric &TNumeric::set(const char *str)
{
    if (validPrecisionScale(precision(), scale()))
    {
        setNumeric(str, false);
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    }
    return *this;
//    std::vector<uint8_t> v_data;

//    const char *start_pos = std::strstr(str, ".");

//    size_t  len        = std::strlen(str);
//    uint8_t i          = 0;
//    size_t  decimalPos = start_pos ? start_pos - str : len;
//    // Skip space in front.
//    for (; str[i] == ' ' && i < decimalPos - 1; ++i)
//    {  }
//    bool negative = str[i] == '-';
//    if (negative) ++i;
//    // Skip space in front.
//    for (; str[i] == ' ' && i < decimalPos - 1; ++i)
//    {  }
//    // Skip 0s in front.
//    for (; str[i] == '0' && i < decimalPos - 1; ++i)
//    {  }

//    int32_t dim = (int)len - i - (start_pos ? 1 : 0);
//    if (dim <= 0)
//    {
//        setError(Convertion);
//        setInvalidNumeric();
//    }
//    else if (dim > MAX_PRECISION)
//    {
//        setError(QString::fromUtf8("The number '%1' is out of the range for numeric "
//                                   "representation (maximum precision %2).").arg(str).arg(MAX_PRECISION));
//        setInvalidNumeric();
//    }
//    else
//    {
//        const uint8_t _precision = precision();
//        const uint8_t _scale     = scale();
//        v_data.reserve(dim);
//        if (start_pos)
//        {
//            uint8_t dimParteIntera = 0;
//            // Add all digits till dot.
//            for (; i < decimalPos; ++i, ++dimParteIntera)
//            {
//                int nn = str[i] - '0';
//                if (0 > nn || nn > 9)
//                {
//                    setError(Convertion);
//                    setInvalidNumeric();
//                    return *this;
//                }
//                v_data.emplace_back(nn);
//            }
//            // Skip dot. Add decimals
//            ++i;
//            uint8_t decimals = 0;
//            for (; i < len; ++i, ++decimals)
//            {
//                int nn = str[i] - '0';
//                if (0 > nn || nn > 9)
//                {
//                    setError(Convertion);
//                    setInvalidNumeric();
//                    return *this;
//                }
//                v_data.emplace_back(nn);
//            }

//            for (int i = 0; i < _scale - decimals; ++i)
//                v_data.emplace_back(0);

//            if (arrotondaDecimali(v_data, dimParteIntera, _precision, _scale) == NUM_OVERFLOW)
//            {
//                setError(Overflow);
//                setInvalidNumeric();
//                return *this;
//            }
//        }
//        else
//        {
//            // Add all digits.
//            for (; i < len; ++i)
//            {
//                int nn = str[i] - '0';
//                if (0 > nn || nn > 9)
//                {
//                    setError(Convertion);
//                    setInvalidNumeric();
//                    return *this;
//                }
//                v_data.emplace_back(nn);
//            }
//            for (uint8_t i = 0; i < _scale; ++i)
//                v_data.emplace_back(0);
//        }

//        memcpy(_data + 2 + _precision - v_data.size(), &v_data[0], v_data.size());
//        // Se negativo aggiungo il bit.
//        if (!isZero())
//            setSign(!negative);
//        _valid = true;
//    }
//    return *this;
}

TNumeric &TNumeric::set(const double num)
{
    return set(std::to_string(num).c_str());
}

double TNumeric::toDouble() const
{
    const char *appo = this->operator const char *();
    return strtod(appo, NULL);
}

Database::TNumeric::operator const char *() const
{
    if (!_data || !_valid)
        return "0";
    std::string &strAppo = getTempString();

    uint8_t *num = _data + 2;
    if (num[0] & 0x80)
        strAppo += "-";

    bool found = false;
    if ((num[0] & 0x0f) > 0)
    {
        strAppo += (num[0] & 0x0f) + '0';
        found = true;
    }

    const uint8_t _precision = precision();
    const uint8_t _scale     = scale();
    for (size_t i = 1; (int)i < _precision - _scale; ++i)
    {
        if (found || num[i] > 0)
        {
            strAppo += num[i] + '0';
            found = true;
        }
    }
    if (!found)
        strAppo += '0';
    if (_scale)
    {
        strAppo += '.';
        for (size_t i = _precision - _scale; i < _precision; ++i)
            strAppo += num[i] + '0';
    }
    return strAppo.c_str();
}

bool TNumeric::operator<(const TNumeric &rhv) const
{
    return (!getSign() && rhv.getSign()) ||
            (getSign() && rhv.getSign() && compare(rhv) == -1);
}

bool TNumeric::operator<=(const TNumeric &rhv) const
{
    return !operator>(rhv);
}

bool TNumeric::operator>(const TNumeric &rhv) const
{
    return (getSign() && !rhv.getSign()) ||
            (getSign() && rhv.getSign() && compare(rhv) == 1);
}

bool TNumeric::operator>=(const TNumeric &rhv) const
{
    return !operator<(rhv);
}

TNumeric TNumeric::operator+(const TNumeric &rhv) const
{

    if (!isValid() || !rhv.isValid())
        return TNumeric("INVALID", 1, 0);

    const bool p1 = getSign(), p2 = rhv.getSign();
    if (p1 && !p2)          // [(1)+] + [(2)-] => [(1)+] - [(2)+]
    {
        return operator-(TNumeric(rhv).changeSign());
    }
    else if (!p1 && p2)     // [(1)-] + [(2)+] => [(2)+] - [(1)+]
    {
        return rhv - abs(*this);
    }
    else if (!p1 && !p2)    // [(1)-] + [(2)-] => -([(1)+] - [(2)+])
    {
        const TNumeric n1 = TNumeric(*this).changeSign();
        return -(abs(*this).operator+(abs(rhv)));
    }


    std::vector<uint8_t> val1, val2;

    val1.reserve(MAX_PRECISION);
    val2.reserve(MAX_PRECISION);

    const TNumeric& n1 = this->scale() <= rhv.scale() ? *this : rhv;
    const TNumeric& n2 = this->scale() <= rhv.scale() ? rhv : *this;
    const uint8_t _precision1 = precision(), _precision2 = rhv.precision();
    const uint8_t _scale1     = scale(),     _scale2     = rhv.scale();

    const uint8_t _precision  = MAX_PRECISION;
    const uint8_t _scale      = std::min((int)n2.scale(), (int)MAX_PRECISION - n1.precision() + n1.scale());
    TNumeric      ret(_precision, _scale);

    for(uint32_t i = 0; (int)i < _precision - _scale - _precision1 + _scale1; ++i)
        val1.emplace_back(0);
    for (uint32_t i = 0; i < _precision1; ++i)
        val1.emplace_back(_data[i + 2]);

    for(uint32_t i = 0; (int)i < _precision - _scale - _precision2 + _scale2; ++i)
        val2.emplace_back(0);
    for (uint32_t i = 0; i < _precision2; ++i)
        val2.emplace_back(rhv._data[i + 2]);

    const int left1 = (int)_precision - (int)val1.size();
    const int left2 = (int)_precision - (int)val2.size();
    for (int i = 0; i < left1; ++i)
        val1.emplace_back(0);
    for (int i = 0; i < left2; ++i)
        val2.emplace_back(0);

    arrotondaDecimali(val1, _precision - _scale, MAX_PRECISION, _scale);
    arrotondaDecimali(val2, _precision - _scale, MAX_PRECISION, _scale);

    uint8_t resto = 0;
    for (int32_t i = _precision - 1; i >= 0; --i)
    {
        uint8_t v = val1[i] + val2[i] + resto;
        val1[i] = v % 10;
        resto = v >= 10 ? 1 : 0;
    }
    if (resto)
    {
        // Overflow;
        TNumeric invalid(_precision, _scale);
        invalid.setError(Overflow);
        invalid.setInvalidNumeric();
        return invalid;
    }

    for (uint32_t i = 0; i < _precision; ++i)
        ret._data[i + 2] = val1[i];
    return ret;
}

TNumeric TNumeric::operator-(const TNumeric &rhv) const
{
    if (!isValid() || !rhv.isValid())
    {
        return TNumeric(-1, -1);
    }

    const bool p1 = getSign(), p2 = rhv.getSign();
    if (p1 && !p2)
    {
        return *this + abs(rhv);
    }
    else if (!p1 && p2)
    {
        return -(abs(*this) + rhv);
    }
    else if (!p1 && !p2)
    {
        return abs(rhv) + *this;
    }
    else
    {
        if (operator<(rhv))
            return -(rhv - *this);
    }

    std::vector<uint8_t> val1, val2;

    val1.reserve(MAX_PRECISION);
    val2.reserve(MAX_PRECISION);

    const TNumeric& n1 = this->scale() <= rhv.scale() ? *this : rhv;
    const TNumeric& n2 = this->scale() <= rhv.scale() ? rhv : *this;
    const uint8_t _precision1 = precision(), _precision2 = rhv.precision();
    const uint8_t _scale1     = scale(),     _scale2     = rhv.scale();

    const uint8_t _precision  = MAX_PRECISION;
    const uint8_t _scale      = std::min((int)n2.scale(), (int)MAX_PRECISION - n1.precision() + n1.scale());
    TNumeric      ret(_precision, _scale);

    for(uint32_t i = 0; (int)i < _precision - _scale - _precision1 + _scale1; ++i)
        val1.emplace_back(0);
    for (uint32_t i = 0; i < _precision1; ++i)
        val1.emplace_back(_data[i + 2]);

    for(uint32_t i = 0; (int)i < _precision - _scale - _precision2 + _scale2; ++i)
        val2.emplace_back(0);
    for (uint32_t i = 0; i < _precision2; ++i)
        val2.emplace_back(rhv._data[i + 2]);

    const int left1 = (int)_precision - (int)val1.size();
    const int left2 = (int)_precision - (int)val2.size();
    for (int i = 0; i < left1; ++i)
        val1.emplace_back(0);
    for (int i = 0; i < left2; ++i)
        val2.emplace_back(0);

    arrotondaDecimali(val1, _precision - _scale, MAX_PRECISION, _scale);
    arrotondaDecimali(val2, _precision - _scale, MAX_PRECISION, _scale);


    for (int32_t i = _precision - 1; i >= 0; --i)
    {
        if (val1[i] >= val2[i])
            val1[i] -= val2[i];
        else
        {
            for (int j = i; j >= 0; --j)
            {
                if (val1[j - 1] > 0)
                {
                    val1[j - 1] -= 1;
                    break;
                }
                val1[j - 1] = 9;
            }
            val1[i] = val1[i] + 10 - val2[i];
        }
    }

    for (uint32_t i = 0; i < _precision; ++i)
        ret._data[i + 2] = val1[i];
    return ret;
}

bool TNumeric::operator==(const TNumeric &rhv) const
{
    return isValid() && rhv.isValid() && compare(rhv) == 0;
}

bool TNumeric::isEqualTo(const TNumeric &rhv) const
{
    const uint8_t _precision = precision();
    if (_precision != rhv.precision() || scale() != rhv.scale())
        return false;
    for (uint32_t i = 0; i < _precision; ++i)
    {
        if (_data[i + 2] != rhv._data[i + 2])
            return false;
    }
    return true;
}

bool TNumeric::operator!=(const TNumeric &rhv) const
{
    return !operator==(rhv);
}

TNumeric &TNumeric::operator<<(const char *rhv)
{
    return set(rhv);
}

TNumeric &TNumeric::operator<<(double rhv)
{
    return set(rhv);
}

TNumeric &TNumeric::operator=(const char *rhv)
{
    if (isValid())
        set(rhv);
    return *this;
}

TNumeric &TNumeric::operator()(const char *rhv)
{
    setNumeric(rhv, true);
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    return *this;
}

TNumeric &TNumeric::operator=(const double rhv)
{
    if (isValid())
        set(rhv);
    return *this;
}

TNumeric &TNumeric::operator=(const TNumeric &rhv)
{
    this->_valid = rhv._valid;
    this->_data  = new uint8_t[PRECISION_SIZE + SCALE_SIZE + calcSizeNum(rhv.precision())];
    memcpy(this->_data, rhv._data, PRECISION_SIZE + SCALE_SIZE + calcSizeNum(rhv.precision()));

#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    return *this;
}

TNumeric &TNumeric::operator=(TNumeric &&rhv)
{
    this->_valid = rhv._valid;
    this->_data  = rhv._data;
    rhv._data    = nullptr;

#ifdef QT_DEBUG
    _string = operator const char *();
#endif
    return *this;
}

TNumeric::TNumeric(const char *initialValue) :
    _data(nullptr), _valid(true)
{
    setNumeric(initialValue, true);
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
}

TNumeric::TNumeric(double initialValue) : TNumeric(std::to_string(initialValue).c_str())
{  }

TNumeric::TNumeric(const TNumeric &rhv)
{
    this->_valid = rhv._valid;
    this->_data  = new uint8_t[PRECISION_SIZE + SCALE_SIZE + calcSizeNum(rhv.precision())];
    memcpy(this->_data, rhv._data, PRECISION_SIZE + SCALE_SIZE + calcSizeNum(rhv.precision()));
    _last_error_validity = rhv._last_error_validity;
#ifdef QT_DEBUG
    _string = rhv._string;
#endif
}

TNumeric::TNumeric(TNumeric &&rhv)
{
    this->_valid = rhv._valid;
    this->_data  = rhv._data;
    rhv._data    = nullptr;
    _last_error_validity = rhv._last_error_validity;
#ifdef QT_DEBUG
    _string = rhv._string;
#endif
}

TNumeric::TNumeric(uint8_t precision, uint8_t scale) :
    _data(nullptr), _valid(true)
{
    if (validPrecisionScale(precision, scale))
    {
        initializeSpace(precision, scale);
        set("0");
    }
    else
    {
        setInvalidNumeric();
    }
#ifdef QT_DEBUG
    _string = operator const char *();
#endif
}

TNumeric::TNumeric(const char *initialValue, uint8_t precision, uint8_t scale) : TNumeric(precision, scale)
{
    set(initialValue);
}

TNumeric::TNumeric(const double initialValue, uint8_t precision, uint8_t scale) : TNumeric(precision, scale)
{
    set(initialValue);
}



}
