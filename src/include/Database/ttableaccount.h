#ifndef TTABLEACCOUNT_H
#define TTABLEACCOUNT_H

#include <cstdint>
#include <QDate>
#include "tnumeric.h"

#define COPY_STR(var, str) strncpy((var), (str).toLocal8Bit().data(), sizeof((var)) - 1)

namespace Database {


struct TTableAccount
{
  uint32_t conto;
  char     name[32];
  char     owner[122];
  char     banca[64];
  bool     contanti;
  bool     risparmio;
  TNumeric init_value{18, 3};
  QDate    date_init;
  TNumeric value{14, 2};
  uint32_t color;
  uint32_t sorting;
  bool     visible;

  void setName (const QString &str) { COPY_STR(name,  str); }
  void setOwner(const QString &str) { COPY_STR(owner, str); }
  void setBanca(const QString &str) { COPY_STR(banca, str); }
};


}

#endif // TTABLEACCOUNT_H
