#ifndef TDATE_H
#define TDATE_H

#include <QDate>

namespace Database {


class TDate : protected QDate
{
  bool _nullable;
  bool _null;

public:
  void operator=(const QDate &date) { QDate::operator=(date); _null = false; }
  bool isNull() { return _nullable && _null; }
  void setNullable(bool nullable = false, bool set_null = false) { _nullable = nullable; if (set_null) setNull(); }
  void setNull(bool null = true)
  {
    if (_nullable)
    {
      if (null)
        setDate(0, 0, 0);
      _null = null;
    }
  }
  QString toAnsi() { return toString("yyyy-MM-dd"); }  // ANSI_DATE_FORMAT

  TDate(bool nullable = false) : QDate(), _nullable(nullable) { if (_nullable) setNull(); }
  TDate(int y, int m, int d, bool nullable = false) : QDate(y, m, d), _nullable(nullable) {  }
};


} // ::Database::

#endif // TDATE_H
