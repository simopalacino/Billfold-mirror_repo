#ifndef DATABASERECORDS_H
#define DATABASERECORDS_H

#include <vector>
#include <QString>
#include <QDate>
#include "tnumeric.h"
#include "tdate.h"

namespace Database
{


struct account_record_t
{
  uint32_t id;
  QString  name;
  QString  owner;
  QString  banca;
  bool     contanti;
  bool     risparmio;
  TNumeric init_value;
  QDate    date_init;
  TNumeric value;
};

enum RecordType { Outgoing, Income, Transfer };
struct new_record_t
{
  RecordType type;
  QDate      date;
  QTime      time;
  QString    cifra;           // Interpreted w/ local settings.
  QString    tag;
  QString    description;
  uint32_t   conto_id;
  TDate      acc_date{ true };
};


}

#endif // DATABASERECORDS_H
