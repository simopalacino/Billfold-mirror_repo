#ifndef _DB_TABLE_H
#define _DB_TABLE_H

#include <stdint.h>
//#include "bplustree.h"
//#include "lib.h"

#define NPOS                    __last_unsigned_num

#define MAX_LENGTH_CHAR         4096
#define MAX_LENGTH_NUMERIC      4096
#define MAX_LENGTH_NUMERIC_PREC 4096

typedef enum field_type
{
  Small_int,          // 2-byte signed number (compl-2)
  Integer,            // 4-byte signed number (compl-2)
  Long,               // 8-byte signed number (compl-2)
  Bit,
  Real,               // 4-byte floating point
  Double_precision,   // 8-byte floating point
  Numeric,            // Exact numeric of selectable precision
  Character,          // Stringa fissa [n char]
  Char_varying,       // Stringa variabile  - Non gestito il varying
  Date,
  Time,
  Datetime,
  /*Uuid,               // Universal unique identifier
  //Query             // Saved search query [reserved for stored-procedure] */
  NOTHING = 0
} field_type;

typedef struct { uint8_t v[2]; }  t_Small_int;
typedef struct { uint8_t v[4]; }  t_Integer;
typedef struct { uint8_t v[8]; }  t_Long;
typedef struct { uint8_t v[1]; }  t_Bit;
typedef struct { uint8_t v[4]; }  t_Real;
typedef struct { uint8_t v[8]; }  t_Double_precision;
typedef struct { uint8_t *v; }    t_Numeric;
typedef struct { uint8_t *v; }    t_Character;
typedef struct { uint8_t *v; }    t_Char_varying;
typedef struct { uint8_t v[4]; }  t_Date;
typedef struct { uint8_t v[8]; }  t_Time;
typedef struct { uint8_t v[8]; }  t_Datetime;
//typedef struct { uint8_t v[16]; } t_Uuid;

typedef struct { field_type type; int dim; } dim_t;

static const dim_t t_dims[] = {
  { Small_int,        2 },
  { Integer,          4 },
  { Long,             8 },
  { Bit,              1 },
  { Real,             4 },
  { Double_precision, 8 },
  { Numeric,          -1 },
  { Character,        -1 },
  { Char_varying,     -2 },
  { Date,             4 },
  { Time,             8 },
  { Datetime,         8 },
  /*{ Uuid,             16 },*/
  { NOTHING, 0 }
};

uint32_t bit_dimension(uint32_t dimension);
uint32_t bit_precision(uint32_t precision);
uint32_t byte_precision(uint32_t precision);
uint32_t byte_dimension(uint32_t dimension);

int cast_cstring_to_Time(t_Time* time, const char* str);  // OK
int cast_Time_to_cstring(t_Time* time, char** str);       // OK

#endif //_DB_TABLE_H
