#include "db_table.h"

#include <io.h>
#include <fcntl.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdbool.h>

#define DB_TABLE_NUM_TABLES(table)    ((table)->pager.tree.num_tables)
#define DB_TABLE_TREE(table)          ((table)->pager.tree)


const uint32_t __last_unsigned_num = (uint32_t)~0;

uint32_t bit_dimension(const uint32_t dimension)
{
  return (int)log2((int)pow(10, dimension) - 1) + 1;
}

uint32_t bit_precision(const uint32_t precision)
{
  return (int)log2((int)pow(10, precision) - 1) + 1;
}

uint32_t byte_dimension(const uint32_t dimension)
{
  return bit_dimension(dimension) / 8 + 1;
}

bool is_digit(char c)
{
  return c >= '0' && c <= '9';
}

int cast_cstring_to_Time(t_Time* time, const char* str)
{
  bool flag;
  size_t  size = 0;
  for (uint32_t i = 0; i < 16; ++i)   // 00:00:00.000000 15 tot + 1 Zero-terminator
  {
    if (str[i] == 0)
    {
//      flag = true;
      size = i;
      break;
    }
  }

  if(size >= 8)
  {
//    char* end;
    char hours[][3] = { {str[0], str[1], 0}, {str[3], str[4], 0}, {str[6], str[7], 0} };
    flag = true;
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 2; ++j)
      {
        if (!is_digit(hours[i][j]))
        {
          flag = false;
          break;
        }
      }
    }

    if(flag)
    {
      const uint32_t hh = strtol(hours[0], NULL, 10);
      const uint32_t mm = strtol(hours[1], NULL, 10);
      const uint32_t ss = strtol(hours[2], NULL, 10);
      uint64_t* t = (uint64_t*)time->v;
      *t = hh * 3600 + mm * 60 + ss;
      *t *= 1000000;
      if (size > 9)
      {
        if (str[8] == '.')
        {
          char m[7];
          size_t in = 0;
          for (; in < size - 9; ++in)
          {
            if (is_digit(str[9 + in]))
              m[in] = str[9 + in];
            else
              return -1;
          }
          for (size_t i = in; i < 6; ++i)
            m[i] = '0';
          m[6] = '\0';
          const uint32_t micros = strtol(m, NULL, 10);
          *t += micros;
        }
        else
          return -1;
      }
      return 0;
    }
  }
  return -1;
}

int cast_Time_to_cstring(t_Time* time, char** str)
{
  if (str != NULL)
  {
    *str                    = malloc(16u);
    uint64_t*       t       = (uint64_t*)time->v;
    const uint32_t  micros  = *t % 1000000;
    uint32_t        ss      = *t / 1000000;
    uint32_t        mm      = ss / 60;
    const uint32_t hh = mm / 60;
    mm -= hh * 60;
    ss -= hh * 3600 + mm * 60;
    sprintf_s(*str, 16, "%02u:%02u:%02u", hh, mm, ss);
    int last_nzero = 0;
    if(micros > 0)
    {
      char appo[7];
      sprintf_s(appo, 7, "%u", micros);
      (*str)[8] = '.';
      for(int i = 0; i < 6; ++i)
      {
        (*str)[9 + i] = appo[i];
        if (appo[i] != '0')
          last_nzero = i;
      }
      (*str)[9 + last_nzero + 1] = '\0';
      *str = realloc(*str, 9 + last_nzero + 1 + 1);
    }
    else
      *str = realloc(*str, 9);
    return 0;
  }
  return -1;
}

uint32_t byte_precision(const uint32_t precision)
{
  return bit_precision(precision) / 8 + 1;
}
