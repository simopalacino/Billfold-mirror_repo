#ifndef DATABASETNUMERIC_H
#define DATABASETNUMERIC_H

#include <string>
#include <vector>
#include <utility>
#include <QString>

namespace Database
{

#define CIFRA_PRECI   18
#define CIFRA_SCALE   2

class TNumeric
{
    enum Res { NUM_OK = 0, NUM_OVERFLOW };
    static constexpr uint8_t  DEFAULT_PRECISION = 18;
    static constexpr uint8_t  DEFAULT_SCALE     = 0;
    static constexpr uint8_t  MAX_PRECISION     = 38;

    enum ErrorId
    {
        Overflow = 0,
        Convertion,
        OutOfRange
    };
    static const std::pair<ErrorId, const char*> _errorMessages[32];

#ifdef QT_DEBUG
    std::string _string;
#endif
    uint8_t *_data;
    bool    _valid;
    QString _last_error_validity;

    void initializeSpace(uint8_t precision, uint8_t scale);

    void setError(ErrorId id, bool setInvalid = true, const std::vector<QString> *args = nullptr);
    void setError(const QString &msg, bool setInvalid = true);
    void setInvalidNumeric();
    void setNumeric(const char *str, bool allocDynamicSpace = false);
    TNumeric &setSign(bool positive = true);

    // Digit is the number of digits in decimal rappresentation. Returns Bytes.
    static Res     arrotondaDecimali(std::vector<uint8_t> &data, uint8_t decimalPos, uint8_t precision = DEFAULT_PRECISION, uint8_t scale = DEFAULT_PRECISION);
    static uint8_t calcSizeNum(uint8_t digit);

public:
    TNumeric             &changeSign();
    int                  compare(const TNumeric &rhv) const;
    std::vector<uint8_t> getDataRaw() const;
    QString              getError()   const;
    bool                 getSign()    const;

    bool    isValid()   const { return _valid && validPrecisionScale(precision(), scale()); }
    bool    isZero()    const;
    uint8_t precision() const { return _data[0]; }
    uint8_t scale()     const { return _data[1]; }

    TNumeric &set(const char *str);
    TNumeric &set(double num);

    double  toDouble() const;
    QString toString() const { return operator const char*(); }


    static bool validPrecisionScale(uint8_t precision, uint8_t scale)
    { return (1 <= precision && precision <= MAX_PRECISION) && scale <= precision; }

    static TNumeric abs(const TNumeric &lhv)
    {
        if (!lhv.getSign())
        {
            return std::move(TNumeric(lhv).changeSign());
        }
        return { lhv };
    }

    /** Cast operator (const char*).
     * Never returns nullptr. */
    operator const char *() const;
    bool     operator< (const TNumeric &rhv) const;
    bool     operator<=(const TNumeric &rhv) const;
    bool     operator> (const TNumeric &rhv) const;
    bool     operator>=(const TNumeric &rhv) const;
    TNumeric operator+ (const TNumeric &rhv) const;
    TNumeric operator- (const TNumeric &rhv) const;
    bool     operator==(const TNumeric &rhv) const;
    bool     isEqualTo (const TNumeric &rhv) const;
    bool     operator!=(const TNumeric &rhv) const;

    TNumeric &operator<<(const char *rhv);
    TNumeric &operator<<(double rhv);

    TNumeric &operator=(const char *rhv);
    TNumeric &operator=(double rhv);
    TNumeric &operator()(const char *rhv);
//    TNumeric &operator()(double rhv);

    TNumeric &operator=(const TNumeric &rhv);   // Copy assignment
    TNumeric &operator=(TNumeric &&rhv);        // Move assignment

    TNumeric(const char* initialValue);
    TNumeric(double initialValue);

    TNumeric(const TNumeric &rhv);              // Copy constructor
    TNumeric(TNumeric &&rhv);                   // Move constructor
    TNumeric(uint8_t precision = DEFAULT_PRECISION /* 18 */, uint8_t scale = DEFAULT_SCALE /* 0 */);
    TNumeric(const char *initialValue, uint8_t precision, uint8_t scale);
    TNumeric(double initialValue, uint8_t precision, uint8_t scale);

    ~TNumeric() { delete _data; _valid = false; }

    static const TNumeric ZERO;
};

inline TNumeric operator-(const TNumeric &lhv)
{
    return TNumeric(lhv).changeSign();
}



} // ::Database::

#endif // DATABASETNUMERIC_H
