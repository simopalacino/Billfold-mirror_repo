#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>

#include <QString>
#ifdef QT_WIDGETS_LIB
#include <QWidget>
void error(const QString &str = "Operation failed", QWidget *parent = nullptr);
#endif

#include "engine/logger.h"

#define UNUSED(x)           {(void)(x);}
#define ANSI_DATE_FORMAT    "yyyy-MM-dd"

extern engine::Logger appLog;

std::string &getTempString();
std::string &getReservedString();
//std::string *getNewNTempStrings(uint32_t n);
QString &doppiApici(QString &str);

#define MARKER_STR "ed43ee04a8dba155a8c961cef36cda806ea60a07"
extern const char *version;

#endif // UTILS_H
