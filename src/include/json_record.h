#ifndef JSON_RECORD_H
#define JSON_RECORD_H

#define REC_ID               "id"
#define REC_TYPE             "type"
#define REC_TAG              "tag"
#define REC_AMOUNT           "cifra"
#define REC_DATE             "date"
#define REC_TIME             "time"
#define REC_REGISTRATION     "registration"
#define REC_REGAMOUNT        "regamount"
#define REC_CONTO            "conto"
#define REC_FROM_ACCOUNT_ID  "from_account_id"
#define REC_TO_ACCOUNT_ID    "to_account_id"
#define REC_DESCRIPTION      "description"
#define REC_ACC_DATE         "acc_date"
#define REC_CAUSAL           "causal"
#define REC_CURRENCY         "currency"
#define REC_NOTE             "note"
#define REC_PLACE            "place"

#endif // JSON_RECORD_H
